# The Blog of Tom Webster

This is a [Jekyll-powered](http://jekyllrb.com/) static site.

A quick breakdown of the various folders and files (this is not a Jekyll
tutorial) [some folders won't be explained, like `donate` or `talks`,
they represent single pages or site sections, purpose should be
apparent]:

* `_data`: This contains a couple YAML lists that are processed and used
in the sidebar.
* `_includes`: This is a folder for HTML partials (small clips of HTML
template code, like sidebar, header, footer). It organizes the site
better to keep them in one folder.
* `_layouts`: This is the main HTML layout folder and controls how the site
looks and feels. The default layout is for everything, the posts layout
is just for blog posts.
* `_plugins`: Here's where the magic happens. Emoji, image tags, sitemap
generation, and more. Ruby files that are executed when Jekyll builds the
site. This is what makes Jekyll special.
* `_posts`: This is pretty self explanatory, but it contains posts in
[markdown](https://daringfireball.net/projects/markdown/) format.
* `assets`: A folder containing the site's CSS, JS, and font files. These
are statically served.
* `p`: An old email-signature of mine that dates back to my Blogger days,
this only holds a redirect so that signature doesn't break.
* `_config.yml`: My config settings for this site.
* `Gemfile`: The various Gems I use in building this site.
* `humans.txt`: Think of this like the Credits reel.
* `keybase.txt`: I use this to prove I own this site on
[keybase.io](https://keybase.io).
* `LICENSE`: The MIT license. Feel free to fork this or do whatever with
it, just throw my name somewhere in the credits.
* `Rakefile`: A small collection of tasks I use to automate deploying and
writing on this site.
