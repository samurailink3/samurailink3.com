---
layout: default
title: "Wifi Grenade"
---

<script async class="speakerdeck-embed" data-id="216ee38b8b5d4b998974310d33321a39" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>

You can find the build guide for this device [right here](http://samurailink3.com/blog/2015/04/09/wifi-grenade/).

# Abstract

Have you ever needed to blanket a wide area in deauth packets, but didn't have a laptop or outlet nearby? Introducing the Wifi Grenade. A script-kiddie-esque annoyance that's sure to make you the talk of your local infosec community. Annoying, stupid, and illegal, the Wifi Grenade blankets an area in deauth packets, executing a denial of service attack in your neck of the woods. Be careful, now, you're broadcasting packets that could land you in the slammer with a felony. You've been warned!

# Bio

Tom Webster is a Linux Sysadmin at Cincinnati Bell and part of the board for the Ohio InfoSec Forum. He's constantly breaking, fixing, and researching things as well as ranting over at [SamuraiLink3.com](https://www.samurailink3.com/), his [Google Plus profile](https://plus.google.com/+TomWebster/about), and [Twitter](https://twitter.com/samurailink3).

# Downloads/Embeds

You can download this whole talk, images, fonts, and all source materials [right here](http://files.samurailink3.com/wifi-grenade.zip).

If you'd like to embed this talk (like I did on this page), head over to [this talk on SpeakerDeck](https://speakerdeck.com/samurailink3/wifi-grenade) to see PDF download options, embed code, and other cool stuff.

# Credits

## Sources

* [wifijammer on GitHub](https://github.com/DanMcInerney/wifijammer)
* [How to kick everyone around you off wifi with Python](http://danmcinerney.org/how-to-kick-everyone-around-you-off-wifi-with-python/)
* [Issue 1104: 	Random BSSID (MAC address) in every connection.](https://code.google.com/p/android-wifi-tether/issues/detail?id=1104)

## Software

* [Libre Office](http://www.libreoffice.org/)
* [Gnu Image Manipulation Program (GIMP)](http://www.gimp.org/)

## Colors

* White - `rgb(255,255,255)`
* Black - `rgb(0,0,0)`
* Red - `rgb(255,51,51)`

## Fonts

* [League Spartan](http://www.fontsquirrel.com/fonts/league-spartan)
* [Bebas Neue](www.fontsquirrel.com/fonts/bebas-neue)
* [Source Code Pro SemiBold](https://github.com/adobe-fonts/source-code-pro/releases/tag/1.017R)

## Image Credits

All images are either under a CC license or used with fair use.

### QR Codes

Generated at [QR Code Artist](http://www.qrcartist.com/qr-code-resources/qr-code-generators/)

### [Glyphicons Free](http://glyphicons.com/)

**Author:** [Glyphicons](http://glyphicons.com/)

**License:** [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

### [bomb](http://pixabay.com/p-40734/?no_redirect)

**Author:** [Nemo](http://pixabay.com/en/users/Nemo-3736/)

**License:** [CC0](http://creativecommons.org/publicdomain/zero/1.0/deed.en)

### [wifi](http://pixabay.com/en/audio-signal-wifi-wireless-304331/)

**Author:** [Nemo](http://pixabay.com/en/users/Nemo-3736/)

**License:** [CC0](http://creativecommons.org/publicdomain/zero/1.0/deed.en)

### wifi-grenade

**Author:** [Tom Webster](http://samurailink3.com)

**License:** [CC0](http://creativecommons.org/publicdomain/zero/1.0/deed.en)

### wifi-grenade-photo

**Author:** [Tom Webster](http://samurailink3.com)

**License:** [CC-BY-SA](http://creativecommons.org/licenses/by-sa/4.0/)
