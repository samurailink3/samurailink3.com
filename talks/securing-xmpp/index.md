---
layout: default
title: "Securing XMPP"
---

<script async class="speakerdeck-embed" data-id="dc2378abf9b242208a3d9363bf85cd50" data-ratio="1.33159947984395" src="//speakerdeck.com/assets/embed.js"></script>

# Abstract

With the huge obsession with various messaging platforms out there, I wondered
if I could roll my own based on XMPP, but do so securely. Follow my trials,
errors, and frustrations as I dig through XMPP and try to deploy a modern
messaging service using an antiquated standard.

# Bio

Tom Webster is just a tech guy who rants about virtually everything. He's
constantly breaking, fixing, and researching things as well as ranting over at
[SamuraiLink3.com](https://samurailink3.com) and
[Twitter](https://twitter.com/samurailink3).

# Downloads/Embeds

You can download this whole talk, images, fonts, and all source materials [right
here](https://429f159747010d3e425a-7558f5f48a8025938a6d2873e8a987c0.ssl.cf2.rackcdn.com/securing-xmpp.zip).

If you'd like to embed this talk (like I did on this page), head over to [this
talk on SpeakerDeck](https://speakerdeck.com/samurailink3/securing-xmpp) to see
PDF download options, embed code, and other cool stuff.
