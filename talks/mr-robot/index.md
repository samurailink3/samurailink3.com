---
layout: default
title: "The Accuracy of Mr Robot"
---

<script async class="speakerdeck-embed" data-id="015a5173a2a44c4686868ed283dbe853" data-ratio="1.33159947984395" src="//speakerdeck.com/assets/embed.js"></script>

# Is Mr. Robot Accurate?

This is a quick little talk I gave at [OISF](http://www.ohioinfosec.org/) about
Mr. Robot and it's accuracy. Nothing really that hasn't been discussed at length
already.

## Downloads/Embeds

You can download this whole talk, images, fonts, and all source materials [right
here](http://files.samurailink3.com/mr-robot.zip).

If you'd like to embed this talk (like I did on this page), head over to [this
talk on
SpeakerDeck](https://speakerdeck.com/samurailink3/the-accuracy-of-mr-robot) to
see PDF download options, embed code, and other cool stuff.

## Findings

* [Spying via open wifi][1]
* [weak passwords][1]
  * [In a safe cracking scene, the PIN was the date a person's MBA was awarded.][4]
* **INACCURATE** - [DDOS used in show where rootkit would be][2]
  * [DDOS could have been used as a distraction while a rootkit was being
    placed][2], but this is unlikely given that DDOS makes it hard to reach servers...
* [Phishing calls to get personal information, which is later used to create password dictionaries.][2]
* [Utilizing supersu to install persistant android malware (Flexispy)][4]
  * [Rooting was done with framaroot][18]
  * [How to Install Flexispy](http://www.flexispy.com/en/movies/video-tutorial-flexispy-installation.htm) - This is shot-for-shot done in the show.
* **INACCURATE** - [Invalid IP address](https://i.imgur.com/WpwYRd3.png)
  * Interesting story here, the show's legal department simply wouldn't allow showing a real IP address, as it posed too much of a liability risk to the show, so they had to use an invalid IP.
* **INACCURATE** - [Time is sped up on some of the slower hacks, dictionary attacks, for instance][3]
  * This is necessary, as scenes with John the Ripper would have taken far too long to accurately portray.
* [Remotely accessible Raspberry Pi with Kali][3]
  * Technical advisors worked in a cell connection so the Pi was "always accessible", even behind NAT on it's ethernet connection.
  * The command was netcat with a listening shell: `nc -l -p6996 -e /bin/sh`
* [Dropping USB keys as a social engineering hack][15]
  * **INACCURATE** - The payload was supposed to open port 22 (ssh), but the target was a Windows machine. Sure, you can run ssh on Windows, but it's an unlikely connection type, also a reverse shell is much much more likely in this scenario.
* **INACCURATE** - [Inaccuracies in the bluetooth keyboard hack, only sniffing tools were shown, not brute force or connection tools.][15]
* [HID/RFID cloning][16]
* [Social Engineering with SET][16] - This is stupidly accurate, awesome!
* [Space before command to avoid history][19]

## Credits

### Sources

1. [PC Magazine](http://www.pcmag.com/article2/0,2817,2486889,00.asp)
2. [Avast Blog](https://blog.avast.com/2015/06/25/are-the-hacks-on-mr-robot-real/)
3. [Forbes](http://www.forbes.com/sites/abigailtracy/2015/07/08/mr-robots-cyber-crime-expert-talks-accuracy-hacking-misconceptions-and-what-other-shows-get-wrong/)
4. [Vulture](http://www.vulture.com/2015/07/mr-robot-usa-hacking-unusually-accurate.html)
5. [GeekWire](http://www.geekwire.com/2015/mr-robot-rewind-mostly-accurate-tech-in-a-mind-blowing-episode-8/)
6. [Wired](http://www.wired.com/2015/07/mr-robot-fact-check/)
7. [IB Times](http://www.ibtimes.com/why-usa-networks-mr-robot-most-realistic-depiction-hacking-television-2020213)
8. [Engadget](http://www.engadget.com/2015/08/29/the-many-surprisingly-realistic-hacks-of-mr-robot/)
9. [Reddit](https://www.reddit.com/r/MrRobot/comments/3d2lvq/is_mr_robot_the_most_real_technically_speaking/)
10. [Quora](https://www.quora.com/Mr-Robot-TV-series/Is-Mr-Robot-a-good-representation-of-real-life-hacking-and-hacking-culture-Is-the-depiction-of-hacker-societies-realistic)
11. [Nerdist](http://nerdist.com/mr-robot-michael-bazzell-hacking-accuracy/)
12. [The Verge](https://www.theverge.com/2015/3/18/8240633/sxsw-2015-mr-robot-review-christian-slater)
13. [Android Police](http://www.androidpolice.com/2015/07/12/usa-networks-new-show-mr-robot-shows-accurate-android-hacking-including-supersu-and-flexispy/)
14. [Reddit](https://www.reddit.com/r/ProgrammerHumor/comments/38ndmr/so_i_was_watching_mr_robot_and_noticed_this_funny/)
15. [GeekWire](http://www.geekwire.com/2015/mr-robot-rewind-hunting-for-flaws-in-an-intense-episode-6/)
16. [GeekWire](http://www.geekwire.com/2015/mr-robot-rewind-analyzing-human-exploits-in-a-wild-episode-5/)
17. [Medium](https://medium.com/android-news/what-is-the-surprisingly-commercial-android-backdoor-depicted-in-mr-robot-bde7804cbac5)
18. [Framaroot](http://framaroot.net/faq.html)
19. [HackerNews](https://news.ycombinator.com/item?id=10102417)

<!-- Inline reference links -->

[1]: http://www.pcmag.com/article2/0,2817,2486889,00.asp "PC Magazine"
[2]: https://blog.avast.com/2015/06/25/are-the-hacks-on-mr-robot-real/ "Avast Blog"
[3]: http://www.forbes.com/sites/abigailtracy/2015/07/08/mr-robots-cyber-crime-expert-talks-accuracy-hacking-misconceptions-and-what-other-shows-get-wrong/ "Forbes"
[4]: http://www.vulture.com/2015/07/mr-robot-usa-hacking-unusually-accurate.html "Vulture"
[5]: http://www.geekwire.com/2015/mr-robot-rewind-mostly-accurate-tech-in-a-mind-blowing-episode-8/ "GeekWire"
[6]: http://www.wired.com/2015/07/mr-robot-fact-check/ "Wired"
[7]: http://www.ibtimes.com/why-usa-networks-mr-robot-most-realistic-depiction-hacking-television-2020213 "IB Times"
[8]: http://www.engadget.com/2015/08/29/the-many-surprisingly-realistic-hacks-of-mr-robot/ "Engadget"
[9]: https://www.reddit.com/r/MrRobot/comments/3d2lvq/is_mr_robot_the_most_real_technically_speaking/ "Reddit"
[10]: https://www.quora.com/Mr-Robot-TV-series/Is-Mr-Robot-a-good-representation-of-real-life-hacking-and-hacking-culture-Is-the-depiction-of-hacker-societies-realistic "Quora"
[11]: http://nerdist.com/mr-robot-michael-bazzell-hacking-accuracy/ "Nerdist"
[12]: https://www.theverge.com/2015/3/18/8240633/sxsw-2015-mr-robot-review-christian-slater "The Verge"
[13]: http://www.androidpolice.com/2015/07/12/usa-networks-new-show-mr-robot-shows-accurate-android-hacking-including-supersu-and-flexispy/ "Android Police"
[14]: https://www.reddit.com/r/ProgrammerHumor/comments/38ndmr/so_i_was_watching_mr_robot_and_noticed_this_funny/ "Reddit"
[15]: http://www.geekwire.com/2015/mr-robot-rewind-hunting-for-flaws-in-an-intense-episode-6/ "GeekWire"
[16]: http://www.geekwire.com/2015/mr-robot-rewind-analyzing-human-exploits-in-a-wild-episode-5/ "GeekWire"
[17]: https://medium.com/android-news/what-is-the-surprisingly-commercial-android-backdoor-depicted-in-mr-robot-bde7804cbac5 "Medium"
[18]: http://framaroot.net/faq.html "Framaroot"
[19]: https://news.ycombinator.com/item?id=10102417 "HackerNews"

### Images

#### QR Codes

Generated at [QR Code Artist](http://www.qrcartist.com/qr-code-resources/qr-code-generators/)

#### [Glyphicons Free](http://glyphicons.com/)

**Author:** [Glyphicons](http://glyphicons.com/)

**License:** [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

* [stereo-typing.gif](http://gifsoup.com/view/3553686/ncis-stereo-typing.html) - Fair use
* [WpwYRd3.png](https://www.reddit.com/r/ProgrammerHumor/comments/38ndmr/so_i_was_watching_mr_robot_and_noticed_this_funny/) - Fair use
* [pi-nc.jpg](http://null-byte.wonderhowto.com/how-to/hacks-mr-robot-build-hacking-raspberry-pi-0163143/)
* https://www.youtube.com/watch?v=hkDD03yeLnU - Fair use
* [independence-day-hacking.jpg](http://www.hdmagazine.it/wp-content/uploads/independence-day-hacking.jpg) - Fair use
* [set.jpg](https://twitter.com/hackingdave/status/624046525237342208) - Fair use

### Colors

* Black - `rgb(0,0,0)`
* Red - `rgb(255,51,51)`
* Gray 3 - `rgb(204,204,204)`

### Fonts

* [Sauce Code Powerline Medium](https://github.com/powerline/fonts)
* [MR ROBOT](https://www.reddit.com/r/MrRobot/comments/3h3l31/mr_robot_font_to_download/) - By [justinionn](https://www.reddit.com/user/justinionn)
