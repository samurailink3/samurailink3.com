---
layout: default
title: "Talks and Podcast Appearances"
---

When I'm not slaying dragons or saving orphan kittens from fires, sometimes I
like to kick back and give talks on various subjects. Here's a collection of
various talks I've given over the years, along with podcast appearances.

# Talks

## [Eject the Warp Core: A Practical Look at Filesystem Segregation and Encryption](/blog/2012/05/10/eject-the-warp-core-a-practical-look-at-filesystem-segregation-and-encryption/) - 2013-05-10

I wanted to hack my CR48 and make it a truly secure box. Secure in the way that
an adversary could walk away with the computer, and I'm left holding the truly
important data. Welcome to ejectable core computing.

## [Making Security Shiny](/talks/making-security-shiny/) - 2013-10-10

Interfaces suck and security applications are among some of the worst offenders.
In this talk, I dive into various security program interfaces, what make them
bad (and good, in a few cases), and what lessons we can learn to make them
better.

## [Salting your Hashes: Modern Password Storage](/talks/salting-your-hashes/) - 2013-12-12

Just a quick presentation for the Ohio InfoSec Forum holiday meeting.

Writing a web app? Storing user passwords? Don't ever store them in plain text,
you already know this. But do you know how to securely hash them? Here's a very
basic look at salted hashes and how they improve security.

## [Hacking People: No Mask Required](/talks/hacking-people/) - 2014-02-13

There has been yet another high-value Twitter account taken over through social
engineering. I take a tour through a couple well-known social engineers, the
hacks of [Mat
Honan](http://www.wired.com/gadgetlab/2012/08/apple-amazon-mat-honan-hacking/all/)
and [Naoki Hiroshima](https://medium.com/cyber-security/24eb09e026dd), and some
lessons we can walk away with.

## [Modern Times: Passwords](/talks/modern-times/) - 2014-05-08

With the release of Stanford's new password requirements, we should all start
re-thinking the modern password, what we're up against, and how we can keep our
users cooperative.

## [2 Factor: General Discussion](/talks/2-factor/) - 2014-09-11

People don't really think about all the ways 2 factor authentication fails us or
doesn't protect us. Here's a quick, shallow look at a few problems facing 2FA
today.

## [Wifi Grenade](/talks/wifi-grenade/) - 2015-04-09

Have you ever needed to blanket a wide area in deauth packets, but didn't have a
laptop or outlet nearby? Introducing the Wifi Grenade. A script-kiddie-esque
annoyance that's sure to make you the talk of your local infosec community.
Annoying, stupid, and illegal, the Wifi Grenade blankets an area in deauth
packets, executing a denial of service attack in your neck of the woods. Be
careful, now, you're broadcasting packets that could land you in the slammer
with a felony. You've been warned!

## [The Accuracy of Mr Robot](/talks/mr-robot/) - 2015-10-08

This is a quick little talk I gave at [OISF](http://www.ohioinfosec.org/) about
Mr. Robot and it's accuracy. Nothing really that hasn't been discussed at length
already.

## [pfSense Basics](/talks/pfsense-basics/) - 2016-02-11

This was a pretty demo-heavy talk I gave at [OISF](http://www.ohioinfosec.org/).
I taught basic installation and configuration. The next month, I gave a
follow-up talk on how to configure remote access and whole-home-vpn.

## [Securing XMPP](/talks/securing-xmpp/) - 2017-04-13

This is a pretty quick talk I have at [OISF](http://www.ohioinfosec.org/). I
dive into trying to create a Signal-replacement using XMPP and the challenges
around it.

# Podcast Appearances

## Security:inThirty

I co-host the [Security:inThirty](http://inthirty.net/category/insecurity/)
podcast with [Chaim Cohen](http://chaimtime.com/)
[(+ChaimCohen)](https://plus.google.com/+ChaimCohen). It's a security show for
normal people about how to protect yourself online, security devices, news, and
more. We record live Wednesday evenings (usually around 8PM) and you can catch
our videos and live recordings on
[YouTube](https://www.youtube.com/user/securityinthirty). We offer MP3 download
on our [website](http://inthirty.net/category/insecurity/) or via our [RSS Feed
(part of the inThirty network)](http://feeds.feedburner.com/inThirty).

  * [Website](http://inthirty.net/category/insecurity/)
  * [YouTube](https://www.youtube.com/user/securityinthirty)
  * [Google+](https://plus.google.com/101747680447336088593/posts)
  * [RSS](http://feeds.feedburner.com/inThirty)

## inThirty

Sometimes I join the [inThirty](http://inthirty.net/) team to talk about popular
security news or tech trends.

### [inThirty 98 – Looking Through the PRISM](http://inthirty.net/2013/06/inthirty-98-looking-through-the-prism/)

In this podcast, I was asked to join in on a discussion about PRISM and what it
means for computer users everywhere.

### [inThirty 111 – Encrypted SEO](http://inthirty.net/2013/09/encrypted-seo-inthirty-111/)

Google switched ALL of their search traffic to SSL. Does this really protect
anyone from the prying eyes of the NSA? The inThirty gang bring me on to discuss
the technical details of the change.

## [Gutrade Podcast](http://www.gutrade.net/)

### [Gutrade Podcast - Free public Wifi is not so free and what to do with all those passwords](http://www.gutrade.net/tom-webster-free-public-wifi-is-not-so-free-and-what-to-do-with-all-those-passwords/)

In this podcast, Gunnar and I discuss open wifi and why it isn't to be trusted.

### [Gutrade Podcast - What are bitcoins?](http://www.gutrade.net/tom-webster-what-are-bitcoins/)

In this podcast, I talk about Bitcoins, how they work, why they are secure, and
how they make sense in the financial world.
