---
layout: default
title: "2 Factor: General Discussion"
---

<script async class="speakerdeck-embed" data-id="a43155801ce00132f326123486985575" data-ratio="1.33159947984395" src="//speakerdeck.com/assets/embed.js"></script>

# Abstract

2-Factor authentication is pretty great. Everyone should use it! Everywhere! For all the things! It is completely and utterly foolproof and perfect in every way!!!!! Wait.. that doesn't sound right... 2-Factor authentication has problems, let's talk about them. This talk is a general discussion about what I use and what I perceive are the main problems with 2FA.

# Bio

Tom Webster is an IT grunt, VPN admin, Security Lead, Programmer, and general tech guy at BWI Group. He's constantly breaking, fixing, and researching things as well as ranting over at [SamuraiLink3.com](https://www.samurailink3.com/) and his [Google Plus profile](https://plus.google.com/+TomWebster/about). Tom has a secret love of cooking while wearing Google Glass and can make a damn good pot roast.

# Downloads/Embeds

You can download this whole talk, images, fonts, and all source materials [right here](http://files.samurailink3.com/2-factor.zip).

If you'd like to embed this talk (like I did on this page), head over to [this talk on SpeakerDeck](https://speakerdeck.com/samurailink3/2-factor-general-discussion) to see PDF download options, embed code, and other cool stuff.

# Credits

## Software

* [Libre Office](http://www.libreoffice.org/)
* [Gnu Image Manipulation Program (GIMP)](http://www.gimp.org/)

## Colors

* 2FA-Green - `rgb(158,212,74)`
* Black - `rgb(0,0,0)`
* White - `rgb(255,255,255)`
* Red - `rgb(255,51,51)`
* Blue 2 - `rgb(51,51,255)`

## Fonts

* [Code](http://www.fontsquirrel.com/fonts/code)
* [Montserrat](http://www.fontsquirrel.com/fonts/montserrat)

## Image Credits

All images are either under a CC license or used with fair use.

### QR Codes

Generated at [QR Code Artist](http://www.qrcartist.com/qr-code-resources/qr-code-generators/)

### [Glyphicons Free](http://glyphicons.com/)

**Author:** [Glyphicons](http://glyphicons.com/)

**License:** [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

### [MultiFactorAuthentication.jpg](https://www.flickr.com/photos/dogsbodyorg/5752309755/in/photolist-9Lj6hp-nVAyP2-ocMMMk-nVAgwY-oeReLP-jD7qGa-9Pcug1)

**Author:** [Dan Benton](https://www.flickr.com/photos/dogsbodyorg/)

**License:** [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)

### [MasterLockRootPassword.jpg](https://www.flickr.com/photos/schill/4813392151)

**Author:** [Scott Schiller](https://www.flickr.com/photos/schill/)

**License:** [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)

### [rsa.jpg](https://www.flickr.com/photos/jes5199/2417291602)

**Author:** [EUNOIA](https://www.flickr.com/photos/jes5199/)

**License:** [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)

### [EvilHacker.jpg](http://www.flickr.com/photos/9917647@N02/6866136812/in/photolist-bsJKgb-bFZpyg-amRqCw-bsKsus-foPnZu-8pTxJv-aSy7yt-bFE7Ma)

**Author:** [Katy Levinson](http://www.flickr.com/photos/katylevinson/)

**License:** [CC BY-NC-SA 2.0](http://creativecommons.org/licenses/by-nc-sa/2.0/)

### [gauth_logo.png](https://www.flickr.com/photos/xmodulo/14596570163/in/photolist-nVAyP2-ocMMMk-nVAgwY-oeReLP-9Lj6hp-jD7qGa-9Pcug1)

**Author:** [Linux Screenshots](https://www.flickr.com/photos/xmodulo/)

**License:** [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)

### [yubikey.jpg](https://www.flickr.com/photos/elecnix/5105433386)

**Author:** [Nicolas Marchildon](https://www.flickr.com/photos/elecnix/)

**License:** [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)

### [phonecall.jpg](https://www.flickr.com/photos/blakta2/8257149092)

**Author:** [1950sUnlimited](https://www.flickr.com/photos/blakta2/)

**License:** [CC BY 2.0](https://creativecommons.org/licenses/by/2.0/)

### [othertokens.jpg](https://www.flickr.com/photos/bassplayerdoc/6245647402/in/photolist-)

**Author:** [Edwin Sarmiento](https://www.flickr.com/photos/bassplayerdoc/)

**License:** [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)

### [nsa_approved.jpg](http://forums-cdn.appleinsider.com/2/20/20ab40f8_B3_NSA_Approved_GG_WEB.jpeg)

**Author:** Unknown

**License:** Fair Use

### [sqrl.png](https://www.grc.com/sqrl/sqrl.htm)

**Author:** [Gibson Research Corporationn](https://www.grc.com/)

**License:** Fair Use

### [fido.png](https://fidoalliance.org/specifications)

**Author:** [FIDO Alliance](https://fidoalliance.org/)

**License:** Fair Use

### thanks.png

**Author:** [Tom "SamuraiLink3" Webster](http://samurailink3.com)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
