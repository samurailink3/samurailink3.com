---
layout: default
title: "pfSense Basics"
---

<script async class="speakerdeck-embed" data-id="65c07f1ca9ca4bb1aa995bf62c89f2b9" data-ratio="1.33159947984395" src="//speakerdeck.com/assets/embed.js"></script>

# Abstract

Get rid of your Linksys and take control of your network. In this demo-heavy
presentation, I'll walk you through basic pfSense installation and
configuration.

# Bio

Tom Webster is a Linux Sysadmin at Cincinnati Bell and part of the board for the
Ohio InfoSec Forum. He's constantly breaking, fixing, and researching things as
well as ranting over at [SamuraiLink3.com](https://www.samurailink3.com/), his
[Google Plus profile](https://plus.google.com/+TomWebster/about), and
[Twitter](https://twitter.com/samurailink3).

# Downloads/Embeds

You can download this whole talk, images, fonts, and all source materials [right
here](https://429f159747010d3e425a-7558f5f48a8025938a6d2873e8a987c0.ssl.cf2.rackcdn.com/pfsense-basics.zip).

If you'd like to embed this talk (like I did on this page), head over to [this
talk on SpeakerDeck](https://speakerdeck.com/samurailink3/pfsense-basics) to see
PDF download options, embed code, and other cool stuff.

# Credits

## Sources

* [https://pfsense.org/](https://pfsense.org/)

## Software

* [Libre Office](http://www.libreoffice.org/)
* [Gnu Image Manipulation Program (GIMP)](http://www.gimp.org/)

## Colors

* White - `rgb(255,255,255)`
* Gray 3 - `rgb(204,204,204)`
* pfSense Red - `rgb(183,28,28)`

## Fonts

* [Montserrat Regular](http://www.fontsquirrel.com/fonts/montserrat)
* [BebasNeue Bold](http://www.fontsquirrel.com/fonts/bebas-neue)

## Image Credits

All images are either under a CC license or used with fair use.

### QR Codes

Generated at [QR Code Artist](http://www.qrcartist.com/qr-code-resources/qr-code-generators/)

### [Glyphicons Free](http://glyphicons.com/)

**Author:** [Glyphicons](http://glyphicons.com/)

**License:** [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

### [pfSense Logo](https://pfsense.org/images/logo-full.png)

**Author:** [pfSense](https://pfsense.org/)

**License:** Fair Use
