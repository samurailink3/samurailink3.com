---
layout: post
title: "The Probable Demise of PastaNet"
date: 2010-08-02 14:31:00 EDT
comments: true
categories: [News]
---

Attention PastaNet Users [and trusted friends in science]: PastaNet may
be going away soon. AT&T doesn't run their fiber across the street from
where I used to live and has no idea when the Uverse service will be
coming to my neck of the woods (about 500 feet away). I'm looking for any
fast (and hopefully cheap, but I can pay a premium) ISP
suggestions/comments/rants so I can pick one that may work for our users.
From what it looks like, a best-case scenario (from my current knowledge)
would be scaling back user accounts and active logins to a minimum level
and having connection speeds hover around 30KBps. If any of you have
ideas for ISPs that we could go with, or if you want to donate to
Run-A-Fiber-Line-Directly-To-PastaNet Fund, please let me know. Your
contributions to the think-tank of ideas and schemes, and your dollars,
are greatly appreciated, and will help save PastaNet. As it stands now,
running PastaNet off of the Time Warner infrastructure will cripple our
operations and slow down everything we are trying to accomplish. I have
recently been pricing how much a dedicated fiber run would cost and have
put up a PayPal donate button on the right sidebar of this page. Thank
you for helping me help you help us all, trusted friends in science.
