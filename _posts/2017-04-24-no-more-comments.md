---
layout: post
title: "No More Comments"
date: 2017-04-24 10:13:44 EDT
categories: [News]
emoji: true
bg: false
---

I've removed Disqus comments for the site. They aren't adding a whole lot beyond
a myriad of tracking domains and extra cruft to load. If you'd like to contact
me about an article, grab me on [Twitter](https://twitter.com/samurailink3) or
[Mastodon](https://mastodon.cloud/@samurailink3).

The main inspiration for this change came from [this
post](http://donw.io/post/github-comments/). I don't feel the need to replace
Disqus with GitHub comments, but it's an interesting idea. The real inspiration
was seeing the load time graph. Comments aren't worth that.
