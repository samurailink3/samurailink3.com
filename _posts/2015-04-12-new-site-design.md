---
layout: post
title: "New Site Design"
date: 2015-04-12 17:09:53 EDT
comments: true
categories: [News]
---

I've been working [for a
while](https://gitlab.com/samurailink3/samurailink3.com/commit/a87008ad992f3ce056fbec2fdfb85e0c1ab2834c)
on re-doing my site. I've decided to move from
[Octopress](http://octopress.org/) to [Jekyll](http://jekyllrb.com/) for
a variety of reasons. Octopress v2 had some [well documented
shortcomings](http://octopress.org/2015/01/15/octopress-3.0-is-coming/),
but it honestly was a fantastic intoduction to Jekyll and flat-file
sites. I really love what Octopress is and how it introduced me to
generating flat file sites, but I've outgrown it. I've moved fully to
Jekyll, and as always [you can grab all the source code
here](https://gitlab.com/samurailink3/samurailink3.com).

If you find any errors, bugs, or want to make something better, I do accept merge requests, [so have at it](https://gitlab.com/samurailink3/samurailink3.com).
