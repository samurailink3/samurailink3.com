---
layout: post
title: "watchtwitch 4.0.0 - Command Line Twitch.TV Browser"
date: 2016-10-14 21:44:32 EDT
comments: true
categories: [Code]
emoji: true
bg: "2016-10-14-watchtwitch-400---golang-edition-bg.png"
---
<h1 style="text-align:center;">:video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game:</h1>

Yet another updated to `watchtwitch`, this time removing the dependency on
[`streamlink`](https://github.com/streamlink/streamlink) (or `livestreamer`).
`watchtwitch` is now able to load up streams all on its own. This is still a new
feature, so it may not work 100% of the time, so I've included a `-fallback`
flag that uses [`streamlink`](https://github.com/streamlink/streamlink) to grab
the streams.

Other than that, I've switch the default media player from VLC to MPV.

[{% img center ../../../../../images/2016-10-14-watchtwitch-400---golang-edition-1.png %}](../../../../../images/2016-10-14-watchtwitch-400---golang-edition-1-full.png)

## Downloads

* [watchtwitch-4.0.0.linux.386](https://files.samurailink3.com/watchtwitch-4.0.0.linux.386)
* [watchtwitch-4.0.0.linux.amd64](https://files.samurailink3.com/watchtwitch-4.0.0.linux.amd64)
* [watchtwitch-4.0.0.linux.arm](https://files.samurailink3.com/watchtwitch-4.0.0.linux.arm)

As always, you can [grab the code here on
GitLab](https://gitlab.com/samurailink3/watchtwitch). If you'd like a build for
a different platform, let me know in the comments.

<h1 style="text-align:center;">:video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game:</h1>
