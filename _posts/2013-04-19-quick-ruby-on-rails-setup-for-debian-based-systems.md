---
layout: post
title: "Quick Ruby on Rails Setup for Debian-Based Systems"
date: 2013-04-19 04:25 EDT
comments: true
categories: [Tutorials, Linux]
---

With new OS installs comes the need to redo my development environment.
Instead of running through and manually installing what I need, I created
a quick-and-dirty bash script to do it for me. Check it out:

This installs `Ruby 1.9.3` and `Rails 3.2.1`. Make sure to run this as a
sudo user.

```bash
# Thanks to http://xyzpub.com/en/ruby-on-rails/3.2/rails3-install-debian.html

sudo apt-get -y install build-essential openssl libreadline6 libreadline6-dev curl git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison subversion python
curl -L https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
rvm autolibs enable
rvm install 1.9.3
gem update
gem install rails --version '~> 3.2.1'
echo "source \$(rvm 1.9.3 do rvm env --path)" >> $HOME/.bashrc
echo "Complete (Ruby 1.9.3 - Rails 3.2.1)"
```
