---
layout: post
title: "Nasty Google Drive Permissions Bug"
date: 2015-02-05 22:26 EDT
comments: true
categories: [News, Security]
---

As some of you know, I co-host [Security:inThirty](http://inthirty.net)
with [Chaim Cohen](http://chaimtime.com). We get emails from time to time
from listeners with questions, comments, and stories. One frequent
listener informed us about a very strange security problem they were
running into with Google Drive: **They can access someone else's files,
even though they haven't this user hasn't shared anything with them.**

As strange as this issue seems at first glance, I couldn't dismiss it as
user error, as I had seen the same thing once in the past myself, and
have read about it happening in a few other instances. The user in
question is [Gunnar Haid](http://www.gutrade.net), he's technically apt
and security aware. This isn't user error (at least on his part), and I
doubt this is human error by the other user either (more on this in a
bit). This problem isn't widespread (as far as I can tell), but other
Drive permissions bugs have been very prevalent in the community (such as
being unable to delete files you own).

I was sent an email thread and several screenshots detailing the problem.
I am not releasing either in the interest of privacy (most screenshots
would need to be heavily censored, removing the point of posting them).
The first thing Gunnar did was contact Google support, the right move.
One support rep was convinced that the other user had marked their files
as "public on the web", this is not the case. Gunnar has provided
screenshots showing file permissions that list only the owner has access
and that link sharing is disabled. Gunnar was then passed around to a
couple other support reps, running in circles trying to explain the same
issue, to no avail. As it stands today, Google requested (and has
received) screenshots, but has not responded to the issue since **October
1st 2014**.

The user in question who is having their data leaked by this bug is very
technical as well. They have several websites and work in a tech-based
field. The filenames also lend me to believe this person is very
technical and working with advanced tech (for non-tech people, anyway).
For obvious reasons, I can't go into personal details beyond that, but
needless to say, this user is also very technically apt.

This isn't a case of user error, this looks like a pretty serious bug
that Google needs to take a hard look into. **The big issue here is
someone's files are completely accessible by someone else who has _no
relation_ to the user.** Our show has a small (but dedicated) following
and it makes me wonder how widespread this issue is. It doesn't seem
widespread, but I have no way of knowing for sure. If Google would
comment on the issue, I'll be more than happy to post the response, at
the moment, I'm only concerned with getting this fixed and figuring out
why it happened in the first place.
