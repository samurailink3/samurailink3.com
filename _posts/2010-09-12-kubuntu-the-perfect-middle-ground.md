---
layout: post
title: "Kubuntu: The Perfect Middle Ground?"
date: 2010-09-12 23:30:00 EDT
comments: true
categories: [Linux, News]
---

I really really like Kubuntu 10.04. Out of curiosity, I loaded up Kubuntu
10.04 onto my external hard drive to give it a spin. Other than loving
the fact that I have my own personal encrypted Linux install bootable at
all times on just about any computer I happen to be near, I really really
like the new KDE. Take the easy-to-use mentality of standard Ubuntu and
mix it with the endless-options power-user mentality of KDE and you get
Kubuntu. First impressions went something like this:

    Oh! Its the ease of Ubuntu! But wait... all of these options.. all of
    these rolled in configuration options, right up front? This is made for
    power users! But it is still Ubuntu? I'm confused... and happy

Kubuntu seems like the perfect middle ground for those not yet happy to
jump to Fedora, SuSE, or Debian proper. Kubuntu still contains some
helpful Ubuntu-centric additions (read: training wheels) for the Linux
newcomers, but maintains the features and customizations that power-users
crave. To add to the list of things I really like about Kubuntu, it is
absolutely beautiful. If I were to pick one Linux distribution to deploy
to a mass enterprise environment, it would be Kubuntu. Debian backend,
Ubuntu ease-of-use, and KDE power and flexibility. It could possibly be
one of the best general office use distributions yet. The only problems I
can see with KDE is operating within a current Microsoft-rich environment
(and this is more of a general Linux /OpenSource problem than a
KDE/Ubuntu problem) is the total lack of Exchange support. Whether it is
with Evolution or Kontact, it just kills me when I find amazing open
source applications that don't work with the biggest force in business
today. If running a business on open standards and open technologies is
interesting to you, consider Kubuntu a powerful choice. I highly
recommend Kubuntu for those who are ready for more Linux power in their
hands, while not quite ready to take the full plunge.
