---
layout: post
title: "PGP Key Expiring Soon"
date: 2017-02-09 12:39:27 EST
comments: true
categories: [News]
emoji: true
bg: false
---

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

My PGP key will be changing soon. It expires on 2017-03-18. I'll attempt to change it this month (2017-02).
I will update keybase.io/samurailink3, https://samurailink3.com/pgp/, and the MIT PGP Key Server.
This message is signed by me (and my still-valid key) so you know it's the real deal.
This will be tweeted by @samurailink3, posted on https://samurailink3.com, and posted in the
Security:inThirty group chat for additional verification.
Another blog post/tweet/message will be made when my new key has been created.
-----BEGIN PGP SIGNATURE-----

iQJLBAEBCAA1FiEEEckwxGk6bJt4m7D3ZELfChS6Tv0FAlicqRQXHHNhbXVyYWls
aW5rM0BnbWFpbC5jb20ACgkQZELfChS6Tv2YgA//WJUqAwAkW9zcxym54+z+zNdV
0s5mQgztt/eCcbfNaakqdkKboREW03dU26k/uKaWOvqW/fOWRmgDQ7XNGq5OHP+f
5L5LD+2z9Hx0T4LlowYnDmqmTplzIfmwpFnXHqXp0kvk2nbPzVwJdcE5fQjdhAF2
sQU12MhcWly9iwYvEo5pU/5WyWfMvaxn3m5pFv0vzdCVzyWb+qdpvN0VZgipDiXK
OSMR+8TDvWHuZSDS02V1QGh9eVT0SrbGeFY6TJHwPjGrzTv+QFKrrgVh+uSsNDuM
CVSeg0yqhLx+fLyOiYHbOklzrcMmIj1lF1wMs/AuyTCtJ3J2E2Xpvc6dyhDp0wma
9noov4yW0HOEZEgrP3GMtsBnBU1gpnawRA8snnc/pUppGQcR2tm7HTOxsGE2d5u3
5DQ59WQc/3LOmrNWlaKEAzITNaWZ74yMu0IB9PLUSx0sPQ+LP6ja0y72cwfWiL4g
L+Di9WMFrnrRf6EDVOTiwW1rQlOQrWr+c/PhyWv/gKtaznHiL9/13ulgRPymqGgv
pRILzRFFBve8n+7QVczr6LILLJDAQqS8xfQuA+Fm9f78vsFflNs59EBNBIqdRaae
ieFJ0YMOxUgbPEX0oo5/BsLG1itW7VH1ylcIlA8r5mMrliP/i8ZGydqTxceTe2ak
fpnSgNpoB9nAIUNmtw0=
=l9er
-----END PGP SIGNATURE-----
```
