---
layout: post
title: "NSA Documents Archive"
date: 2015-02-17 18:10 EDT
comments: true
categories: [Projects]
---

I've put up a [document archive](/nsa/) for all of the
[EFFs](https://www.eff.org/nsa-spying/nsadocs) NSA documents. You can
download the entire zip file in one click, no BS. I've signed it with my
[public key](/pgp/) so you can verify it's legit. [Head over this
way](/nsa/) for the details and mirroring instructions.
