---
layout: post
title: "Unix Toolbox"
date: 2015-08-30 13:23:46 EDT
comments: true
categories: [Linux, Link]
---

A couple days ago I came across this link which is really helpful. A
one-page reference for TONS of \*nix commands. Everything from SSH
tricks, to database commands, to encryption is here. Pretty handy!

Just a caveat: In the ssh section, don't use DSA keys, use ED25519 or RSA
(4096 bit) keys. I imagine some of this information is older, keep an eye
out.

[Unix Toolbox](http://cb.vu/unixtoolbox.xhtml)
