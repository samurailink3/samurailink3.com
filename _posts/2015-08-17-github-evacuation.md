---
layout: post
title: "GitHub Evacuation (Hello GitLab)"
date: 2015-08-17 20:00:00 EDT
comments: true
categories: [News]
---

I'm leaving GitHub for [GitLab](https://gitlab.com/u/samurailink3). All
project references on this site have been changed to reflect their new
home. All GitHub-hosted code gists have been replaced with
locally-highlighted code snippets.

The reasons for this are three-fold:

1. I love open source software, I should support it by using an open
platform.
2. I run my own GitLab instance for personal projects that
aren't yet ready to be open sourced. I'm really digging the interface.
3. I don't want or need [politics mixed my with code](https://news.ycombinator.com/item?id=10043668).
