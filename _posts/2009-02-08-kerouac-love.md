---
title: "Kerouac = Love"
layout: "post"
date: "2009-02-08 23:40:00"
categories: [News]
---

_"Listen: 'They danced down the streets like dingledodies, and I shambled
after as I've been doing all my life after people who interest me, because the
only people for me are the mad ones, the ones who are mad to live, mad to
talk, mad to be saved, desirous of everything at the same time, the ones that
never yawn or say a commonplace thing, but burn, burn, burn like fabulous
yellow roman candles exploding like spiders across the stars and in the middle
you see the blue centerlight pop and everybody goes "Awww!"'"_  
**-Dean Moriarty**  

One of my favorite quotes of all time, and one of my favorite authors of all time. Do yourself a favor, pick up this gem, and go do some research about it!  

<http://en.wikipedia.org/wiki/Dean_Moriarty>  
<http://en.wikipedia.org/wiki/On_the_Road>
