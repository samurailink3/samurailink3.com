---
title: "SnapPages: Website creation for the rest of us."
layout: "post"
date: "2009-04-06 21:59:00"
categories: [Link]
---

[{% img center ../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-1.png %}](../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-1.png)

Have you ever wanted to build a webpage without bothering to need Dreamweaver,
Frontpage (also known as 'complete shit'), or an open source solution, like
[Kompozer](http://kompozer.net/)? I've got the answer for you:
[SnapPages!](http://www.snappages.com/) This isn't a site for you hard-core
hand-tweaked code people, nor is it a solution for people that need an
relatively large amount of space to play with. But for everyone else just
looking for a website building utility that comes with more power than the
others, you've found your fit.

[{% img center ../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-2.png %}](../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-2.png)

[SnapPages](http://snappages.com) makes every page a drag and drop effort.
Pick the piece you want from the sidebar, drag it over, then click it and edit
its options. [SnapPages](http://snappages.com) has an amazing amount of
content to choose from and modify. If you want, you can even use the built in
banner-creation utility to create modified and animated banner logos for your
website.

[{% img center ../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-3.png %}](../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-3.png)

But you don't only get to create simple pages, you can choose from themes,
blogs, photo galleries, shared, public, or private calendars, or pages that
only your friends can see.  

[{% img center ../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-4.png %}](../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-4.png)

Don't know exactly where to get started? Let the "Page Templates" fuel your
curiosity. From profile pages to contact forms, this is pretty much as easy as
it gets.

[{% img center ../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-5.png %}](../../../../../images/2009-04-06-snappages-website-creation-for-rest-of-5.png)

Go ahead! [Give it a try!](http://snappages.com) The only thing you need to be
concerned about is that some of the themes require a paid membership, but the
free stuff works well enough for personal pages. A great option for those
looking to create a webpage for personal use, but aren't willing to grab a
dedicated server or hosting plan. Sweet, simple, sexy, SnapPages brings Web
Design to the average user.
