---
layout: post
title: "Git Training and Reference Links"
date: 2015-08-26 18:00:00 EDT
comments: true
categories: [Tutorials]
---

_Updated: 2015-12-04_

## Git Training

Here's a collection of links about Git that I've found
useful. I've been pretty involved in teaching people about
version control recently and have used these resources to
teach myself. If you want to see another link added, leave a
comment or issue a [merge
request](https://gitlab.com/samurailink3/samurailink3.com/).
I'll make sure you're credited on this post.

### [Try GitHub](https://try.github.io/levels/1/challenges/1)

A fantastic first-run introduction to git. Covers just the
basics. Completely interactive and all training takes place
inside the browser.

### [Codecademy: Learn Git](https://www.codecademy.com/learn/learn-git)

I really love Codecademy, they have tons of free courses ranging from
programming languages, to frameworks, to APIs, and now onto version control. A
highly-interactive tutorial, just like Try GitHub, just a bit more in-depth.
Highly recommended.

### [Git Immersion](http://gitimmersion.com/)

A great VERY in-depth tutorial for git. Covers lots of
subjects, but is quite text-heavy. Relies on local git and
having Ruby installed.

### [Linux Academy](https://linuxacademy.com/cp/modules/view/id/36)

If you have a Linux Academy subscription, I highly recommend
this git course. It walks you through Git, GitLab, and
GitHub. From basic usage to management.

### [Git SCM Book](http://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)

Another VERY in-depth book about git. Always up-to-date, but
a bit dry. It isn't written in a tutorial-style, it's more
of a tech book. Good for some people, bad for others. I use
this site as a reference _all the time_.

### [Udemy Git Tutorial](https://blog.udemy.com/git-tutorial-a-comprehensive-guide/)

If you learn from reading textbook examples and diagrams, Udemy has a course
available for Git training as well. It's a bit wordy, but the content is solid.

## Reference Links

### [Your Real-World Git Cheat Sheet ](http://www.linux.com/learn/tutorials/864504-your-real-world-git-cheat-sheet-)

A super-light cheat sheet for everyday git operations. If you want a simple
walkthrough of the most basic git commands, or just forget when to use `git
revert`, check this page out.

### [GitRef](http://gitref.org/)

An absolutely wonderful single-page reference. The
information isn't too dense, and will be very helpful to
those new to git who just need to remember a few commands or
remember the syntax of something. Totally
bookmark-toolbar-worthy.

### [Git SCM Site](http://www.git-scm.com/)

Not a single-page reference by any stretch of the
imagination. This site has _everything_ you could ever need.
Combine your favorite search engine and
`site:http://www.git-scm.com/` for some truly stellar search
results.

## Commit Messages

### [Why good commit messages matter](http://chris.beams.io/posts/git-commit/)

A lesson on why commit messages are the most important thing
to get right when using git. Commit messages are your
messages to the universe, important, meaningful, they stand
on their own. Commit messages as art.

### [Utter Disregard for Git Commit History](http://zachholman.com/posts/git-commit-history/)

Who cares about commit messages? Seriously, no one cares.
Put anything you want into them, the pull/merge request is
what matters! Have the discussion, collect :+1:s and :-1:s,
[gather in-line code
comments](https://about.gitlab.com/images/git_flow/mr_inline_comments.png),
and hash it out there. Anyway, the view is better from the
merge request.

### Note:

You should probably read both links above and make your
decision. Software development and version control
especially is not one-size-fits-all. Some shops will focus
on the commit itself, others value the pull/merge request
and attached discussion. Some shops are literally one guy
and his side project that no one will ever look at so who
the hell cares anyway? Do the research, try things out, pick
the best method for you and your team.

## Branching Models

### [GitFlow](http://nvie.com/posts/a-successful-git-branching-model/)

Dubbed "Git Flow", this model is one of the biggest original
git management workflows. I've used it for countless
projects and it works pretty well. The only real downside is
that it can be overbearing for smaller projects or teams.

### [GitHub Flow](https://guides.github.com/introduction/flow/index.html)

A very simple Git Flow model, without the baggage. Companies
without proper testing or CI integration _should not_ use
GitHub Flow. It really is suited for internet-based services
and projects.

There's one rule: master is always production-ready and deployable.

The flow works like this:

1. Create a branch
2. Make your commits
3. Open a pull/merge request
4. Everyone discusses the proposed change on GitHub/GitLab
  * You make changes as necessary, adding commits to the pull/merge request
5. Deploy your changes, make sure nothing breaks (either manually or through continuous integration)
6. Merge your code into master

And from there the process starts over.

### [GitLab Flow](http://doc.gitlab.com/ce/workflow/gitlab_flow.html)

GitLab flow is a teeny tiny bit more complex than GitHub
flow, but is still quite simple compared to Git Flow. The
only difference is that instead of a single master branch,
you have different environment branches (with master being
deployed to your staging or pre-production environment with
every commit), while all development happens on master via
pull/merge requests.

Here's how the flow breaks down:

1. Create a branch
2. Make your commits
3. Open a pull/merge request
4. Everyone discusses the proposed change on GitHub/GitLab
  * You make changes as necessary, adding commits to the pull/merge request
5. Deploy your changes, make sure nothing breaks (either manually or through continuous integration)
6. Merge your code into master
7. Do your testing in staging
8. If staging looks good, merge staging to pre-production
9. If pre-production looks good, merge pre-production to production

This process ensures that all features are stage-gated
through each environment, cutting down on potential problems
or downtime.

GitLab Flow also has the concept of release branches, but
those are only useful if you're releasing versioned software
to the outside world. Check out the link to learn more.

## Additional Thoughts:

If you have an interest in this kind of work, you'd better
check out [Semantic Versioning](http://semver.org/). It's a
great way to version software and keep outside developers
happy. Many open source projects are now using this system.
It's way better than the old [odd-numbered-dev-versions
system](https://en.wikipedia.org/wiki/Software_versioning#Odd-numbered_versions_for_development_releases).
