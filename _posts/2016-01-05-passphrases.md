---
layout: post
title: "Passphrases"
date: 2016-01-05 04:13:00 EST
comments: true
categories: [Security]
bg: false
---

### Passwords are so last year

_Passphrases_ are the new hotness. Who cares about upper case, lower case,
symbols, and numbers? Not me.

**Caveats:**

* This isn't a panacea
* It will not stop all the evils of the world
* If the site you use stores your credentials in plain text, you're as good as
  done
* Dictionary attacks happen
* This is just some brute-force math that shows length is more important than
  artificial complexity
* Phrase-Dictionary attacks do happen
  * **DO NOT** use a Bible verse or song lyric
  * **DO NOT** use a sports team
  * **DO NOT** use a famous quote
  * You must be only slightly creative and make a slightly original sentence

_Time to crack is based on a massive cracking array that runs one hundred
trillion guesses per second._ (Thanks to [GRCs password
haystacks](https://www.grc.com/haystack.htm) page for the calculations)

#### Take this password, typical, classical: `tha2uy2Ieti+`

* Length: 12 characters
* Categories: lower-case, upper-case, numbers, symbols
* **Time to crack:** 1.74 centuries

#### And this one, a bit longer: `ae3chav3Ho{cik6g`

* Length: 16 characters
* Categories: lower-case, upper-case, numbers, symbols
* **Time to crack:** 1.41 hundred million centuries

#### Let's get crazy: `pahzoon2uCh9phoS'iSeeBa`

* Length: 23 characters
* Categories: lower-case, upper-case, numbers, symbols
* **Time to crack:** 9.88 billion trillion centuries

#### And this passphrase: `unlock syntax for tailspin`

* Length: 26 characters
* Categories: lower-case
* **Time to crack:** 35.64 billion trillion centuries

#### And this passphrase, but two words longer: `unlock syntax for massive director tailspin`

* Length: 43 characters
* Categories: lower-case
* **Time to crack:** 45.34 thousand trillion trillion trillion trillion
  centuries

The evidence is clear, the four-word passphrase is not only easier to remember,
but it's even more secure than the 23-character all-random password. Let's keep
this in mind when choosing a new root _passphrase_.

#### Storing the passphrase

You should be using a [password
manager](http://keepass.info/news/n150809_2.30.html) by now. Also, if you're
storing the passphrases securely, why not make a 64-character random password
for everything? Use a nice passphrase as your master password.
