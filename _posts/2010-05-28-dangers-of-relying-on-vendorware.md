---
title: "The Dangers of Relying on \"Vendorware\""
layout: "post"
date: "2010-05-28 20:52:00"
categories: [Rants]
---

In my experience, most businesses have a major reliance on Vendor Software. In
case you don't know what vendor software is, let me take a quick minute to
explain it _(If you know already, skip the to the next paragraph)_. Vendor
software is a software package that large businesses/organizations either
lease out (on contract) or buy from a software vendor. These software packages
can come with bundled software support, so if a problem were to arise, the
business could call the software developers and obtain support and backup on a
particular problem. This support isn't free, most of the time, and on
occasion, not very helpful. The businesses that choose to implement
'vendorware' are usually lacking in staffing, technical ability, or both.
Vendor software can give confidence to an otherwise technically-inexperienced
business. That is the selling point, at least.  
But this isn't to say vendorware is without its flaws. By the very nature of
purchased, leased, or contractually-bound software, there are many many
pitfalls a business can (and should expect) to encounter.  

For instance:  

* **The vendor goes out of business:**  

This happens all the time in the tech industry, a start-up gets some funding,
builds a ton of code, implements it, then falls apart. Its all fine and dandy
if that start-up is a web-property or other piece of unwanted software, but
what if it is a core component of your network? You as a company just lost
your support for email/groups/calendars because a company couldn't quite keep
its head above water. This wouldn't really be a problem if it weren't for my
next point.  

* **The vast majority of vendorware cannot be modified:**  

Ok, so what if Company ABC went out of business... its just an email system,
lets just modify it ourselves, build a better mousetrap, and rely on our own
power to get us through. **Wrong.** The vast majority of vendorware is
contractually-bound to be non-modifiable. There really isn't any reason that
you _couldn't_ modify the code, but if you were to attempt it, the now-in-the-
red company could sue you for an undisclosed sum. Most vendors don't like
their software to be modified beyond their own constraints. There are a few
reasons for this, some rational, some business-minded:  

* **The vendors don't want to support some crazy hack-job the business admins have put in place to make their system work the way they want it to.**  

**Verdict: _Rational_**  

**Explanation:** If each group of developers were made to clean up after every
wanna-be computer scientist's screwed-up code, they would never get any real
work done. If a business wants help and support, they need to play by the
vendor's rules.  

* **Vendors don't want businesses building features into their software set that they can sell later.  

Verdict: _Business-Minded_**  

**Explanation:** Take 'Business-Minded' as you want. Personally, I don't like
profit margins or bottom lines, I like what works and what makes sense in each
individual situation. Vendors don't want businesses building in features that
they can sell later as upgrades or additional (purchasable) plugins. _Want the
chat feature? Only $9.95 per month added to your five-year contract! Want to
attach multiple files to an email? A low low flat rate of $200 per year for this
must-have feature!!_ This is annoying. This isn't how software should work. This
is software sales at their worst.  

* **To use vendorware, you must sign a contract.**  

Software vendors want guaranteed payments, strong-arming customers into lengthy,
expensive-to-break contracts. Vendor software is never as simple as: _Purchase
software, install software, be happy._ Most of the time, vendor software
includes early contract termination fees, do-not-edit requirements, and various
clauses designed to limit that business' freedom with the software.  

* The vast majority of the time, vendorware uses closed-systems while ignoring
  open-standards.   Some companies want so badly to keep a company's business
  that they will do everything they can inside the software to keep you from
  moving your data to any competition. From storing your data on inaccessible
  servers in their data centers, to using proprietary file-types that prevent
  exporting your data; some vendors don't allow you to move _your_ data
  elsewhere. Sometimes tools do exist to move your data from a proprietary file
  format to a standards-based one, but these are often against the terms of the
  contract and fines are likely to be imposed.  


* **The tech industry changes far too often to stick with any one solution for too long.**  

If you've been around the tech industry (of have ever purchased an iPod) for
any amount of time, you know firsthand that the tech industry moves insanely
fast. What was brand new yesterday is tomorrow's bargain bin lining. This
isn't limited to just hardware either, software changes just as quickly. While
it is true that users want stability in their software, what they crave is
features. For example: What worked for a robust email solution yesterday has
been overshadowed by what Google Apps, or any other deployable webmail
solution is today.  

The point is this: The tech world doesn't stand around idly with a product
that is "good enough". A company's lock-in contract with a vendor prevents
them from participating in this evolution.  

* **The support from software vendors is either very slow or very poor.**   

A beautiful example of 'slow' is a company that was having problem with their
wireless network being buggy and randomly dropping wifi clients. Company A
called Wireless Inc. and set up an appointment. A week later, a few techs from
Wireless Inc. came to Company A to assess the situation.  After the
assessment, Wireless Inc. left to create a solution and would be back in a
week. One week later, Wireless Inc. introduces the bug fix to Company A, and
it doesn't work. The next week, Wireless Inc. sends another tech to assess the
issue. The next week, Wireless Inc. attempts another patch. To make this long,
drawn-out story shorter, Company A had their wireless network working 'well
enough' after 8 weeks of back and forth, assessments, appointments, and many
upset users. The real problem wasn't even fixed properly, it was just decided
that Wireless Inc. would put in a 'Band-Aid Fix' as a temporary solution,
because it was financially undesirable to solve the actual problem.  

I wish I could tell you that this was a one-time scenario, but the ugly truth
is that it happens all the time.  

* **Users are used to working with consumer-level technologies, vendorware is
  big, complex, and confusing to the end-user.**  

The last and most important point in why a business should avoid proprietary
vendor software is that it is confusing to users. Most computer users are
completely familiar with end-user tools for managing contacts, sending email,
sharing calendars, and writing instant messages. Users have expectations that
computers at their jobs should work as well (or better than), and as easily,
as the computers they have at home.  

The vast majority of the time, vendor software is big, confusing, and out-of-
touch with user expectations. The systems are developed with very specific
purposes in mind without considering the types of people that will be
interacting with the system day-in/day-out. As a business, if your email
system isn't as feature-rich or easy-to-use as Gmail or Yahoo Mail, you've
already failed your employees and your users in a big way.

There are many ways to accomplish these goals, including:

* Choosing a vendor that won't lock you into a lengthy commitment (in case your needs, or the industry itself changes).
* Making sure the vendor supports open standards and will allow you to export your data. The last thing you want to do as a company is get locked into a single software solution that you have no choice of getting out of.
* Choosing a vendor that already has experience with 'Normal users', something that users will be relieved to see and use.
* Choosing a vendor that is forward thinking and not satisfied by the status-quo. Choosing a forward thinking vendor will allow you to stay on the bleeding edge of features, while maintaining a support model.
* Choosing a vendor that will work on your time schedule and understand how important your employees/users are to you. If a vendor feels pressure to fix an issue when you call, you know you've chosen right. The last think you (or your users) need is a company that is slow to respond when problems arise.

The majority of these problems can be solved in two ways: Either by choosing a
vendor that supports these standards, or building your own system using free,
open-source tools that you can build upon and use for your business solutions.
If you're looking for examples of good companies to partner with for your
business tech, see the handy list below:  

* [Google Apps](http://www.google.com/apps/intl/en/business/index.html)
* [Microsoft Office Live for Small Business](http://smallbusiness.officelive.com/en-us/)
* [Yahoo! Small Business](http://smallbusiness.yahoo.com/)
