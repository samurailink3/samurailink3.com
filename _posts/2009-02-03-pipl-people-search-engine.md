---
title: "Pipl - The People Search Engine"
layout: "post"
date: "2009-02-03 17:42:00"
categories: [Link]
---

By now you all know that courts make their records public for those over the
magical age of 18. If you've ever received a speeding ticket, you're in a
publicly accessible internet database with the charge plastered all over some
webpage. But how much information is truly out there about you? And more
importantly, how easily accessible is it? If you've ever googled your name,
you've either been totally creeped out, or more likely, reassured. Google does
a lot of things very well, the one thing they suck at is _people search_. I
can search all day on Google for Tom Webster, and I think I'm the 18th on the
list. Not bad... but most everything else in the search results are not me.
Granted.. those with a more common name will be harder to search out, but
Google doesn't index court records, and if they do, they don't do it very
well. [Pipl](http://www.pipl.com/ "Pipl" ) (pronounced "people") is more than
just a public records search engine, its (as the name implies) a search engine
if you're looking for one thing and one thing only: **A person**.
[Pipl](http://www.pipl.com/ "Pipl" ) searches not only court records, but they
also search public government databases, tons of social networking websites,
photo websites (such as [flickr](http://www.flickr.com/ "flickr" )),
LexisNexis, Amazon.com public wish lists, and many other places to grab
information about a particular person. Its an absolutely amazing tool if
you're an employer, and a terrifying tool if you're a person who's running
from what's available in the public record. [Pipl](http://www.pipl.com/ "Pipl"
) is a wonderful technical achievement for making information more easily
accessable, but its also a bit unnerving. I have a [FriendFeed
Profile](http://www.friendfeed.com/samurailink3 "FriendFeed Profile" ) which I
laughably nickname "Stalker 2.0", but its never quite revealed this much
information. Again.. the less common the search term, the better results
you'll get. Try it out! I'm sure you'll be digging up terrible family secrets
in no time!

[http://www.pipl.com](http://www.pipl.com/ "http://www.pipl.com" ) 
