---
layout: post
title: "New Theme: Amber"
date: 2015-10-28 05:30:00 EDT
comments: true
categories: [News]
bg: 2015-10-28-new-theme-amber-bg.jpg
---

_Thanks to
[Stuart Webster](https://www.flickr.com/photos/stuartwebster/) for the
[awesome background!](https://www.flickr.com/photos/stuartwebster/4177602386/) -
[CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/)_

As you may have noticed (unless you're reading this through the RSS
feed or a screen reader), this site has undergone a major theme
update. I'm calling it "Amber", because of the color choice. I took
inspiration from various places on the web to come up with this
design. This still uses the awesome
[Twitter Bootstrap](http://getbootstrap.com/) under the hood. It just
makes things too easy to build to get rid of.

My favorite feature, aside from the cleaned up navigation and
better-looking site, is the per-post-backgrounds. This is accomplished
with the code below. Pretty simple, if a `bg` element isn't present
(or set to `false`), the default background is shown, otherwise show
the image specified in the `bg` YAML Front Matter item.

```html
{{"{% if page.bg "}}%}
  <header style="background-image: url(/images/{{"{{page.bg"}}}});">
{{"{% else "}}%}
 <header style="background-image: url(/images/bg.jpg);">
{{"{% endif "}}%}
```

I've gotten rid of glyphicons in favor of
[FontAwesome](http://fontawesome.io/). It's way better to have just
one icon-font library instead of two, faster page loads.

One of my favorite features (that was made before this theme was
launched) is the
[compressed cache-busting asset-cruncher](https://gitlab.com/samurailink3/samurailink3.com/blob/master/_plugins/assets.rb). The
plugin is stupid, it takes all of your CSS, JS, and fonts from
`_assets`, compresses what it can, then copies the compressed assets
into `assets` that Jekyll uses to build the site. The result is a
single CSS and a single JS file are used, brining the number of total
requests down and site speed up. This is hugely helpful until
[HTTP/2](https://en.wikipedia.org/wiki/HTTP_2.0) takes over
everywhere. The plugin isn't perfect by any stretch of the
imagination, it really needs to be tuned up to run only when it's
supposed to. Right now, it doesn't re-fire when Jekyll runs in
`--watch` mode, meaning you have to `CTRL+C` and re-launch `jekyll
serve` for CSS/JS updates to take effect on your local system. Merge
requests are appreciated if you have the time.

As always, you can get the code to my site on
[GitLab](https://gitlab.com/samurailink3/samurailink3.com/) and if
you'd like to base your Jekyll site off of mine, you can do so easily
with the
[deployable version](https://gitlab.com/samurailink3/samurailink3.com/tree/deployable/).
