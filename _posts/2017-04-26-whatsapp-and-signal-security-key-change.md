---
layout: post
title: "WhatsApp and Signal Security Key Change"
date: 2017-04-26 20:11:14 PDT
categories: [News]
emoji: true
bg: false
---

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

My WhatsApp and Signal security keys are changing.
I am rebuilding my phone, going from a custom rom
to the official Google firmware.
Cheers!
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEN+jPAm6swpX57Jy5y3RCc+2g4LsFAlkBYbAACgkQy3RCc+2g
4LsS4hAA3PCktvvrEf0lPi7bOVpZ5R7TfEZTFDDJxwxEI7VLkspFglnwb5h4YoYC
R6m/g/wF66XURaCMPQu9Cg7/f+YLhE/IkzpeBuIGkgN2Si83v1YpPJGH18glDtBC
sxW/JdmZvr+5+U9i0Iz0znGwupZssXp0SaYJE5qRT2hWsMApur2QEcGuqnTTX2W7
MiKnOdrbx0TVL4/JFDnWJzgNBqrWW2zvyqYO+79rzUNiOl6AW4RA1L62JFb28arZ
7ZeJibz50WAKkjJoXxk9SO8X+hZvcpF5f6D68D/zaYBmP2z8LiKD4KW48x3MIa4r
AUm+L0sp32HcATPkxNwEg5d6l4Fl6Dw3rhyBNyL/5VRoZ6rPb87xFuEk3yANig+O
Qs9hyTkFdxS0U9Xt1QOTWZU/EPgXalPiHO5gBd4Sx4AJSP+YAgUb0ebBH9WOVZuq
b/kdl4g/OstKuwAdsPQOk/K9o5P9uW44r1TjuLKrdInSxueaFmL1s7nVUr5RSDa6
1veZNtv/qf8azk078qF8i3GLmlVi8viKyf0JMQV/twK5iW0s1VDrz+DtvjnFdFyi
zbOavaueU0sJIEs0MnQ9So72hYar6xvg/3qC6/DDD8oQvSS46Qqx1sL89kQaAMjI
Hf3EFhjjgr5LDRIN18cmFQuuU0mjYoxcmco1RqFQM+9xB7k5vQg=
=mGl0
-----END PGP SIGNATURE-----
```
