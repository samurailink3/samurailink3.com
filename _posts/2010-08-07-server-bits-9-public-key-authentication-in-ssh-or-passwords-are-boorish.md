---
layout: post
title: "Server-Bits #9: Public Key Authentication in SSH or Passwords are Boorish"
date: 2010-08-07 11:44:00 EDT
comments: true
categories: [Tutorials, Linux]
---

Server-Bits #9: Public Key Authentication in SSH or Passwords are Boorish
In this Server-Bits tutorial, I'll show you a real time-saver when it
comes to SSH and anything connected to SSH. To put it simply, public key
encryption in SSH is where you don't need to log into an SSH account
because the public key (stored on the server) matches your private key
(stored on the client machine), and it logs you into your account.
Because anyone with your private key can appear to be you and gain access
to your account, it is extremely important to guard your private key with
your life. The public key can float around the internet for all time
without any danger to yourself, your accounts, or your private key, as
public/private key encryption is very secure.


What this all translates to in layman's terms is that public key
encryption allows you to securely log into your server without using a
password. If this freaks you out at all, don't worry, you aren't alone.
But don't worry, as long as you're creating keys from your user account
and not from root, the risk of huge damaging effects is minimized, as you
still need a sudo password to do any serious damage (provided that your
server is properly configured). Of course, to take full advantage of
Public Key Authentication, you will be using two computers, but all of
the key generation and even the copy will be performed client-side (The
computer you want to have remote, password-less access).

The first step to generating a public/private key pair is to run this
command (which we will break down into detail):

`ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa -C "Your Comments"`

_This is how the command breaks down:_

-b: This is the number of bits to be used in the key. This number can be as low as 768, but since we're running a server, lets be overly-paranoid and use 4096.  
-t: The type of algorithm we will be using. In this example, we'll be using [RSA](http://en.wikipedia.org/wiki/RSA) for our key generation, but you also have the choice of [DSA](http://en.wikipedia.org/wiki/Digital_Signature_Algorithm), but in that case, you will need to make your key exactly 1024-bit.  
-f: This option specifies the file in which the key will be saved.  
-C: This allows you to specify a comment to go at the end of each key. This is important because you will most likely have several keys floating around (we will go into the why this is a good idea later) and if you need to void a key, it is very hard to distinguish which key is which.  

_You can specify a passphrase for each key if you wish. To log into the
server with this key, you will need to type in the passphrase before the
key will be unlocked. The downside to this is that by using a key, you
are trying to move away from having to use a password, but the upside is
that you can leave this key on a less-secure network (your office/school
network) with greater ease-of-mind. You do have the ability to use a
blank passphrase, but that is only recommended for systems you completely
trust (your completely encrypted laptop, for instance)._

Next, you need to make sure that no one else on the system can read your
private key.

`chmod -R 600 ~/.ssh/`

Now that we have a key on our client machine that is readable by us, we
need to pipe it over to our username on the remote server.

`cat ~/.ssh/id_rsa.pub | ssh username@server.address.com 'cat >> ~/.ssh/authorized_keys'`

_This is how the command breaks down:_

cat: This command just spits out any data in a file to the terminal. You can redirect the output to other files, however, which is exactly what we are doing here.  
The ssh login bit logs us into the remote server under our remote username, then gives a command to the terminal on that end. Our previous text output from the client machine is being redirected through 'cat' server-side, then it is appending the new text to the end of the file located at '~/.ssh/authorized_keys'.  

Most of the time, when an SSH login is established, a key is looked for
first, before the password. How does the system know who is who when
using a key? It looks in the 'authorized_keys' file. Any public key
located in that file is a match, and if is corresponds correctly with the
matching private key, you are logged in using public key encryption.

For our server system, we don't really need any additional configuration,
as most Linux distributions (including Ubuntu) automatically accept
private keys through SSH. The really cool part about this is that you can
now mount remote directories without having to put in a password, making
bash scripts endlessly useful. For instance, you could set up a
completely encrypted backup system over SSH (I'll be writing about this
later). To learn more about Public Key Cryptography, head over the
[Wikipedia page on it](https://en.wikipedia.org/wiki/Public-key_cryptography). Very wonderfully written article.

Thanks to [Utah.edu](http://www.cs.utah.edu/%7Ebigler/code/sshkeys.html%20) for the key-copy command.

_About Server-Bits:_

If you've ever wanted to get started building a server, right in your own
backyard/kitchen/closet/mother's closet/mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn,
this will give you everything you need to build that monster-server
you've dreamed of. **My goal: To give you a working server, for free,
that you can use daily.**
