---
layout: post
title: "AD Unlocker: Unlock Active Directory User Batch Scripts"
date: 2013-04-24 12:52 EDT
comments: true
categories: [Code]
---

In a typical day at work, I run into a lot of locked user accounts,
mostly because of mistyped passwords or misbehaving applications, and
using ADUC (Active Directory Users and Computers) was getting to be a
hassle. ADUC is a very powerful tool, but often it takes a long time to
get started, and if the network conditions are less-than-ideal, you can
run into major issues with lag and commands not completing. I needed a
better way.

**Scripting!** The savior of us all. I decided to write two versions of
this script, one that operates with PowerShell (and Active Directory
module), and one that does not. The PowerShell version is much easier
to use, as you only need your own admin account details and the other
person's username. If you use the PowerShell-less version, you will
need the full User Distinguished Name, which most of us don't have
laying around. Use whichever you feel most comfortable with, and if you
think they can be improved, please, [be my
guest](https://gitlab.com/samurailink3/samurailink3.com/fork/new).

Here are the two files:

`AD Unlocker - No PowerShell Needed`

```powershell
@echo off

REM ABOUT: This file will unlock a user account that you specify the DN of.
REM REQUIREMENTS: Active Directory Account with Unlock permissions; DN of user you would like to unlock.

SET /P ADDomain=Your domain name:
SET /P AdminUser=Your admin username:
SET /P UserDN=Full User DN you would like unlocked:

echo "This file will unlock a user who's DN you specify."

runas /noprofile /user:%ADDomain%\%AdminUser% "dsmod user \"%UserDN%\" -disabled no"
pause
```

`AD Unlocker - Easier to Use`

```powershell
@echo off

REM ABOUT: This file will unlock a user account that you specify on the command line.
REM REQUIREMENTS: Powershell with ActiveDirectory Module; Active Directory Account with Unlock permissions.

echo "This file will unlock a user who's username (sAMAccountName) you specify."

SET /P ADDomain=Your domain name:
SET /P AdminUser=Your admin username:
SET /P UserToUnlock=Username you would like unlocked:

runas /noprofile /user:%ADDomain%\%AdminUser% "%windir%\system32\WindowsPowerShell\v1.0\powershell.exe -command import-module ActiveDirectory; Unlock-ADAccount -Identity %UserToUnlock%"

pause
```
