---
title: "Fizy! Music for the Rest of Us"
layout: "post"
date: "2009-03-24 15:26:00"
categories: [Link]
---

[{% img center ../../../../../images/2009-03-24-fizy-music-for-rest-of-us-1.png %}](../../../../../images/2009-03-24-fizy-music-for-rest-of-us-1.png)

Yet another "Web 2.0" app to write about... This time, its
[Fizy](http://fizy.com/)! Have you ever just wanted to hear one song to get it
out of your head, or look up an old classic, or just play one awesome song
over and over again? Well now you can... for now that is..

[Fizy](http://fizy.com/) is a super-simple, easy-to-use search engine / music
player that probably has a couple weeks to live. Don't get me wrong, I love
the idea, I love the style, I love the simplicity, I love the way the songs
just come up and play. Its fast, easy, and makes P2P just about useless if you
only have an itching to hear a song once or twice. The advertising
possibilities are endless! But then again... that's what people said about
[Pandora](http://www.pandora.com/). Now I love Pandora as much as the next
music junkie, but its become a lot less useful ever since the RIAA had their
way with it.

I really love the direction [Fizy](http://fizy.com/) is going, but I can't
help but wonder... how long can they keep this up?

[http://fizy.com/](http://fizy.com/)
