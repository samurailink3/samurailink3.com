---
title: "Tutorials?"
layout: "post"
date: "2009-07-13 04:20:00"
categories: [News]
---

[{% img center ../../../../../images/2009-07-13-tutorials-1.png %}](../../../../../images/2009-07-13-tutorials-1.png)

Looking for new content to write about. I've got an amount of time in my day
that I'd like to spend ranting, raving, and explaining about various projects
I've got on my plate. Only one problem: I have no idea where to begin. That's
why I need you to tell me. What do you want to read about? It could be
anything from, "I want to build a media server" to "How to synchronize various
folders/disks/filesystems easily" to "I want to make a DOOM server". Anything
that's doable and you think I could be pretty apt at explaining. Remember,
Linux is a plus, but anything that works across multiple operating systems is
great too. I could even cover programs or general "I want to do this..."
computer questions. Leave a comment! Go! Its up to you now.  
I'm just letting you know... we're counting on you.
