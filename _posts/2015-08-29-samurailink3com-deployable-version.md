---
layout: post
title: "SamuraiLink3.com: Deployable Version"
date: 2015-08-29 10:33:48 EDT
comments: true
categories: [Projects]
---

In case you wanted to use this site's theme easily, I've
made a [deployable
version](https://gitlab.com/samurailink3/samurailink3.com/blob/deployable/).
It is literally this site, but with all of my posts/pages
and data removed. Easy to check out and get started. I have
kept the rake tasks in there to make it easy to get started,
and modified the readme to walk you through how the site
functions. Hope it's useful!
