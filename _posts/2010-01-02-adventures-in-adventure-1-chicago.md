---
title: "Adventures in Adventure 1: Chicago Arrival"
layout: "post"
date: "2010-01-02 08:47:00"
categories: [News]
---

We are here in Chicago. We're staying in the [Chicago South Loop
Hotel](http://www.chicagosouthloophotel.com/), not the fanciest place around,
but it more than serves its purpose as a sleep/shower joint. Right now we're
online trying to figure out which pizza options are open to us at this time of
night (1:52 AM).  
New Info:  

  * Trains are $2.50 per ride, or $5 for a day pass.
  * Some of the reviewers over at [Hotels.com](http://hotels.com/) are whiny bastards (The room I'm in is dandy!).
  * In-Phone GPS systems _HATE_ Chicago streets.
  * I really really really want a cup of shitty diner coffee.

(2:23 AM) Apparently there is no such thing as authentic Chicago-style pizza
past midnight.  

Video of hotel room **_[coming soon]_**:

<object height="344" width="425"><param name="movie" value="http://www.youtube.com/v/vxbVGw8WBIM&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/vxbVGw8WBIM&hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>
