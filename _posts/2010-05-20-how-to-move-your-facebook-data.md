---
title: "How-To: Move Your Facebook Data Elsewhere"
layout: "post"
date: "2010-05-20 03:07:00"
categories: [Tutorials]
---

[{% img center ../../../../../images/2010-05-20-how-to-move-your-facebook-data-1.jpg %}](../../../../../images/2010-05-20-how-to-move-your-facebook-data-1.jpg)

Since deactivating my Facebook account, I've come to realize that I really
only miss one thing about the service... the constant access to all of my
contacts email addresses and phone numbers. The really important ones, I've
had stored in my phone all along, but I've never bothered to make a backup of
the one's that I contact less often. As we all know, doing precise dentistry
with a nine-iron is easier than getting data out of Facebook, especially email
addresses. This is one of the biggest problems with Facebook, we put all of
our data into it, but they leave us with two very important questions
unanswered:_ _  

> **1\. We are made no guarantees what will be done with our data if we choose
to delete our accounts.  
> 2\. We can never export data that either, A: We have access to. Or B: That
we've made ourselves.**

 Have you ever tried to export a photo album from Facebook? You can't do it.
Well.... you can... If you right click and 'Save Image As' on every single
picture in that album. Its a pain in the ass to say the least. Third-party
developers have [created certain applications that Facebook have
banned](http://www.vincentcheung.ca/facedown/) due to "Terms of Service
Violations", meaning, "You are not allowed to remove your data from Facebook".  
The easiest way to extract and move your Facebook contacts to a standard [CSV
file](http://en.wikipedia.org/wiki/Comma-separated_values) that any mail
service/application can use is to [import your Facebook contacts to
Yahoo!](http://www.labnol.org/internet/export-email-addresses-from-
facebook/12970/). You can then export from Yahoo and import wherever you
please. Your contact data is now free from Facebook's clutches and yours to do
whatever the hell you want with it.  
For photo's, I used
[FaceDown](http://www.vincentcheung.ca/facedown/download.html), a super-
simple, but now banned Facebook Album downloader to pull all of my albums and
import them to [Google's Picasa Web](http://picasaweb.google.com/). You can
also use a program called [Fotobounce](http://fotobounce.com/index.php), but I
have only read about this. **I have not tried this program myself.** The
tutorial seems pretty straight-forward and easy, so go for it.  

Liberate your data.  
Contact Export How-To (Via Digital Inspiration):
<http://www.labnol.org/internet/export-email-addresses-from-facebook/12970/>  
Photo Export How-To (Via Digital Inspiration):
<http://www.labnol.org/software/download-facebook-photo-albums/9647/>  

This all said, [Facebook has admitted that their privacy models have become a
bit too complex and that they will soon be implementing simpler (and hopefully
better) privacy
controls.](http://www.informationweek.com/news/hardware/desktop/showArticle.jhtml?articleID=224900330&cid=RSSfeed_IWK_ALL)
So we'll see... I may end up coming back to Facebook if things work out for
the best.
