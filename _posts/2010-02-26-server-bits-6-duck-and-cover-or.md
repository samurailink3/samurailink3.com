---
title: "Server-Bits #6: Duck and Cover or: Protecting your users with VPN"
layout: "post"
date: "2010-02-26 18:53:00"
categories: [Tutorials, Linux]
---

In this world of turmoil, uncertainty, and
[Wireshark](http://www.wireshark.org/), web browsing in a public place is like
putting on an 80's hair metal concert, completely unsafe and everyone can see
what's going on. Unsecured wifi access points (And even those secured, but
using older protection) will show a great deal of your traffic in plain text
to anyone who loads up a program and 'sniffs' the air. Too embarrassed about
still using [Friendster](http://www.friendster.com/)? Then set your server up
as a VPN proxy and route all of your traffic through encrypted SSH. Completely
secured internet, on all of those untrusted networks. What are you waiting
for? Get to it!  

1. The first thing to do is set up a new user account that will only have VPN access. - 'sudo adduser vpnbuddy' (You can set this up with any user account you wish)  
2. Set up a new password for the user account (Make sure it is a lengthy/complicated password, the user will _not_ be able to log in and change it.
3. Next, we need to disable shell access for this account. We can change shells in /etc/passwd - 'sudo nano /etc/passwd'
  1. **NOTE!** This is if you would like the user to have _only_ VPN access, for standard SSH accounts, you don't need to change anything on their account, VPN is already available to them.
4. Navigate to the bottom of the file, to the line with the new username, go to the end of the line and change the '/bin/bash' to '/bin/false'.
5. Ctrl+O to save, hit enter.
6. Ctrl+X to exit.

That should be all the server-side work you have to do. With SSH already set
and configured, your newly created VPN user is ready to go! But.... That's the
easy part... Next, we have to work on deploying and setting up VPN for your
new users.  

Possibly the easiest way to accomplish this goal is to make a nice zip file of
the things we will be building, and give people a foolproof way to set up
their programs to take advantage of the secured connection.  

1. The first thing to do is go download Putty. Realistically, most of your users will be on Windows, so we need an SSH client to connect them over to your server and open up the VPN port. The perfect program to do the job would be [Putty](http://www.chiark.greenend.org.uk/%7Esgtatham/putty/).
2. create a new text file. You can do this in notepad, nano, gedit, [Notepad++](http://notepad-plus.sourceforge.net/uk/site.htm), whatever plain text editor you please.
3. You want to put this in your text file:  

> putty.exe -N -D 8888 CLIENTUSERNAMEHERE@pastanet.homelinux.com

4. Save the file as "Connect.bat"
5. Make a new folder somewhere and put the 'putty.exe' file in it. Next, throw your new 'Connect.bat' file in it.
6. Now... its time for some documentation... One of the slowest parts of running your own server. Yes... we all know _you_ know how to connect to SSH and make everything _just work_, but your users need you to hold their hands as they walk through this desolate land of technology. [Take a look at the documentation I've given my users...(Google Docs Link)](https://docs.google.com/fileview?id=0B1Rrmx2rA2GKYTcxMjhlYTUtMDkxNi00ZDYyLWI5NTAtZTVlY2M5ZmU5ZTI2&hl=en)  

As you can see, its easy to understand, easy to distribute, and in a format
that most anyone can open (PDF).  

7. Now users need to set up FireFox and Pidgin to use the VPN access. You can head [HERE](http://www.flickr.com/photos/samurailink3/sets/72157623318168109/) to run through the [Flickr screenshot tour of setting up FireFox and Pidgin](http://www.flickr.com/photos/samurailink3/sets/72157623318168109/) (Feel free to take my screenshots/documentation and use it for your own server, everything I make is under a [free-to-share Creative Commons license](http://creativecommons.org/licenses/by-nc-sa/3.0/))
8. After that, the only thing that is left is to acquire users and remain encrypted online. VPN works wonders for thwarting wifi eavesdroppers, suspicious network admins, and poorly built web filtering software. Have fun with it!

Again: Sorry this took so long to publish, its been a great deal of work
getting all of the documentation kinks worked out. Its a bit difficult to make
things perfectly easy and usable for standard users, and while running your
own server, you'll figure that out as well. Next up: Remote torrent
administration with Transmission.  

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**  
