---
title: "Server-Bits #5: Sockso Music Server and the Joys of SSL"
layout: "post"
date: "2010-01-16 16:43:00"
categories: [Tutorials, Linux]
---

[{% img center ../../../../../images/2010-01-16-server-bits-5-sockso-music-server-and-1.png %}](../../../../../images/2010-01-16-server-bits-5-sockso-music-server-and-1.png)

Sockso!! Sockso is a music streaming server program. It will take into account
any folders you have on your computer, index the music from them, then create
an online-accessible database which you can stream from. Your entire music
collection: Working anywhere the internet and flash will. Lets get started:  

1. Head over to <http://sockso.pu-gh.com/> and download Sockso. If you're running this through ssh, use this command to download it: **wget <http://sockso.googlecode.com/files/sockso-1.2.1.zip>**
2. Then extract the files - **unzip sockso-1.2.1.zip**
3. Then we'll move it to the /var directory - **mv sockso-1.2.1  /var**
4. Next, we'll jump into the sockso directory - **cd /var/sockso-1.2.1/**
5. But we can't run it just yet, we don't have the java runtime environment install on our server, but don't fret, its but a command away.
6. **sudo apt-get install openjdk-6-jre**
7. Now we can launch Sockso _**[This method of launching will launch Sockso with https forced and without a graphical interface, if you want to run unsecured or with a graphical interface, just remove either (or both) of those switches] **_-** cd /var/sockso-1.2.1 &amp;&amp; java -jar /var/sockso-1.2.1/sockso.jar "$@" --ssl --nogui**
8. Now you can type **'help'** to see your choice of commands. Running Sockso through the terminal limits you in some ways, to gain the full feature set, plug in a monitor and check out the sockso GUI. _**[The easiest way of launching the Sockso GUI is by running 'sh /var/sockso-1.2.1/linux.sh]**_
9. Now lets add a folder for Sockso to watch - **coladd /home/username/Music/**
10. This could take a while to add to Sockso... When it adds a folder, it indexes all of the files into a database that it can then pull from.
11. Next, we should add a user to the system - **useradd username password [emailaddress@hostnamehere.com](mailto:emailaddress@hostnamehere.com)**
12. The default port for Sockso is 4444, but you can change this with - **propset server.port [port number]**
13. All of the settings can be shown with **proplist** and changed with **propset**.
14. The only thing left to do now is test it! Head over to _**https**://[yourhostnamehere.com:4444](http://yourhostnamehere.com:4444/)_ to test it out! _**[You must use https if you have --ssl enabled, otherwise, use http]**._
15. I recommend using the commands **"propset users.disableRegistration yes" **and **"propset users.requireLogin yes"** to lock down your media streaming to only those users you specify, but this is up to you.
16. Have fun streaming!  

One of the cooler parts about running a linux box is the ability to add things
to what's called your  _.bashrc _file. This file can do anything from setting
environment variables, to running a startup command when you log in, to
setting program aliases. We we be doing the latter with Sockso. The command to
run Sockso securely is long, arduous, and complicated, by adding a single line
to the .bashrc file, we'll turn this command into a single word. **_[NOTE: You
will have to restart your bash session for the changes to take effect. This
means either logging out of ssh and logging back in, or closing the terminal
and opening a new one.]_**  

> To add a line to your .bashrc file, use the following command: **echo
"****cd /var/sockso-1.2.1 &amp;&amp; java -jar /var/sockso-1.2.1/sockso.jar
"$@" --ssl --nogui" &gt;&gt; ~/.bashrc  
> **  
>

Ok, let me explain this one.. The **echo** command just throws text on the
screen, but it can also be used with a redirect to throw text in a file as
well. Right now, you are throwing that big long command in quotes into the
file **~/.bashrc**. "~" or "Tilde" is a very short way to say "My Home
Directory" and the .bashrc is the text file located in your home directory.
Now, the part in the middle of these two, "&gt;&gt;", this is output
redirection. Instead of echo throwing text into the terminal, it will instead
append that text into the file of your choice (in this case, your .bashrc
file). Doubles (&gt;&gt;) will add the output to the end of the file, while a
single redirect (&gt;) will **completely replace the file**. You should
probably be careful with this one. You can redirect output for just about any
program in a bash shell, it comes in hand for many many things. For further
reading on [Bash Redirection head over
here](http://www.skorks.com/2009/09/output-redirection-with-bash/), a
wonderful noobie-friendly post.  

And that's it for Sockso. In the future, we'll be covering remote BitTorrent
administration, URL-rewriting, and Wiki's!  

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**  
