---
title: "Server-Bits: Interlude"
layout: "post"
date: "2010-02-15 21:44:00"
categories: [Tutorials, Linux]
---

I really have been working on the next set of posts, really... For two weeks.
The next post will include something equally different, but just as important
as sheer technical skill: The ability to work with users. Deployment,
documentation, dumbing-things-down. If you always count on the super-nerdy to
use what you build, you won't go very far, things need to be documented and
easy to use. Stay tuned, there is a lot to go through.
