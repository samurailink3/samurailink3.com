---
title: "The Magic of Android"
layout: "post"
date: "2010-05-22 15:12:00"
categories: [Rants]
---

[{% img center ../../../../../images/2010-05-22-magic-of-android-1.png %}](../../../../../images/2010-05-22-magic-of-android-1.png)

One year ago, no one would have argued that Apple was going to own and
dominate the consumer smartphone market while RIM was going to continue to
dominate the business world. But that was one year ago... [Recent NPD
numbers](http://www.gizmodo.com.au/2010/05/android-surpasses-iphones-sales-
for-q1-2010/) show that Android has pulled ahead of the iPhone in the US, but
what does this mean for consumers? If you've used both Apple and Android
handsets in the past year, I'm sure you've noticed one glaring flaw for
Android: The App Stores don't even begin to compare. The iPhone's App Store
has been around longer, has more mature (speaking of software code, here...)
applications, and a greater variety of both paid and free programs. While the
Android marketplace is still playing catchup, Apple is still king of the apps
on this front.  
Thankfully, all this is about to change. With the recent NPD numbers and the
booming success of [Verizon's
Droid](http://phones.verizonwireless.com/motorola/droid/) series, consumers
can expect the Android marketplace to start booming, and soon. Why? _Android
is where the numbers are_. RIM is on the decline, and Apple is slowing down.
If you're a developer, where do you want to sell? On Blackberry? You app is as
good as dead. Want to release to Apple? Sure, you've got quite the install-
base, but... Why not Android? The install base is obviously good enough to get
started, but you have one killer feature that the iPhone just doesn't have
right now: An upward trend. Not only will your app be available to a
respectable install base, but your potential customer base will continue to
grow. Another thing Android has over the iPhone OS is that: As a developer,
you are not limited to a single carrier. Develop an iPhone application that
pisses off AT&amp;T? [Prepare to get
banned.](http://www.zdnet.com/blog/apple/apple-removes-google-voice-based-
apps-from-the-app-store/4551) With Android, you are never limited to a single
carrier. [Sprint](http://now.sprint.com/android/),
[T-Mobile](http://www.t-mobile.com/shop/phones/Default.aspx?features=48CC3997-D234-4683-8B5A-F026B9DB5528&WT.z_unav=mst_shop_phones_android),
[AT&amp;T](http://www.wireless.att.com/cell-phone-service/cell-phone-
sales/promotion/ces.jsp), and
[Verizon](http://phones.verizonwireless.com/motorola/droid/#/home) all have
Android offerings ([Along with Google's own](http://www.google.com/phone/)),
and while, admittedly, some are
[better](http://phones.verizonwireless.com/htc/incredible/) than
[others](http://www.t-mobile.com/shop/phones/Cell-Phone-Detail.aspx?cell-
phone=T-Mobile-G1-with-Google-White) out there, as a developer, you should
never fear that you'll be shot down because a single carrier disagrees with
what you are doing on their network.  
Google is constantly making their Android OS [better and better with each
release](http://gizmodo.com/5543853/what-is-froyo) ([Apple as
well](http://www.engadget.com/2010/04/08/iphone-os-4-0-unveiled-shipping-this-
summer/), OS4 looks to be a much-needed shot in the arm for the aging OS3), so
there isn't any reason why you shouldn't be developing for a mobile platform.
If I had to pick one, I'd pick Android for the pure and simple fact that
[Google is beating Apple at their own game](http://gizmodo.com/5543794/google-
is-leapfrogging-apple). Its a great time to be a Googler, and I'm sure there
are even greater times ahead.
