---
layout: post
title: "Scripting FreeBSD pkg Installs"
date: 2016-05-18 17:34:41 EDT
comments: true
categories: [Tutorials]
emoji: false
bg: false
---

I needed to bootstrap `pkg` and install some packages in FreeBSD via a
script, but the `pkg` was getting hung up on the interactive
element. Thanks to
[this forum post](https://forums.freebsd.org/threads/36183/) (which
references
[this blog post](http://dan.langille.org/2013/12/06/bootstrapping-installing-pkg-on-freebsd-unattended-and-without-answering-yes/),
I now can.

Use `env ASSUME_ALWAYS_YES=YES pkg bootstrap` to get `pkg` up and
running, you can use that same `env` line to install packages without
any prompts, like this:

`env ASSUME_ALWAYS_YES=YES /usr/sbin/pkg install git zsh curl`

`pkg` won't prompt you and will install the packages. Great for
scripting or ezjail flavours.
