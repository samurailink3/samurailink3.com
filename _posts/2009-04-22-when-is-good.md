---
title: "When is Good?"
layout: "post"
date: "2009-04-22 22:19:00"
categories: [Link]
---

Have you ever wanted an easy way to set up a time to meet somewhere, do a
project, plan a big dinner, or just about anything else that involves a group
of people with busy schedules? If your group is part of the digital age, plan
your next dinner party/act of terrorism with **[When is
Good](http://www.whenisgood.net/ "When is Good" )**. A very simple site, but
it does the best job of getting you an answer with the least amount of hassle,
you don't even have to register!

First: Choose some dates and times that work for you by clicking them.

Next: Write down your super secret code (remember, you don't have to
register!).

Then you'll receive some links to throw around. Need to change the event? No
problem! Just be warned though... any already submitted responses will be
deleted. Send the invites off and wait for everyone to chime in!

When you get responses, [When is Good](http://www.whenisgood.net/ "When is
Good" ) will bring up a helpful little chart of who can and can't make it.

If your invitees leave comments, you'll be able to see those as well with a
mouseover.

Click on a day, and a synopsis of who can and can't make it will pop up.
Leaving you the amazing task of who you should leave behind.

And that concludes the screenshot tour of ["When is
Good"](http://www.whenisgood.net/ ""When is Good"" ) . A simple little web app
that comes in handy just often enough to be of use.  

[http://www.whenisgood.net/](http://www.whenisgood.net/)
