---
title: "Quick Notes on \"Thoughts on Flash\""
layout: "post"
date: "2010-05-01 13:18:00"
categories: [News]
---

Lets start with a quote from the [original
article](http://www.apple.com/hotnews/thoughts-on-flash/):

> "Adobe's Flash products are 100% proprietary. They are only available from
Adobe, and Adobe has sole authority as to their future enhancement, pricing,
etc. While Adobe’s Flash products are widely available, this does not mean
they are open, since they are controlled entirely by Adobe and available only
from Adobe. By almost any definition, Flash is a closed system."

Lets make some quick changes to this...

>  "~~Adobe's Flash~~ _Apple_ products are 100% proprietary. They are only
available from ~~Adobe~~ _Apple_, and ~~Adobe~~ _Apple_ has sole authority as
to their future enhancement, pricing, etc. While ~~Adobe’s Flash~~ _Apple's_
products are widely available, this does not mean they are open, since they
are controlled entirely by ~~Adobe~~ _Apple_ and available only from ~~Adobe~~
_Apple_. By almost any definition, ~~Flash~~ _any Apple software _is a closed
system."

Don't even get me started on the [App
Store](http://www.geekculture.com/joyoftech/joyarchives/1151.html).
Hey, Jobs, talk when you can [**put your app store where your mouth
is.**](http://www.android.com/market/)
[Just sayin'. ](http://smarterware.org/5863/steve-jobs-on-flash)
