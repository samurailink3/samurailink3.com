---
title: "Adventures in Adventure 3: The Museum of Science and Industry / McDonalds"
layout: "post"
date: "2010-01-05 17:37:00"
categories: [News]
---

Well, we decided to head out to the Museum of Science and Industry, we took a
ton of video, and I'll let that speak for itself:  

  <object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/D5ygOe4GB5I&hl=en_US&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/D5ygOe4GB5I&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object><br /><object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/2akRc71H96U&hl=en_US&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/2akRc71H96U&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>

We did head out to Chicago's Double-Decker McDonalds to grab some food before
we headed back to Ohio:  

<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/dNwfMQSFb-o&hl=en_US&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/dNwfMQSFb-o&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>

I'm working on some quick editing of another video of us just driving through
the city.  

And that's about it for our Chicago adventure. I'm sure we'll be taking
another trip up there sometime, but until then, enjoy those videos. Stay tuned
for more Server-Bits, I'll be finishing up another one here soon.
