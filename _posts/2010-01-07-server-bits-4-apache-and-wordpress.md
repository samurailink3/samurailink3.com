---
title: "Server-Bits #4: Apache and Wordpress"
layout: "post"
date: "2010-01-07 21:14:00"
categories: [Tutorials, Linux]
---

In this tutorial, I'll walk you through how to get your server hosting
webpages powered by [Wordpress](http://www.wordpress.org/). Wordpress is the
most powerful free blogging software suite out there. Powering everything from
CNN and BBC blogs to [72pc.com](http://72pc.com/) [Shameless Plug], Wordpress
is as easy as you want to make it, or as complex and extendable as you want it
to be.  

The first thing we need to do is install a web server and supporting server
applications. [Apache2](http://httpd.apache.org/) will serve up webpages,
[mySQL](http://www.mysql.com/) will contain the wordpress database and any
posts you make, [PHP](http://php.net/index.php) will handle the web-side
scripting needed by Wordpress.  

**For these commands, you should run these in a Bash shell, either via a terminal window or ssh.**  

1. 'sudo apt-get install apache2 mysql-server-5.1 php5 php5-mysql'
2. You should then make a password for the mySQL root user (The installer will automatically prompt you). **Remeber: Passwords should be lengthy and complicated.**
3. 'wget <http://wordpress.org/latest.tar.gz>' - This will download the very latest build of Wordpress to your home directory.
4. 'sudo mv latest.tar.gz /var/www' - This will move the _tarball_ (This is essentially the linux equivalent of a zip file) to the directory /var/www.
5. 'cd /var/www' - This will change your current directory to /var/www.
6. 'sudo tar xvvf latest.tar.gz' - This will extract the contents of latest.tar.gz.
7. 'sudo mysql_install_db' - This will install the database platform on your machine.
8. 'sudo mysql -u root -p' - This will bring you to the mysql prompt logged in as root.
9. 'CREATE DATABASE wordpress;' - This will create the wordpress database we will later use.
10. 'CREATE USER **_[enter your own username here];_**' - Creates a database user with the name you specify. **Remember this username!**
11. 'SET PASSWORD FOR **_[your username]_** = PASSWORD("**_[enter your own password here]_** ");' - This will set a password for the user you just created.
12. 'GRANT ALL PRIVILEGES ON wordpress.* TO _**[your username]**_@localhost IDENTIFIED BY '**_[enter your own password here] _**';' - This will grant the user you just created privileges to do whatever he/she wants on the wordpress database.
13. 'exit' - I think this command is self explanatory.
14. Next, open up FireFox and, if you are on your server, go to <http://localhost/wordpress/wp-admin/install.php>, otherwise use _<http://your.domain.com/>_wordpress/wp-admin/install.php.
15. The database name should be _wordpress_ by default, so you shouldn't have to change this. Fill in your username and password (The one you created at the mySQL promp), you can leave the Database Host and Table Prefix at their default values.
16. Log in with the randomly generated password, then change the admin password to something you will remember easier.
17. For security purposes, you should create a new wordpress user and use that to post.
18. To finish the install procedure, we need to change file permissons to be viewable externally. Run the command 'sudo chmod -R 755 /var/www/wordpress/'.
19. In FireFox, navigate to <http://localhost/wordpress/>. You should see the front page of your blog pop up!

Now... Wordpress is one of the most extendible  platforms on the web, you can
re-theme it, add plugins and widgets, control how editors/authors/readers
interact with your page, and a ton of other stuff.  

For themes, [go here](http://wordpress.org/extend/themes/), for plugins,
[here](http://wordpress.org/extend/plugins/).  

Now for the most important part: Content. Get to writing, building, posting,
uploading content that people will care about. This is now a public facing
web-server (As long as you have forwarded port 80), so get to building!  

Thanks to [Jonathan Moeller](http://www.jonathanmoeller.com/screed/?p=1261)
for parts of this guide!  

__About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
