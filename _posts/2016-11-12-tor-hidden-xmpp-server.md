---
layout: post
title: "Tor Hidden XMPP Server"
date: 2016-11-12 15:45:37 EST
comments: true
categories: [Projects]
emoji: true
bg: false
---

A few weeks ago I got on a kick and wanted to self-host a
[Signal](https://whispersystems.org/) server that used usernames instead of
phone numbers. Ideally, this could federate with other self-hosted Signal
systems as well. Unfortunately, this hasn't been easy, [Signal is open
source](https://github.com/whispersystems), but [isn't designed to be
federated](https://whispersystems.org/blog/the-ecosystem-is-moving/).

Signal rules, I love it, it's easy to use, and it is the best secure
communication app that is usable by normal people, not just crypto-nerds like
myself. But at the same time... I really wanted something I could stand up, run
on my own hardware, and tear down at will. I don't really have a huge use case,
I just wanted to see if I could.

One of the most widely-used federated messenging protocols,
[XMPP](https://xmpp.org/), is functional today, but not nearly user-friendly
enough or technically up-to-snuff when compared to messenging platforms like
[WhatsApp](https://www.whatsapp.com/), [Signal](https://whispersystems.org/), or
[Hangouts](https://hangouts.google.com/). Fortunately, for this use-case, it
works well enough (if not a bit rough around the edges). For end-to-end
encryption, XMPP classically relied on [Off-The-Record
(OTR)](https://en.wikipedia.org/wiki/Off-the-Record_Messaging) to keep
conversations secure. Sadly, OTR was pretty annoying to use in production and
liked to fail in odd ways. I didn't want to use OTR in my new system, I really
wanted to use the [Signal-style double ratchet end-to-end encryption
algorithm](https://en.wikipedia.org/wiki/Double_Ratchet_Algorithm) that [Moxie
Marlinspike](https://moxie.org/) pioneered.

Enter [OMEMO](https://conversations.im/omemo/). It utilizes the same
Signal-style end-to-end encryption, but wraps this in a way that it works with
XMPP servers and clients. [Check out the website for more
information](https://conversations.im/omemo/), but needless to say, it is
superior to OTR in many ways.

So what does this mean for my project and you? Well, I've designed an [Ansible
playbook for Debian (and Ubuntu) systems](https://gitlab.com/samurailink3/xmpp_hidden_service) that will set up a system running [the
Prosody XMPP server](https://prosody.im/) and configure
[Tor](https://www.torproject.org/) for you automatically (if you so desire).

**IMPORTANT NOTE!!** - As far as I can tell, multi-user chat **IS NOT ENCRYPTED
VIA OMEMO**. Disclaimer: XMPP is an old dog, it has some nasty edge-cases.
This is purely experimental. If you need Real Security: Use
[Signal](https://whispersystems.org/), Use [Tor](https://www.torproject.org/).

**IMPORTANT NOTE!!** - Configuring the system in general is out-of-scope for this
playbook. Some things that I usually do for all servers is set up [Debian
auto-upgrades](https://wiki.debian.org/UnattendedUpgrades), install some base
packages, [secure
ssh](https://stribika.github.io/2015/01/04/secure-secure-shell.html), and
configure IP Tables to block everything incoming (for tor servers).

**IMPORTANT NOTE!!** - When setting this up, make sure to follow [hidden service
best practices](https://samurailink3.com/blog/2016/05/14/an-introduction-to-tor-hidden-services/).
Don't administer the server over the internet, use a tor hidden service for
ssh. Don't let ssh listen on the default interface, only localhost. Use a
single-use ssh key to prevent identity verification (ssh public keys are
PUBLIC!). Do your research and read up on how to avoid leaking your identity
when running a hidden service.

**IMPORTANT NOTE!!** - By default, this server allows OPEN REGISTRATION! Anyone
can register a username on your server and connect to other XMPP servers with
it. For my default use case (providing an open secure communications channel
for all), this is fine. It may not be fine for you. Remember, this is on Tor,
there are bad people on Tor that _could_ use your server to post or store
**VERY** illegal data.

**IMPORTANT NOTE!!** - By default, no username is set up to be an administrator,
there is a loopback-only (not exported via the hidden service) telnet server
that you can use to administer the system in various ways. A tutorial is out
of scope for this post, but [you can read up on it
here](https://prosody.im/doc/console).

[Ansible Playbook For: XMPP Hidden Service](https://gitlab.com/samurailink3/xmpp_hidden_service)
