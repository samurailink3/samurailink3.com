---
layout: post
title: "Let's Encrypt Automation"
date: 2016-09-16 09:46:43 EDT
comments: true
categories: [Link]
emoji: true
bg: false
---

Originally I had drafted out this post to show and explain a shell script that I
created to automatically handle [Let's Encrypt](https://letsencrypt.org/)
certificate issuance and renewal, but honestly, it was a messy hack. A far
better solution is Hugo Landau's (hlandau)
[acmetool](https://github.com/hlandau/acme).

It is trivial to install, easy to use, and it keeps things updated automatically
(even without root if you choose). The official documentation is awesome and you
should check it out.

* [https://github.com/hlandau/acme](https://github.com/hlandau/acme)
* [https://hlandau.github.io/acme/](https://hlandau.github.io/acme/)
