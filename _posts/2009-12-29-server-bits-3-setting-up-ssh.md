---
title: "Server-Bits #3: Setting up SSH"
layout: "post"
date: "2009-12-29 14:58:00"
categories: [Tutorials, Linux]
---

[{% img center ../../../../../images/2009-12-29-server-bits-3-setting-up-ssh-1.png %}](../../../../../images/2009-12-29-server-bits-3-setting-up-ssh-1.png)

Now that you have a server built, the first thing we need to do is get SSH set
up. SSH will allow you to remote control your computer from anywhere that has
an internet connection. _You will need to forward port 22 on your router if
you want to hit it from the outside._  

1. Just a reminder: **Make sure your user password is lengthy and complicated. **The last thing you want is for someone to waltz into your machine using the password 'password'. SSH will give an attacker **direct access** to your entire machine. While it is an extremely useful tool, it can be utter hell if misued.
2. Next, open up a terminal. The command you want is 'sudo apt-get install openssh-server'.   
Now wait patiently for the program to finish installing.

3. If you'd like to test SSH, use the command 'ssh localhost', ssh should ask you if you would like to trust the server on the other end, then it should ask you for your password.
4. You have just established an SSH connection to your server. Congrats.
5. A few things to keep in mind about SSH: To give someone SSH access to your computer [Known as giving someone a **Shell Account**], you would create a new user as you would usually do (either through the GUI or the command 'adduser'), and they are automatically granted SSH access.
6. To establish an ssh connection, you will need a _Terminal Emulator_, the most popular program for Windows is called [PuTTY](http://www.chiark.greenend.org.uk/%7Esgtatham/putty/download.html). On any Linux or Mac OSX machine, you can just open up a terminal and type 'ssh [username@host.com](mailto:username@host.com)' to connect (You will be using your DynDNS hostname). If your username was 'samurailink3' and your domain was '[google.com](http://google.com/)', you could use 'ssh [samurailink3@google.com](mailto:samurailink3@google.com)' to establish a connection.


_Note:_ You don't have to use a domian, IP addresses work as well: 'ssh
[samurailink3@64.233.169.105](mailto:samurailink3@64.233.169.105)'.

**Cool things to try:**  

1. On machines that are running an X-Server (Most any Linux machine), you can remotely display graphical applications with '-X': 'ssh -X [samurailink3@google.com](mailto:samurailink3@google.com)', then I can run 'firefox', and the instance of FireFox over at [google.com](http://google.com/) will be displayed on this screen. _Note:_ This is stupidly slow. Really really slow. But it is secure.
2. SSH tunneling to secure otherwise insecure protocols (We will get into this in a later tutorial).
3. SFTP: You can use most any FTP program (Such as [FileZilla](http://filezilla-project.org/)) to easily transfer files to and from the host machine [Just make sure to specify port 22!].
4. If you want to set a pre-login banner to SSH, create a new text file [/etc/ssh/banner] and put any text you would like in that file, save, exit your text editor **{Protip: You can use the command 'nano' to edit text in a terminal, you can then use Ctrl+X to exit to editor, answer yes or no to whether or not you would like to save. Example: 'nano /etc/ssh/banner'}**. Now, edit your sshd config file [/etc/ssh/sshd_config] and add the line 'Banner /etc/ssh/banner'. Save and exit. Next time you attempt to log into SSH, you will be greeted by whatever text is in /etc/ssh/banner!
5. If you would like to set a post-login message to SSH, (On Ubuntu 9.10), you must first disable update-motd. You can do this by running 'sudo update-motd --disable'. Then edit the text file [/etc/motd] to say whatever you want. When a user successfully logs into SSH, they will see whatever is in this text file.
6. Remember: If you change any configuration, restart your SSH server with this command 'sudo /etc/init.d/ssh restart'. **{Protip: Changing a configuration file for a service in Linux doesn't mean you have to restart your computer, that's the Windows way of thinking. The vast majority of the time, all you need to do is restart the service. These scripts are located in /etc/init.d/. We will cover them when the time is right.}**

For now: Enjoy remote administration of your machine!  

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
