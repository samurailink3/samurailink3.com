---
layout: post
title: "Museum Madness Portable"
date: 2013-05-07 07:00 EDT
comments: true
categories: [Projects]
---

[{% img ../../../../../images/2013-05-07-museum-madness-portable-1.png %}](../../../../../images/2013-05-07-museum-madness-portable-1-full.png)

When I was a kid, I played a _ton_ of the game Museum Madness. An old
[MECC](http://en.wikipedia.org/wiki/MECC) game that was all about
learning various subjects, solving cool puzzles, and enjoying the
point-and-click adventure world. Unfortunately, the company eventually
went under, leaving the game as
[Abandonware](http://en.wikipedia.org/wiki/Abandonware). Getting this old
game to run isn't terrible on Windows 7, but unless you know your way
around [DosBox](http://www.dosbox.com/) or a command line interface, your
average user may run into trouble. To fix this problem, I created a
[double-click-to-run Museum Madness
launcher](https://gitlab.com/samurailink3/MuseumMadnessPortable). All of
my code is open source and freely available, the game itself is kind of
under a grey-ish abandonware type license, so I'm not entirely sure if
you can deconstruct it. I grabbed the game data from
[Abandonia.com](http://www.abandonia.com/en/games/479/Museum+Madness.html),
who host a **TON** of abandoned games, go check it out. In the mean time,
here's the game:

[GitLab Repo](https://gitlab.com/samurailink3/MuseumMadnessPortable)

[Windows XP/7 x86 and
x64](https://gitlab.com/samurailink3/MuseumMadnessPortable/repository/archive.zip?ref=windows) -
You can try it on other Window's platforms, I haven't tested it though.

[Linux
x64](https://gitlab.com/samurailink3/MuseumMadnessPortable/repository/archive.tar.gz?ref=linux_x64)

Want it ported to a different system? Do it, send me a pull request.
