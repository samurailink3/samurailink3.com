---
layout: post
title: "Git Branch in Bash Prompt"
date: 2013-07-22 14:02 EDT
comments: true
categories: [Code]
---

If you've wanted to see your current branch in Bash, check out this easy
`.bashrc` addition from [henrik](https://github.com/henrik). Just add the
code to the bottom of your `.bashrc` file and `source ~/.bashrc` to add
the changes.

```bash
# http://henrik.nyh.se/2008/12/git-dirty-prompt
# http://www.simplisticcomplexity.com/2008/03/13/show-your-git-branch-name-in-your-prompt/
#   username@Machine ~/dev/dir[master]$   # clean working directory
#   username@Machine ~/dev/dir[master*]$  # dirty working directory

function parse_git_dirty {
  [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit (working directory clean)" ]] && echo "*"
}
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/[\1$(parse_git_dirty)]/"
}
export PS1='\u@\h \[\033[1;33m\]\w\[\033[0m\]$(parse_git_branch)$ '
```
