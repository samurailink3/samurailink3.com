---
layout: post
title: "Facebook's Added Transparency"
date: 2010-10-06 19:51:00 EDT
comments: true
categories: [News]
---

Today Facebook has announced two major changes that directly combat
issues that power users of the site have  had since the website's
inception:

1. **The ability to export your data.**
2. **The ability organize friends into groups.**

The most important of these is the ability to export user data. Every
comment, picture, post, and status update, rolled into a ZIP file, given
directly to the user. **This is a game-changer.** Facebook can no longer
be called a _walled garden_. With the ability to take your data
elsewhere, users don't have to feel "stuck" on Facebook. Want to leave
and take your data elsewhere? Go to a social network that will allow you
to upload your Facebook ZIP file. Just like that, you've left Facebook
for (hopefully) greener pastures. Personally, I'll feel safer using
Facebook, now that I know my data isn't forever locked away in the
vaults. Facebook was one of the largest bastions of **data lock-in**, and
now that they've changed, they've set an example that will (and should)
be followed throughout the tech community.

While not as important as being able to leave, Facebook has also
introduced a new Groups feature. Groups isn't just a resurrected "Friend
Lists", groups will fundamentally change the way Facebook will operate.
What if you want to post a message to your college drinking gang, but
leave your family out of it? Before, it was convoluted. Creating the
list, managing it, sending a message to the list. Friend lists never
really felt like an A-List feature of Facebook. If groups work like they
should, this will bring Facebook much closer to allowing users to
separate their social networking actions to specific aspects of their
lives. Want to create a Thanksgiving Dinner event for just your Family
Group without your creepy Facebook-stalker seeing? Go right ahead. Like
the title of the GigaOM article: [New Facebook Groups Encourage Private
Interactions](http://gigaom.com/2010/10/06/facebook-groups-to-encourage-more-private-interactions/).
And it's true.


But what does this mean for startup projects like Diaspora? Hopefully not
too much. The difference between Diaspora and Facebook is Diaspora is
still an open project (albeit a very young one) and still has a chance of
doing these things better than Facebook. Diaspora was created with the
ideas of data-export and social-aspects from the very beginning, Facebook
was not. What may very well happen, though, is drive much-needed
developers away from the project, simply because they don't see the need
for it anymore. Only time will tell on this last point.

In the tech world, today's events seem like part of a recent trend: Once
super-closed companies are starting to tear down their walls. With [Apple
allowing a Google
Voice](http://mashable.com/2010/09/18/google-voice-iphone-apps/) and
Facebook letting users take their ball and go home, it seems that walled
gardens are finally starting to realize that open is not only better, but
more profitable as well. At-ease users are happy users.
