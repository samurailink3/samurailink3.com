---
layout: post
title: "Startup Apps in Gnome 3"
date: 2016-01-04 05:13:06 EST
comments: true
categories: [Linux, Link]
bg: false
---

I recently switched to Gnome 3 on my personal laptop. When Gnome 3
first launched, it was an utter train wreck. Completely unusable, poor
design choices, and seemingly purposely-built to serve a non-existent
straight-linux tablet market. While the tablet influences are still
around, the project has moved towards its original roots.

While Gnome 3 still isn't the perfect being that Gnome 2 (now MATE)
was, the tweak tool being part of the default install and the
extension library are helping make up for the shortcomings. One thing
I really wanted to do is add custom startup applications to my
session, this is accomplished via the tweak tool for now, since there
is no UI for session management (yes, really).

I found
[this great article](http://linuxandfriends.com/how-to-add-startup-programs-in-gnome-3/)
on how to do this, it involves creating a `app.desktop` file that can
be seen by the tweak tool. This means that for every command line
startup utility you want run on gnome session start, it needs a
`.desktop` file. Ridiculous, but it works.

In case the site ever disappears, I've paraphrased the content below:

Create a `app.desktop` file in `~/.config/autostart`, if this
directory doesn't exist, create it.

Here's the basic content of a `app.desktop` file:

```
[Desktop Entry]
Type=Application
Exec=/usr/bin/xflux -z 12345
Hidden=false
X-GNOME-Autostart-enabled=true
Name[en_US]=xflux
Name=xflux
Comment[en_US]=Screen color temperature changer
Comment=Screen color temperature changer
```

Then head over to the _Gnome Tweak Tool_ and go to the _Startup
Applications_ tab. From there, you can select the program for the
`app.desktop` file you just made. Repeat as necessary.
