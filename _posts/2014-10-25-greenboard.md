---
layout: post
title: "GreenBoard"
date: 2014-10-25 20:00 EDT
comments: true
categories: [Projects]
---

Here's a huge project that I'm happy to open source:
[GreenBoard](https://gitlab.com/samurailink3/GreenBoard). It's a simple
web app designed to be displayed on a vertically-oriented TV in a
manufacturing environment. The premise is simple, you take pre-run
measurements, if they come back green, run parts, if they come back red
or yellow, you should fix things before you run parts. Without delving
too much into specifics, it lets you specify and take measurements
against pieces of equipment, and get a quick look to see if they are
within the tolerance values you specify. This is probably best
illustrated with a picture:

[{% img ../../../../../images/2014-10-25-greenboard-1.png %}](../../../../../images/2014-10-25-greenboard-1.png)

Table cells are colored depending on whether or not they are
in-compliance with the values you have specified.

* Green Cells - Value is within the bounds and within the tolerance percentage.
* Yellow Cells - Value is within bounds, but is currently falling out of tolerance.
* Red Cells - Value is outside of specified bounds.

There are some existing products that do this, but nothing was open
source or generalized enough to fit the requesting company's
specifications, so we decided to build one ourselves.

This was designed with audit compliance in mind, so all tables are using
[PaperTrail](https://github.com/airblade/paper_trail). Even if a user
deletes the tables they control, you can still look back on history for
audit compliance purposes.

There's still a good bit of work to complete on this project, such as
building an API, pulling new values into the page in a modern way, not
using a jQuery polling hack (it's really nasty...), moving configuration
to environment variables with
[Figaro](https://github.com/laserlemon/figaro), and additional logon
methods (for all you Google Apps users out there). If you'd like to help
out, check out the [GitLab
Project](https://gitlab.com/samurailink3/GreenBoard). I am accepting
merge requests and can help out with deployment or other questions if
they come up. Have fun!
