---
title: "SEO: A Fake Science?"
layout: "post"
date: "2010-06-10 20:00:00"
categories: [Rants]
---

_**Disclaimer:** 99% of this post was taken from a series of back-and-forth
Facebook comments. The debate was extremely thought-provoking and raises some
interesting questions from both sides of the fence. In the interest of the
other individual, neither quotes, nor names will be posted regarding the
discussion, rather, the general feel and main points of the conversation will
be posted. There isn't a clear-cut victor at all, the point of this post is to
get us thinking about the internet and how it is built, structured, searched,
and consumed. To be honest, I thought about skipping this article entirely, as
these are extremely controversial talking points among web-folk. Think about
your own websites as you read on._

Recently I was involved in a Facebook debate about Search Engine Optimization
(SEO), and I align with Leo Laporte in [his thinking about
SEO](http://techguylabs.com/radio/ShowNotes/Show440#toc10). In case you don't
know what this is, I'll explain it to you: Search Engine Optimization - These
con-men promise you a number one spot on Google searches and the only thing
they do is change page titles. There isn't any way to game Google, but these
guys con people out of hundreds of dollars at a time with a snake-oil
solution.

My position is that SEO is a fake science. Sure, there are a few things to
make your site more search-friendly: Change your page titles to reflect your
content, put important things near the top, register your domain for more than
a year, and build a site map. Guess what? These are all things Google _itself_
suggests when you look up page rank information or use Google's Webmaster
Tools. There is no magical incantation to game Google. Not now, not ever. The
only way to get top results is to create rich content on a consistent basis.
How do you expect to game a constantly changing algorithm? The search
algorithm goes through multiple changes every month at the very least. If it
were that easy to game the results, you'd only find spam in search results.
SEO is utter trash. The only way to rise in search results is to create rich
content, bring users to your site, and create this rich content on a
consistent basis. This is also known as, the elusive and rare, "Having a
decent website you actually put work into." _Crazy idea, I know..._
[From some research on Google
Patents](http://www.buzzle.com/editorials/6-10-2005-71368.asp), the best way
to jump ahead in Google search results is to grow your site organically, there
aren't any magical keyword or link incantations or special header HTML one can
call on to give your site that search boost to propel it to the top, _it just
won't happen_.
The funny thing is, SEO's make tons of money every day by scamming people out
of their hard-earned cash by promising them a number one spot in search
results. At one point in time, there were plenty of people making huge amounts
of money from astrology, fortune telling, and alchemy. SEO is the exact same
'science'. These internet 'magicians' promise clients huge traffic boosts out
of arcane, secretive methods, that are _far, far too complicated_ for the
average person to learn. I'm sure we've all heard these stories before.
There are ways to help content become searchable, but a company buying out
keywords from linkfarms does nothing to get the top spots from Google. Buying
advertising is another thing entirely. Sure, spend thousands on AdWords, it
WILL drive traffic, but those aren't search results, those are sponsored
positions.
Just like those (at one point) 'sciences' of centuries past, plenty of people
put in plenty of time and made plenty of money, bit in the end, was anything
really accomplished? No.
Don't get caught up in this same trap.

Even with my beliefs, the fact remails: There are
[TONS](http://search.twitter.com/search?q=seo) of people out there that build
entire careers out of becoming SEO ninjas. To them, its a real job, doing
market research, finding keywords, categorizing and saving backlinks, and
using all of these at the proper time to avoid being categorized as spam. They
own tons of domains and use various page layout techniques and keyword
frequencies to get their job done.

**But does it actually work?** My money says, 'No'. But it does raise some interesting questions about search engines, web design, spam, and risk management. As web designers and construction-workers of the internet, we should ask ourselves these questions:

  * What works better? Raising a site organically, or building a site using a highly-organized series of backlinks?
  * Does repeating keywords drive traffic?
  * Will the new [Google Caffeine](http://googleblog.blogspot.com/2010/06/our-new-search-index-caffeine.html) make old, keyword-heavy content obsolete?
  * Do you see keyword-heavy or link-heavy pages as spam?
  * Can Google (or any other search entity) be gamed for a significant profit?
  * Is it worth the risk? If a site is flagged as spam on Google, you can guarantee the website is losing out on much more than it could ever gain.

Go ahead and post your answers in the comments, and if you want, ask your own
questions as well! Questions like this are important to the foundation of the
internet and how we think about search and quality content. One thing is for
certain: These aren't easy questions to answer, and I don't believe any one
person is qualified to answer them, but its the thought that counts.
