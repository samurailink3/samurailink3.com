---
layout: post
title: "Mechanical Keyboards"
date: 2016-01-23 21:40:10 EST
comments: true
categories: [Reviews]
bg: "2016-01-23-mechanical-keyboards-bg.jpg"
---

## Introduction

Mechanical keyboards. Oh hell yes. Clickity clackity. I will type on nothing
else as long as I live, given that I have the choice. In this post, I'm going to
tell you about two keyboards that I've bought and have been using for a while
and why you should take out a second or third mortgage just to type on these
beautiful pieces of modern machinery.

The first question you might be asking is "Why mechanical?". It's honestly a
hard thing to answer. It's just something you have to feel to truly know the
difference. I like to spend my money where I spend my time, and most of my
waking hours are spent in front of a screen bashing on a keyboard. Just the feel
alone is well worth the money.

The best way I can describe it is this:

Would you rather spend 8 hours a day sitting in this:

[{% img ../../../../../images/2016-01-23-mechanical-keyboards-1.jpg %}](../../../../../images/2016-01-23-mechanical-keyboards-1.jpg)

or this:

[{% img ../../../../../images/2016-01-23-mechanical-keyboards-2.jpg %}](../../../../../images/2016-01-23-mechanical-keyboards-2.jpg)

After typing on a mechanical keyboard for a while, every other keyboard feels
like using that plastic lawn chair for extended periods of time. The best way
I've heard it described is that all other rubberdome (non-mechanical) keyboards
feel like typing on mashed potatoes, a very apt comparison.

## CODE Keyboard

[{% img responsive ../../../../../images/2016-01-23-mechanical-keyboards-3.jpg %}](../../../../../images/2016-01-23-mechanical-keyboards-3.jpg)

The first mechanical keyboard I bought was the [CODE
Keyboard](http://codekeyboards.com/) with Cherry MX Clear switches. The keys
aren't too heavy, but they have a nice spring to them. If you're a heavy typer
like myself, you'll want to get some
[o-rings](http://www.wasdkeyboards.com/index.php/cherry-mx-rubber-o-ring-switch-dampeners-125pcs.html)
to help dampen the sound a bit. I ended up getting the red ones, but I may
switch to blue to help enhance the sound dampening. After a while using it, my
shift key started to squeak, so I picked up [this tube of
lubricant](http://www.amazon.com/gp/product/B002L5UL92/) and it fixed it up
perfectly. Also, any keyboard of mine is going to have a [red escape
key](http://www.wasdkeyboards.com/index.php/products/printed-keycap-singles/esc-cherry-mx-keycap.html),
it just has to be done.

Later on, I got [backlight color covers](http://www.amazon.com/dp/B00QGJGOIG/) to try out, but I ended up sticking with
the white backlight.

CODE Keyboard parts list and price breakdown:

* [CODE Keyboard with Cherry MX Clears](http://www.wasdkeyboards.com/index.php/products/code-keyboard/code-104-key-mechanical-keyboard.html) - $155
* [Red O-Rings](http://www.wasdkeyboards.com/index.php/cherry-mx-rubber-o-ring-switch-dampeners-125pcs.html) - $15
* [Teflon Grease](http://www.amazon.com/gp/product/B002L5UL92/) - $11
* [Red ESC Key](http://www.wasdkeyboards.com/index.php/products/printed-keycap-singles/esc-cherry-mx-keycap.html) - $3
* [Keyboard LED COvers](http://www.amazon.com/dp/B00QGJGOIG/) - $15
* **Total:** $199

## Unicomp Ultra Classic Black Buckling Spring USB

[{% img responsive ../../../../../images/2016-01-23-mechanical-keyboards-4.jpg %}](../../../../../images/2016-01-23-mechanical-keyboards-4.jpg)

The second mechanical keyboard I bought was the Unicomp Ultra Classic. If you've
ever used the [classic IBM Model M
keyboard](https://en.wikipedia.org/wiki/Model_M_keyboard), you know exactly what
you're getting into. This keyboard is a buckling spring board, and the only
difference I can see in this and the classic Model M is that the housing is a
bit smaller, the color scheme is different (if you chose black like I did), and
it has a USB connection (but you can get a PS2 connection instead). It's heavy,
it feels fantastic, and I only have one small complaint: The rubber stoppers on
the bottom slide a bit, unlike the CODE Keyboard. I think bigger rubber stoppers
would help aleviate this problem, but it isn't like the keyboard just slides
around everywhere, it's a subtle tiny problem.

This keyboard feels and
[sounds](https://upload.wikimedia.org/wikipedia/commons/4/4d/Modelm.ogg) (IBM
Model M sounds, but it's the same thing) absolutely amazing. The buckling
springs are nice a crisp, you know when you've pressed a key and accidental
presses don't happen. The key springs are heavy enough that you can rest your
fingers on the keys, completely dead-weight, without accidentally pressing down
a key. The keys spring back nice and crisp, ready for orders.

The keys look fantastic as well, the glyphs are created using [dye
sublimination](https://deskthority.net/wiki/Keycap_printing#Dye_sublimation),
which means you cannot wear the prints off of the keys, as the plastic of the
keycaps are stained with that particular glyph. The result is that the keys will
continue to look nice for the next few decades.

Probably the greatest feature of this keyboard is the price, a mere $84 for the
keyboard. You're not gonna get a backlit keyboard that will fly you to space or
get you to the top of
[/r/mechanicalkeyboards](https://www.reddit.com/r/mechanicalkeyboards), but what
you are going to get is a fantastic keyboard with a great feel, that will be
absolutely reliable for a decade or more. Well worth the money all by itself.

But that didn't stop me from adding a small number of additions: A red escape
key, and a tux keycap set to replace my Windows keys.

But wait! There's more! These are made in the USA. In Lexington, Kentucky to be
exact. Also, I recieved a key that was the wrong size in my Tux Key Set, so I
emailed customer service, got a reply crazy fast, and they shipped me the proper
key right away. Great customer service, no bullshit, no jumping through hoops,
just making people happy quickly and easily. I'm very impressed.

Unicomp Ultra Classic parts list and price breakdown:

* [Unicomp Ultra Classic Black Buckling Spring USB](http://www.pckeyboard.com/page/product/UB40P4A) - $84
* [Red ESC Key](http://www.pckeyboard.com/page/product/PRK) - $1.50
* [Linux Tux Key Set](http://www.pckeyboard.com/page/product/LinTuxSet) - $12
* **Total:** $97.50

## Which do I like better

I have to say, I do much prefer the Unicomp keyboard. It's crazy loud, it's not
fancy, there's no backlighting, but it just feels great and the price is
unbeatable for a well-respected and well-made mechanical keyboard.

## Mechanical Keyboard Resources

* [Reddit's Mechanical Keyboard Subreddit](https://www.reddit.com/r/mechanicalkeyboards)
  * From reviews, to buying guides, to general information, this subreddit is the nexus of all things Mechanical Keyboard. Go browse around a bit.
* [WASD's Cherry MX sampler kit](http://www.wasdkeyboards.com/index.php/products/sampler-kit/wasd-sampler-kit.html)
  * If you're going to get a Cherry MX keyboard instead of a buckling spring, start out with this switch sampler kit and find out what kind of switches you prefer. There is no "best" switch, this is 100% personal preference.
