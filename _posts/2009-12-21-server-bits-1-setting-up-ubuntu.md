---
title: "Server-Bits #1: Setting Up Ubuntu"
layout: "post"
date: "2009-12-21 22:04:00"
categories: [Tutorials, Linux]
---

[![Screenshot-1](http://farm3.static.flickr.com/2691/4203725161_d5beded270_t.jpg)](http://www.flickr.com/photos/samurailink3/4203725161/
"photo sharing" )  
[Screenshot-1](http://www.flickr.com/photos/samurailink3/4203725161/),  
originally uploaded by
[samurailink3](http://www.flickr.com/people/samurailink3/).  

### First up: Setting up Ubuntu with Encryption or: How I learned to stop
worrying and love the AES cipher.


Before you begin there are a few things this guide will assume:  

1\. You have a computer capable of running Ubuntu Linux 9.10.  
2\. You have a router with the ability to port forward.  
3\. Your internet connection is of Broadband capacity or better.  
4\. You don't want to spend any money, or you want to spend as little as
possible.  

First thing you will need to do is download and burn an Ubuntu 9.10 Alternate
Install Disk. [Why the Alternate install disk? I, personally, like to encrypt
my server hard drives. This is completely optional, and the 'normal' install
disk is faster/easier to install, but this guide will walk through the
alternate install disk for encryption purposes.]  

Go ahead and step through the ["Server-Bits
#1"](http://www.flickr.com/photos/samurailink3/4203725161/) photo set on
[Flickr](http://www.flickr.com/) to run through the tutorial.  

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
