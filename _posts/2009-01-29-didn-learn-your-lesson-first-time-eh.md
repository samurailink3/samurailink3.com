---
title: "Didn't learn your lesson the first time, eh?"
layout: "post"
date: "2009-01-29 19:57:00"
categories: [Rants]
---

I wouldn't consider myself an Apple fanboy in any way, but one thing is for
sure, they know how to make things simple for their customers. One thing
that's always irked me about Microsoft's Windows line... Why all the different
versions? Why do I (the consumer) need to choose between Windows 7 Starter,
Basic, Home Basic, Pro Basic, Pro, Home Extended, Premium Pro Home, Home,
Premium, and Premium Ultimate Home Pro Edition with Sprinkles?  

Ok... ok.. so I'm being a little ridiculous here... But even still.. I think I
could work with two versions, three is pushing it.. but **five**?? Why do we
need to choose from five different versions of the same product? When you buy
Leopard 10.5 for the mac, you buy **Leopard 10.5**. There isn't a pro, basic,
starter, or business edition. Its just **Leopard**. All of these different
versions cause a bit of a headache with Windows XP, and somewhat of a migraine
with Vista. _What do you want to do with your computer? Take a look at this
handy table full of terms consumers don't understand and some terms that only
exist in our marketing textbooks!! _**No, Microsoft!** This shit has to stop.
People want to use their computers. People don't want to choose between 2, or
3, or 4, or hell.. 5 different versions of the exact same codebase. **Its
fucking stupid**.

I've got an idea.. a brilliant idea. No tech company has _ever, Ever_ done
this before. Make a solid product (You've got me there, 7 is a fucking rock),
make it shiny (7's got this one down too), then make it stupid easy to buy.
How do you go about doing this? Put one version out, put it on every shelf.
Don't even put out Upgrade packs and Full install packs. Throw it on one disk,
put it on shelves, in schools, in the hands of celebrities (You can skip
Seinfeld safely.. don't worry), and make people _want_ it. You'll be the talk
of the town! You'll be unique! You'll be pretty and everyone will love you! Oh
wait... **Apple did that already.**  

This post is in reply to:

[http://gizmodo.com/5139306/evidence-of-five-windows-7-retail-versions-surprises-no-one](http://gizmodo.com/5139306/evidence-of-five-windows-7-retail-versions-surprises-no-one)
