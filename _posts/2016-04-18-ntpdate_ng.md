---
layout: post
title: "ntpdate_ng"
date: 2016-04-18 07:05:21 EDT
comments: true
categories: [Code, Projects]
emoji: false
bg: false
---

I've been learning [Go](https://golang.org/) recently (the programming language,
not the game) and ran into a small, annoying problem that I could fix nicely
with some code. Perfect opportunity to build a golang binary and try out the
language for a simple use case.

My problem was that on a particular network I often use NTP traffic is
completely blocked, and the particular machine I'm using has a horrible internal
clock. Each week this machine drifts by 90 seconds. It's a stationary machine,
always attached to the ntp-blocking network. While this isn't a huge deal, I
sometimes work with time-sensitive code relating to [time-based security
tokens](https://tools.ietf.org/html/rfc6238), and 90 seconds (let alone 30)
completely throws this off. I needed a way to set my system clock regularly,
without watching the time on my phone and hitting <kbd>Enter</kbd> at the
perfect time to send the `date -s` command.

I've built a small utility (Linux and Windows support for now) that uses the
[HTTP Date
Header](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Request_fields)
and either the `date` command on Linux or the w32 API on Windows to set the
time. The program runs in about 0.3 seconds, which is good enough for my use
case, I need time accurately set within a couple seconds. Basically how it works
is that it grabs Google's homepage by default (you can use a flag to set your
own URL) and uses the date header to set the system clock. Obviously you'll need
administrator permissions for this all to work correctly. I've designed the
program to not pollute system mail with useless messages if you run it in cron.

You can grab the binaries here:

* [ntpdate_ng-1.1.0.linux.386](https://files.samurailink3.com/ntpdate_ng-1.1.0.linux.386)
* [ntpdate_ng-1.1.0.linux.amd64](https://files.samurailink3.com/ntpdate_ng-1.1.0.linux.amd64)
* [ntpdate_ng-1.1.0.windows.386.exe](https://files.samurailink3.com/ntpdate_ng-1.1.0.windows.386.exe)
* [ntpdate_ng-1.1.0.windows.amd64.exe](https://files.samurailink3.com/ntpdate_ng-1.1.0.windows.amd64.exe)

And you can see the source code and readme on [the project's GitLab
page](https://gitlab.com/samurailink3/ntpdate_ng). As usual, it's [MIT
Licensed](https://gitlab.com/samurailink3/ntpdate_ng/blob/master/LICENSE).

As with any of my projects, if you'd like to make this better or find a bug,
head over to the GitLab page and send a merge request or put in an issue.

A huge thanks to [VividCortex](https://github.com/VividCortex) for their [golang
w32 API library](https://github.com/VividCortex/w32).

## Update

Apparently this has been done before with `htpdate` and the [HTTP Time
Protocol](http://www.vervest.org/htp/). I won't remove the project, but if
you're looking for something a bit more polished and professional, `htpdate` is
the better constructed tool for this purpose.
