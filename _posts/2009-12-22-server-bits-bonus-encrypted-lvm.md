---
title: "Server-Bits BONUS: Encrypted LVM Explained"
layout: "post"
date: "2009-12-22 22:49:00"
categories: [Tutorials, Linux]
---

[{% img center ../../../../../images/2009-12-22-server-bits-bonus-encrypted-lvm-1.png %}](../../../../../images/2009-12-22-server-bits-bonus-encrypted-lvm-1.png)

Here is a brief walkthrough of how Encrypted LVM works on boot.  

1. The /boot partition is mounted.
2. The /boot partition attempts to mount the physical volume for encryption (crypto-disk).
3. The system asks the user for their password.
4. Upon entering a successful password, the Logical Volume Manager takes over and mounts the / (root) and swap partitions.
5. The system continues the boot process.
6. When the system is shut down, the filesystems are all unmounted.
7. The root and swap partitions cannot be mounted/decrypted without the proper password, keeping your data secure.

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
