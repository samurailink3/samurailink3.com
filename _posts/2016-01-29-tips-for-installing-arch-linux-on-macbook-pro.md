---
layout: post
title: "Tips for installing Arch Linux on Macbook Pro"
date: 2016-01-29 07:19:39 EST
comments: true
categories: [Tutorials]
bg: false
---

Some pointers on installing Arch Linux on a Macbook Pro. I don't use
Arch anymore, but this might be helpful for someone. In these
examples, I'm using `pacaur` instead of `pacman` for the AUR
integration. Check out
[this page in the Arch wiki for more information on pacaur](https://wiki.archlinux.org/index.php/Pacaur).

### Wifi

`pacaur -S broadcom-wl-dkms` - I use the dkms version to keep things
stable.

### Backlight

Set this in `/etc/rc.local`: `setpci -v -H1 -s 00:01.00
BRIDGE_CONTROL=0`

### LTS Mode (more stable)

#### LTS Kernel Installation

`pacaur -S linux-lts linux-headers-lts nvidia-lts`, then use dkms
versions of modules to build against your new kernel.  Then use `sudo
grub-mkconfig -o /boot/grub/grub.cfg` to create a new boot menu. LTS
should be at the top, but if not, you can add these options to
`/etc/default/grub` to make grub save your last boot option:

```
GRUB_DEFAULT=saved
GRUB_SAVEDEFAULT=true
GRUB_DISABLE_SUBMENU=y
```

#### General Kernel Modules

Enable the dkms service to automatically build kernel modules when the
source updates. Only install dkms versions of modules.

#### Virtualbox Host DKMS

Use `dkms install vboxhost/$(pacman -Q virtualbox|awk '{print $2}'|sed
's/\-.\+//') -k $(uname -rm|sed 's/\ /\//')` to install the virtualbox
host modules using dkms.
