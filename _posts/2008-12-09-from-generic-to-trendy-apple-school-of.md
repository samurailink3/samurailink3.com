---
title: "From Generic to Trendy: The Apple School of Marketing"
layout: "post"
date: "2008-12-09 06:58:00"
categories: [News]
---

Thanks to [arstechnica.com](http://www.arstechnica.com/ "arstechnica.com" )
for the Image

Now.. don't get me wrong.. I'm the farthest thing from an apple fanboy there
is. I'm a Linux advocate and heavy PC user. But Apple is the only company on
the planet to turn "Generic" Marketing into trendy, beautiful, effective
marketing. Do you remember walking into the grocery store as a kid, seeing the
cans with the white label, black text, that reads "Corn"? Yea, that was
generic. Simplistic. Run of the mill. Poor. Cheap. Now look at the iPod box: A
picture of the product, name on the top in a clear, non-stylized font.
Essentially the same idea: Avoid the fluff, get the information out there,
keep it simple, keep the focus on the product itself. Apple took an old
concept, made it trendy, and is now at the top of the branding charts
everywhere. People recognize iPods, Macbooks, iMacs; Hell, mis-informed
consumers even refer to personal media players as the one generic term "iPod"
now. Apple has taken an idea from old-world save-a-buck cans and applied it to
a consumer electronics device in such a way that it became a phenomenon.  

Congratulations, Apple. It really is a beautiful thing.
