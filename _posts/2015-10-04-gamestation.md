---
layout: post
title: "Gamestation"
date: 2015-10-04 23:02:32 EDT
comments: true
categories: [Projects, Gaming]
bg: 2015-10-04-gamestation-bg.png
---

_Thanks for the [awesome background](https://github.com/libretro/retroarch-assets/blob/master/wallpapers/emulationstation%20blured/1280x720/Nintendo%20-%20Super%20Nintendo%20Entertainment%20System.png) [libretro](http://www.libretro.com/)!_

### About

[Project Gamestation](/blog/2013/02/20/project-gamestation/) took a major
backseat when I discovered
[RetroPie](http://blog.petrockblock.com/retropie/).
[petRockBlog](http://blog.petrockblock.com/) did a fantastic job putting
everything together and making it "Just Work". I seriously recommend
checking it out. When it comes to gaming on the Raspberry Pi, RetroPie
did everything I could have wanted and more, but it's really designed for
the Pi, not for x86/x64 systems.

That's where Gamestation comes in. The project is really nothing special.
It's a couple shell scripts that automate fetching the repos, building
other people's projects, creating a Debian package, and installing it.
It's not a major piece of work, but it makes my life easier and has
resulted in a great double-click-easy system that I can hook to my TV
that has a bit more power than a Pi 2.

The best part of this is combining it with Syncthing. I always hated how
my roms and saves would differ between machines, requiring a central
folder somewhere on ownCloud or manually syncing. Syncthing is like
Bittorrent Sync, but open source and well respected. By using this and a
centralized folder structure, you can keep all roms/saves/themes/metadata
syncronized between Gamestation machines. It works really well and is
useful for bootstrapping new machines.

### Installation

Right now Gamestation is only supported by Debian and Debian-family Linux
distributions. I'm 98% sure you can get it running on just about any
\*nix OS out there, but I can't claim to have tested it. If you'd like to
add other OS support, check out the [GitLab
Repo](https://gitlab.com/samurailink3/gamestation).

[Grab the .deb
here](https://files.samurailink3.com/gamestation-20160106120249.deb)
and install with `sudo dpkg -i`. If your system complains about dependencies, try `sudo apt-get -f install` to find and install the missing libraries.

Gamestation expects the following folders to be in `~/roms/`:

```
amiga
amstradcpc
apple2
atari2600
atari5200
atari7800
atari800
atarilynx
atarist
c64
coco
dragon32
fba
fds
gamegear
gb
gba
gbc
intellivision
macintosh
mame-advmame
mame-mame4all
mastersystem
megadrive
msx
n64
nds
neogeo
nes
ngp
pc
pcengine
ports
psx
quake3
scummvm
sega32x
segacd
sg-1000
snes
vectrex
videopac
wonderswan
zmachine
zxspectrum
```

And a symlink from `~/roms/megadrive` to `~/roms/genesis`: `ln -s ~/roms/megadrive ~/roms/genesis`

To actually run the system, use the command `emulationstation`.

### Meta

[As usual, the project is open source, grab the code
here.](https://gitlab.com/samurailink3/gamestation)

This project is licensed under the [GNU GPL
v3](https://www.gnu.org/licenses/gpl.html).

If you're building a nightly-release, check out the source code's
[README.md](https://gitlab.com/samurailink3/gamestation/blob/master/README.md). It'll walk you through how to build one from source and end up with a Debian package of your own.

### TODO

There's still a lot of work to be done. If you'd like to make this
project better, send me a merge request.

1. Windows support - This will make it easy for non-techies to get their
emulators up and running easily.
2. RetroPie currently makes controller bindings between EmulationStation and
Retroarch seamless (for the most part). I'd love to port that over.
3. Create a build-bot system that puts out weekly beta builds of GameStation.
Ideally, this would be an ansible playbook or something equally as automated.
4. Automated Syncthing setup. Currently, Syncthing is pretty geared towards
techies. If mass adoption is going to be possible, this part will need to be
easier to set up. Maybe a web service that links IDs together through the
Syncthing API? Lots to consider.
5. **PIPE DREAM** - I'd LOVE to get this working on a
[PiGRRL](https://learn.adafruit.com/pocket-pigrrl) with wifi. Take your roms and
saves ANYWHERE! Sync when you hit wifi.

### Syncthing Setup

1. Follow the instructions on [Syncthing's Debian page](http://apt.syncthing.net/).
2. Create a service file for syncthing:
  * ``sudo cp /usr/lib/systemd/system/syncthing@.service /etc/systemd/system/syncthing@`whoami`.service``
  * If you aren't on a system with systemd yet, use [this init file](https://gist.github.com/arudmin/5a13e9105814c3f568ec).
3. Go to your local syncthing management page at [http://localhost:8384/](http://localhost:8384/).
4. Click `Add Folder` and add `~/roms/`, feel free to set up file versioning if you'd like.
5. Click `Add Folder` and add `~/.emulationstation`, feel free to set up file versioning if you'd like.
6. Use `Actions -> Show ID` to show your device's code.
7. On a new device, install Syncthing and click `Add Device`, paste in your first device's code and give it a name.
  * Adding your first device as an "Introducer" is pretty helpful, it can act like a central point for all things Syncthing in your setup.
8. On the original device, Syncthing will eventually ask you if you'd like to trust the new device you've added. Go ahead and trust it, then share the `.emulationstation` and `~/roms/` folders with it.
9. Synchronization should happen automatically.
10. Install Gamestation on the new system, your saves, roms, and themes should all be synced between all Gamestation devices.

### Credits

This project wouldn't be possible without the amazing work by the
following projects:

* [RetroPie](https://github.com/RetroPie/RetroPie-Setup/)
* [Libretro](https://github.com/libretro)
* [Retroarch](https://github.com/libretro/RetroArch)
* [EmulationStation](https://github.com/Aloshi/EmulationStation)
* [Syncthing](https://syncthing.net/)

For full details and credits, check out the
[README.md](https://gitlab.com/samurailink3/gamestation/blob/master/README.md)
file.
