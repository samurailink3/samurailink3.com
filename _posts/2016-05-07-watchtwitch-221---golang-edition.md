---
layout: post
title: "watchtwitch 2.2.1 - Browse Twitch.TV"
date: 2016-05-07 15:49:32 EDT
comments: true
categories: [Code]
emoji: true
bg: "2016-05-03-watchtwitch-200---golang-edition-bg.png"
---
<h1 style="text-align:center;">:video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game:</h1>

Here's a new update to `watchtwitch`. You can now use the program to really
*browse* Twitch.tv. I've built in functionality to search for games by name and
see the top streaming games on Twitch.

## Examples

`watchtwitch -search zelda -quality medium`

```
1) The Legend of Zelda: Ocarina of Time
2) The Legend of Zelda: A Link to the Past
3) The Legend of Zelda: Majora's Mask
4) The Legend of Zelda: Skyward Sword
5) The Legend of Zelda: The Wind Waker
6) The Legend of Zelda: Ocarina of Time 3D
7) The Legend of Zelda
8) Zelda II: The Adventure of Link
9) The Legend of Zelda: Twilight Princess
10) The Legend Of Zelda: Four Swords Anniversary Edition
11) The Legend of Zelda: Oracle of Seasons
12) The Legend of Zelda: Oracle of Ages
13) Hyrule Warriors
14) The Legend of Zelda: Phantom Hourglass
15) The Legend of Zelda: A Link to the Past & Four Swords
16) The Legend of Zelda: Ocarina of Time / Master Quest
17) Zelda: The Wand of Gamelon
18) The Legend of Zelda: Link's Awakening DX
19) The Legend of Zelda: Link's Awakening
20) The Legend of Zelda: Four Swords Adventures
Make a selection: 2
1) SpeedGaming (383)
2) ChristosOwen (19)
3) wqqqqwrt (8)
4) caznode (4)
5) Cransoon (3)
6) DevolitionDerby (3)
7) iReference (0)
8) Napptasm (0)
Make a selection: 1
```

`watchtwitch -topgames -q medium`

```
1) League of Legends (131815)
2) Counter-Strike: Global Offensive (103283)
3) Hearthstone: Heroes of Warcraft (99037)
4) Dota 2 (49565)
5) Call of Duty: Black Ops III (43002)
6) Overwatch (34032)
7) DayZ (33725)
8) FIFA 16 (29582)
9) StarCraft II (25943)
10) Clash Royale (25615)
Make a selection: 3
1) dreamhackhs (43118)
2) mira_hs (1495)
3) Shadybunny (820)
4) DeerNadia (788)
5) Hotform (602)
6) Alliestrasza (565)
7) RageGamingVideos (313)
8) TZAR1337 (242)
9) Lotsko (134)
10) Himiwako (93)
Make a selection: 1
```

## Downloads

* [watchtwitch-2.2.1.linux.386](https://files.samurailink3.com/watchtwitch-2.2.1.linux.386)
* [watchtwitch-2.2.1.linux.amd64](https://files.samurailink3.com/watchtwitch-2.2.1.linux.amd64)
* [watchtwitch-2.2.1.linux.arm](https://files.samurailink3.com/watchtwitch-2.2.1.linux.arm)

As always, you can [grab the code here on
GitLab](https://gitlab.com/samurailink3/watchtwitch). If you'd like a build for
a different platform, let me know in the comments.

<h1 style="text-align:center;">:video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game:</h1>
