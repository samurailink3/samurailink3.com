---
layout: post
title: "How to Change Date in a Git Commit"
date: 2016-01-21 06:50:18 EST
comments: true
categories: [Tutorials]
bg: false
---

Sometimes you just gotta fix a date in git commits, and this can
happen for a wide variety of reasons. Maybe you were on a system with
an unset clock and you want accurate timekeeping, maybe you don't want
to admit you were up at 4AM coding. I get it. Here's the fix:

```
git filter-branch --env-filter \
'if [ $GIT_COMMIT = 119f9ecf58069b265ab22f1f97d2b648faf932e0 ]; then
export GIT_AUTHOR_DATE="Fri Jan 2 21:38:53 2009 -0800"
export GIT_COMMITTER_DATE="Sat May 19 01:01:01 2007 -0700"
fi'
```
