---
layout: post
title: "Virtual Web Server: Part 6 - ProFTPd Setup"
date: 2013-05-09 04:36 EDT
comments: true
categories: [Tutorials]
emoji: false
---

Now that you have most of what you need to run some simple sites, you
need a way to open it up to people, allow them to upload their own files.
This is where [ProFTPd](http://www.proftpd.org/) comes in. First, install
it with `apt-get install proftpd`. There are some configuration options
as you set it up. Right now, select `standalone` when the installer asks
you whether to run as a service or standalone server.

Now we have to create a user who will manage our domain. I usually set
this name to the domain name itself without the ending (so `example`
would manage `www.example.com`). Create a new user with `adduser example`
and set a good, complex password. Debian will ask you for more details,
but you don't need to fill these out. This creates a new user and home
directory. We actually don't want our user to use `/home/example` as
their home directory, we want them to be jailed to the web directory.
Open up `/etc/passwd` in your text editor of choice, we're going to make
some changes to this new user. We need to set a new home directory, and
more imporantly, a new shell, we don't want the user logging on via SSH,
they don't need that kind of access. Check out the example below for
configuration details.

```
bob:x:0:0:root:/home/bob:/bin/bash
# The above user can log in interactively, and their home folder is in the usual location
alice:x:1000:1000:,,,:/var/www/www.example.com:/bin/false
# The above user can't log in interactively, and their home folder is set to their public website folder.
```


After you set up your user like `alice` in the example above, we need to
change the permissions on our new user's home directory. Use this command
to set the owner and group of the folder to your new user: `chown -R
example:example /var/www/www.example.com/`. Now we're ready to set some
ProFTPd configuration options. Open up `/etc/proftpd/proftpd.conf` and
copy my configuration file below.

```
#
# /etc/proftpd/proftpd.conf -- This is a basic ProFTPD configuration file.
# To really apply changes reload proftpd after modifications.
#

# Includes DSO modules
Include /etc/proftpd/modules.conf

# Set off to disable IPv6 support which is annoying on IPv4 only boxes.
UseIPv6    			on
# If set on you can experience a longer connection delay in many cases.
IdentLookups			off

ServerName			"Debian"
ServerType			standalone
DeferWelcome			off

MultilineRFC2228		on
DefaultServer			on
ShowSymlinks			on

TimeoutNoTransfer		600
TimeoutStalled			600
TimeoutIdle			1200

DisplayLogin                    welcome.msg
DisplayChdir               	.message true
ListOptions                	"-l"

DenyFilter			\*.*/

# Use this to jail all users in their homes
DefaultRoot			~

# Users require a valid shell listed in /etc/shells to login.
# Use this directive to release that constrain.
RequireValidShell		off

# Port 21 is the standard FTP port.
Port				21

# In some cases you have to specify passive ports range to by-pass
# firewall limitations. Ephemeral ports can be used for that, but
# feel free to use a more narrow range.
# PassivePorts                  49152 65534

# If your host was NATted, this option is useful in order to
# allow passive tranfers to work. You have to use your public
# address and opening the passive ports used on your firewall as well.
# MasqueradeAddress		1.2.3.4

# This is useful for masquerading address with dynamic IPs:
# refresh any configured MasqueradeAddress directives every 8 hours
<IfModule mod_dynmasq.c>
# DynMasqRefresh 28800
</IfModule>

# To prevent DoS attacks, set the maximum number of child processes
# to 30.  If you need to allow more than 30 concurrent connections
# at once, simply increase this value.  Note that this ONLY works
# in standalone mode, in inetd mode you should use an inetd server
# that allows you to limit maximum number of processes per service
# (such as xinetd)
MaxInstances			30

# Set the user and group that the server normally runs at.
User				proftpd
Group				nogroup

# Umask 022 is a good standard umask to prevent new files and dirs
# (second parm) from being group and world writable.
Umask				022  022
# Normally, we want files to be overwriteable.
AllowOverwrite			on

# Uncomment this if you are using NIS or LDAP via NSS to retrieve passwords:
# PersistentPasswd		off

# This is required to use both PAM-based authentication and local passwords
# AuthOrder			mod_auth_pam.c* mod_auth_unix.c

# Be warned: use of this directive impacts CPU average load!
# Uncomment this if you like to see progress and transfer rate with ftpwho
# in downloads. That is not needed for uploads rates.
#
# UseSendFile			off

TransferLog /var/log/proftpd/xferlog
SystemLog   /var/log/proftpd/proftpd.log

<IfModule mod_quotatab.c>
QuotaEngine off
</IfModule>

<IfModule mod_ratio.c>
Ratios off
</IfModule>


# Delay engine reduces impact of the so-called Timing Attack described in
# http://security.lss.hr/index.php?page=details&ID=LSS-2004-10-02
# It is on by default.
<IfModule mod_delay.c>
DelayEngine on
</IfModule>

<IfModule mod_ctrls.c>
ControlsEngine        off
ControlsMaxClients    2
ControlsLog           /var/log/proftpd/controls.log
ControlsInterval      5
ControlsSocket        /var/run/proftpd/proftpd.sock
</IfModule>

<IfModule mod_ctrls_admin.c>
AdminControlsEngine off
</IfModule>

#
# Alternative authentication frameworks
#
#Include /etc/proftpd/ldap.conf
#Include /etc/proftpd/sql.conf

#
# This is used for FTPS connections
#
#Include /etc/proftpd/tls.conf

#
# Useful to keep VirtualHost/VirtualRoot directives separated
#
#Include /etc/proftpd/virtuals.con

# A basic anonymous configuration, no upload directories.

# <Anonymous ~ftp>
#   User				ftp
#   Group				nogroup
#   # We want clients to be able to login with "anonymous" as well as "ftp"
#   UserAlias			anonymous ftp
#   # Cosmetic changes, all files belongs to ftp user
#   DirFakeUser	on ftp
#   DirFakeGroup on ftp
#
#   RequireValidShell		off
#
#   # Limit the maximum number of anonymous logins
#   MaxClients			10
#
#   # We want 'welcome.msg' displayed at login, and '.message' displayed
#   # in each newly chdired directory.
#   DisplayLogin			welcome.msg
#   DisplayChdir		.message
#
#   # Limit WRITE everywhere in the anonymous chroot
#   <Directory *>
#     <Limit WRITE>
#       DenyAll
#     </Limit>
#   </Directory>
#
#   # Uncomment this if you're brave.
#   # <Directory incoming>
#   #   # Umask 022 is a good standard umask to prevent new files and dirs
#   #   # (second parm) from being group and world writable.
#   #   Umask				022  022
#   #            <Limit READ WRITE>
#   #            DenyAll
#   #            </Limit>
#   #            <Limit STOR>
#   #            AllowAll
#   #            </Limit>
#   # </Directory>
#
# </Anonymous>
```

This configuration jails users in their home directories, which we've
just set to be the public web directory. You do need to run `service
proftpd restart` to apply the configuration changes.

Have your users log in with the following details (changed to fit your
domain/username of course):

```
Host: www.example.com
Port: 21
Username: example
Password: # the password you set for this user #
```

Now your users can push and pull files to set up their websites.

**Other posts in this series:**

1. [Getting a Server](/blog/2013/04/23/virtual-web-server-part-1-rackspace/)
2. [Getting a Domain Name](/blog/2013/04/23/virtual-web-server-part-2-hover/)
3. [Automatic Updates](/blog/2013/04/23/virtual-web-server-part-3-automatic-updates-in-debian/)
4. [Apache2 Setup](/blog/2013/04/28/virtual-web-server-part-4-apache-web-server/)
5. [PHP and MySQL Setup](/blog/2013/05/02/virtual-web-server-part-5-php-and-mysql-setup/)
6. ProFTPd Setup _- You are here_
7. [WordPress Setup](/blog/2013/05/09/virtual-web-server-part-7-wordpress-setup/)
8. [RVM Setup](/blog/2013/05/11/virtual-web-server-part-8-rvm-setup/)
9. [Git Setup](/blog/2013/05/11/virtual-web-server-part-9-git-setup/)
10. OctoPress Setup
11. Rackspace Backups
