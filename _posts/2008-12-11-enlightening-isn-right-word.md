---
title: "Enlightening isn't the right word..."
layout: "post"
date: "2008-12-11 17:47:00"
categories: [Reviews]
---

But its the first word that came to mind.

Most of you have already witnessed a work by Chuck Palahniuk, most likely it
was a movie, to most of you, it presented an interesting concept, a counter-
culture of sorts. That movie was Fight Club. But you liked it because of the
brilliant film making, the impressive cinematography, and the starring roles
of two big-name actors. Most of you didn't know this was also a book. First a
book. I'm not gonna lie. I saw the movie first, then I read the book. I'm not
going to judge you, I'm not hypocritical, I did the same thing. But I'm not
here to discuss that movie, nor that book. I'm here to tell you about some of
Palahniuk's tales that you might have missed.  

Recently I've finished Invisible Monsters by Chuch Palahniuk. No spoilers
here. I just wanted to say that it is quite an amazing book. Its the type of
book that you'll read two or three times just to fully grasp the concepts it
throws at you. While Invisible Monsters shared some similar themes with Fight
Club, both books stand alone in their own right. Both take a hard look at
society, consumerism, and the daily grind, but each has its own story to tell
and its own way of telling it.  

This isn't your grandfather's paperback. This isn't your grandfather's world
anymore, and Palahniuk's books perfectly represent that. Fight Club hooked me
from the start, same with Choke, same with Survivor, but Invisible Monsters
hooked me somewhere in the middle. Not to say that the book was bad, boring,
or dry, it wasn't. I guess it just didn't have the same "Kick in the door"
beginning that the other books had. Invisible Monsters is one of those books
that you figure out all the twists half-way through, then it kicks you in the
groin, tells you you're wrong, daddy never loved you, and then reveals what's
really going on. Its an amazing book that looks at modeling, beauty,
deformation, and individualism on an entirely different level.  

Palahniuk's books are never about a happy person. Its always the person in the
back row you'd never expect (This is an exception in Invisible Monsters, in
the beginning, she is all but invisible). The person who you think is quiet
and content, only to be thinking the entire time. You can't shut their mind
up. They have a bleak view of society and humanity. And that's what kicks off
each story. A person who isn't content. Or is it a person who is all too
content? You'll just have to read and find out.  

Regardless, all of these books are absolutely engaging, enthralling, and
ultimately entertaining. I'm two chapters into Survivor (or is it two chapters
out of Survivor?) and I'm already hooked. You know a book is good when you
consider that it could be your favorite book of all time two chapters in.

[Go](http://www.amazon.com/Survivor-Novel-Chuck-
Palahniuk/dp/0385498721/ref=pd_bbs_sr_2?ie=UTF8&s=books&qid=1229017449&sr=8-2
"Go" ) [buy](http://www.amazon.com/Invisible-Monsters-Chuck-
Palahniuk/dp/0393319296/ref=pd_bbs_sr_3?ie=UTF8&s=books&qid=1229017449&sr=8-3
"buy" ) [these](http://www.amazon.com/Choke-Chuck-
Palahniuk/dp/0307388921/ref=pd_bbs_sr_4?ie=UTF8&s=books&qid=1229017449&sr=8-4
"these" ) [now](http://www.amazon.com/Fight-Club-Novel-Chuck-
Palahniuk/dp/0393327345/ref=pd_bbs_sr_7?ie=UTF8&s=books&qid=1229017449&sr=8-7
"now" ).
