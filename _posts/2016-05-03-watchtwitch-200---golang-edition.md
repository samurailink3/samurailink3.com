---
layout: post
title: "watchtwitch 2.0.0 - GoLang Edition"
date: 2016-05-03 10:35:41 EDT
comments: true
categories: [Code]
emoji: true
bg: "2016-05-03-watchtwitch-200---golang-edition-bg.png"
---

<h1 style="text-align:center;">:video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game:</h1>

If you haven't seen [my command line twitch.tv
browser](/blog/2016/02/09/command-line-twitch-tv-browser/), check that out
first. While the ruby script works perfectly, it was still pretty heavy, needing
ruby and gems installed everywhere I wanted to have at-my-fingertips Dota 2
streams. I've been getting into [GoLang](https://golang.org/) recently and this
seemed like the perfect project to re-implement.

[{% img center ../../../../../images/2016-05-03-watchtwitch-200---golang-edition-1.png %}](../../../../../images/2016-05-03-watchtwitch-200---golang-edition-1-full.png)

Notable changes include command flags for choosing the game you'd like to watch,
the quality of the stream, and the language of the stream. Check out the
[readme](https://gitlab.com/samurailink3/watchtwitch/blob/master/readme.md) for
more information. All this goodness rolled up into a single binary.

Like with the ruby script, it does rely on `livestreamer` and `vlc` to do all of
the heavy lifting. This program is merely a menu of the 10 most popular Twitch
streams for a given game and language.

As usual, the code is MIT licensed and [available on
GitLab](https://gitlab.com/samurailink3/watchtwitch). If building source code
isn't your thing, grab the downloads below.

## Downloads

* [watchtwitch-2.0.0.linux.386](https://files.samurailink3.com/watchtwitch-2.0.0.linux.386)
* [watchtwitch-2.0.0.linux.amd64](https://files.samurailink3.com/watchtwitch-2.0.0.linux.amd64)
* [watchtwitch-2.0.0.linux.arm](https://files.samurailink3.com/watchtwitch-2.0.0.linux.arm)

<h1 style="text-align:center;">:video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game: :video_game:</h1>
