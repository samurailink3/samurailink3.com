---
layout: post
title: "Linux Workstation Security Checklist"
date: 2015-08-28 12:19:37 EDT
comments: true
categories: [Linux, Security, Link]
---

The [Linux Foundation](http://www.linuxfoundation.org/) has
put out a pretty stellar checklist dealing with Linux
workstation security. Covers everything from the obvious to
the truly paranoid. Well worth the read.

[Linux Workstation Security Checklist](https://github.com/lfit/itpol/blob/master/linux-workstation-security.md)
