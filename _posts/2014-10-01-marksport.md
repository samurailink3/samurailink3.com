---
layout: post
title: "MarkSport!"
date: 2014-10-01 18:37 EDT
comments: true
categories: [Projects]
---

I've created yet another stupid simple little app. We love Markdown for
our documentation, but with we could make it pretty with Bootstrap.
Totally doable with some linking stylesheets and javascript, but we love
having everything in one HTML file. From shell script to simple web app,
MarkSport allows you to put in your markdown, some variables, and export
a self-contained, bootstrapified HTML file, ripe for downloading.

Why not PDF? Some people hate them. Some people love the way bootstrap
looks on a mobile device. Etc.

You can check out the app here:
[https://marksport.herokuapp.com](https://marksport.herokuapp.com)

Of course, this is all open source:
[https://gitlab.com/samurailink3/marksport](https://gitlab.com/samurailink3/marksport)
