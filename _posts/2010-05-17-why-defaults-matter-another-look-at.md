---
title: "Why Defaults Matter - Another Look at the Facebook Debate"
layout: "post"
date: "2010-05-17 14:33:00"
categories: [Rants]
---

Not all of us are computer geeks... Actually... the vast majority of us are
anything but. Yes, there are privacy settings, and in the case that you have a
PhD in Facebooking, you can edit these settings and know exactly how they will
function. In a perfect world, we could set, process, and understand each and
every one of these privacy settings and know exactly how they will work,
regardless of our friends' privacy settings, the applications they use, or the
websites we visit that have included Facebook technology.  
Facebook's privacy settings [have been
worsening](http://www.nytimes.com/interactive/2010/05/12/business/facebook-
privacy.html) ever since the company came into being in 2004, and it [doesn't
look like Facebook's stance will change](http://www.businessinsider.com/well-
these-new-zuckerberg-ims-wont-help-facebooks-privacy-problems-2010-5) anytime
in the near future. I've deleted my account for a few reasons, but the biggest
reason is the [same as Leo Laporte's](http://www.businessinsider.com/well-
these-new-zuckerberg-ims-wont-help-facebooks-privacy-problems-2010-5). If I
continue to stay on Facebook, even as _just_ a content publisher and
aggregator, I'm pushing people to continue to use that service and put their
privacy and data at risk. Just my presence on the service is like saying, "No,
its ok if you're on Facebook, just use it in this way.", but we all know that
won't fix anything. Just by participating, I'm giving more power to the
Facebook machine. This is why I've decided to end my Facebook account and move
to [more](http://www.google.com/profiles/samurailink3)
[open](http://twitter.com/samurailink3)
[services](http://www.blogger.com/profile/13794157523677532724). Jason
Calacanis made [many excellent points in his blog
post/email](http://calacanis.com/2010/05/12/the-big-game-zuckerberg-and-
overplaying-your-hand/) and it appears that he will also follow suit. As
always, you can follow me here or on
[Twitter](http://www.twitter.com/samurailink3) to stay up with the latest.
