---
layout: post
title: "SSH Agents"
date: 2017-08-08 20:51:58 PDT
categories: [Link]
emoji: true
bg: false
---

I recently re-installed Gnome 3 on my Debian Sid machine and I quickly realized why I left for XFCE. The damn Gnome Keyring trying to rule my life (and ssh-agent). Here's what I found to fix it.

I'm linking and mirroring these steps in case the original sources ever go away.

* [Prevent Gnome Keyring from launching its own SSH agent](https://wiki.archlinux.org/index.php/GNOME/Keyring#Disable_keyring_daemon_components)
    * ([Secondary Source](https://askubuntu.com/questions/162850/how-to-disable-the-keyring-for-ssh-and-gpg/213522))

```
mkdir ~/.config/autostart
cp /etc/xdg/autostart/gnome-keyring-ssh.desktop ~/.config/autostart/ &&
printf '%s\n' 'Hidden=true' >> ~/.config/autostart/gnome-keyring-ssh.desktop
printf '%s\n' 'X-GNOME-Autostart-enabled=false' >> ~/.config/autostart/gnome-keyring-ssh.desktop
printf '%s\n' 'NoDisplay=true' >> ~/.config/autostart/gnome-keyring-ssh.desktop
```

* [Prevent Gnome Keyring from launching the GPG agent](https://wiki.gnupg.org/GnomeKeyring):

`sudo dpkg-divert --local --rename --divert /etc/xdg/autostart/gnome-keyring-gpg.desktop-disable --add /etc/xdg/autostart/gnome-keyring-gpg.desktop`

* [Launch your own ssh-agent on login using local systemd](https://stackoverflow.com/a/38980986)

On Arch Linux, the following works really great (should work on all systemd-based distros):

Create a systemd user service, by putting the following to `~/.config/systemd/user/ssh-agent.service`:

    [Unit]
    Description=SSH key agent

    [Service]
    Type=forking
    Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
    ExecStart=/usr/bin/ssh-agent -a $SSH_AUTH_SOCK

    [Install]
    WantedBy=default.target

Setup shell to have an environment variable for the socket (`.bash_profile, .zshrc, ...`):

    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

Enable the service, so it'll be started automatically on login, and start it:

    systemctl --user enable ssh-agent
    systemctl --user start ssh-agent

Add the following configuration setting to your ssh config file `~/.ssh/config` (this works since SSH 7.2):

    AddKeysToAgent	yes

This will instruct the ssh client to always add the key to a running agent, so there's no need to ssh-add it beforehand.

---

As a disclaimer, these answers have been copied wholesale (and linked to the
original source) for preservation.
