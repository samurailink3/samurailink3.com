---
layout: post
title: "Installing and Holding Stable Packages in Debian Sid"
date: 2016-01-22 18:49:13 EST
comments: true
categories: [Tutorials]
bg: false
---

Sometimes, running Debian Unstable is... well... _unstable_. Most of
the time it's perfectly stable, it's just bleeding-edge Debian,
perfect for my workstations at home, where I want the latest and
greatest the FOSS community has to offer. Rarely, a package will just
break or come with some nasty bugs. Two I've gotten bit by recently
are
[network-manager](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=809195)
and
[nvidia-driver](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=811349). These
have been fixed recently, but for a bit, the network-manager bug
really bummed me out, I wanted a solution to get back to 'normal' (and
I had already cleared my apt-cache).

In both cases, I was able to install the package from `stable` or
`testing` and get back to my original functionality. Today I'll walk
you through how to do that.

First, let's make sure we have the ability to use HTTPS with apt:

`sudo apt-get install apt-transport-https`

I use the https mirrors at [kernel.org](https://kernel.org/), so my
`/etc/apt/sources.list` looks like this:

```
deb https://mirrors.kernel.org/debian/ sid main contrib non-free
deb-src https://mirrors.kernel.org/debian/ sid main contrib non-free
deb https://mirrors.kernel.org/debian/ testing main contrib non-free
deb-src https://mirrors.kernel.org/debian/ testing main contrib non-free
deb https://mirrors.kernel.org/debian/ stable main contrib non-free
deb-src https://mirrors.kernel.org/debian/ stable main contrib non-free
```

Next, let's set our default release. This will tell `apt` where we
prefer our packages to come from. In my case, I want my packages to
come from `sid` (the codename for Debian unstable).

Add this line to `/etc/apt/apt.conf.d/70debconf`:

`APT::Default-Release "sid";`

**Note:** If you want your default release to be `stable` or
`testing`, use that instead of `sid` in the line above.

What this does is tell apt and dpkg that we prefer for our packages to
come from the `sid` release channel, that keeps us on the `unstable`
branch of Debian. Nice and current. To see this in action, run this
command:

`sudo apt-cache policy network-manager`

```
network-manager:
  Installed: 0.9.10.0-7
  Candidate: 1.0.10-2
  Version table:
    1.0.10-2 990
      990 https://mirrors.kernel.org/debian sid/main amd64 Packages
    1.0.10-1 500
      500 https://mirrors.kernel.org/debian testing/main amd64 Packages
    *** 0.9.10.0-7 500
      500 https://mirrors.kernel.org/debian stable/main amd64 Packages
      100 /var/lib/dpkg/status
```

See the `990` next to the `sid` version? That's our pin-priority
value, the highest value wins when apt-get looks for a version to
install. By setting our default release channel, we've set sid's
pin-priority to 990.

_For more information on pinning, check out these great resources:_

* [https://wiki.debian.org/AptPreferences#Pinning](https://wiki.debian.org/AptPreferences#Pinning)
* [http://jaqque.sbih.org/kplug/apt-pinning.html](http://jaqque.sbih.org/kplug/apt-pinning.html)
* [https://help.ubuntu.com/community/PinningHowto](https://help.ubuntu.com/community/PinningHowto)

You can now use `-t stable` to install a package from the stable
repositories:

`sudo apt-get install -t stable network-manager`

Now, that package will be upgraded next time you run updates (even
unattended-upgrades will do this), so you need to tell `dpkg` to `hold` the
package and add it to the "Don't auto-upgrade this one" list:

If you have unattended-upgrades installed and configured, you should
see this section in `/etc/apt/apt.conf.d/50unattended-upgrades`:

```
// List of packages to not update (regexp are supported)
Unattended-Upgrade::Package-Blacklist {
//      "vim";
//      "libc6";
//      "libc6-dev";
//      "libc6-i686";
};
```

Add your package to that list:

```
// List of packages to not update (regexp are supported)
Unattended-Upgrade::Package-Blacklist {
//      "vim";
//      "libc6";
//      "libc6-dev";
//      "libc6-i686";
        "network-manager";
};
```

Now unattended-upgrades won't upgrade that package, but you still can
accidentally upgrade it if you use `sudo apt-get upgrade` or `sudo
apt-get dist-upgrade`, so here's how to fix that problem:

Run: `echo "network-manager hold" | sudo dpkg --set-selections`

**Note:** You don't want to leave packages on hold forever, that would
be **very very bad**. Packages regularly get security updates, and
even the bug you suffer from currently will eventually be fixed (watch
the Debian bug tracker, it's helpful), so you'll want to remove the
hold once a new version of the package has been released. **If you
keep a package on hold forever, you will compromise the security of
your system!**

To "unhold" a package, use this command:

`echo "network-manager install" | sudo dpkg --set-selections`

Now you can `sudo apt-get upgrade` or `sudo apt-get dist-upgrade` to
get the latest release of the package.

If you have any questions or comments, hit up the section below.
