---
layout: post
title: "New Project: PodcastMaker"
date: 2019-12-17 08:39:07 PST
categories: [Code,Projects]
emoji: true
bg: false
---

I've launched a new project: PodcastMaker. TLDR: It is a program that converts YouTube channels into real podcast feeds complete with video enclosures.

The real star of the show is [`youtube-dl`](https://ytdl-org.github.io/youtube-dl/), which does all the video downloading and hard work. To get started, check out the [repository and the Readme](https://gitlab.com/samurailink3/podcastmaker/tree/master).

Feel free to contribute, submit bugs, or just let me know how its working for you. PodcastMaker is released into the Public Domain and uses the [Unlicense](https://unlicense.org/), just like `youtube-dl`. Feel free to fork it, build things with it, whatever you want to do. This code is truly unencumbered. Go have fun. Or not.

[https://gitlab.com/samurailink3/podcastmaker/tree/master](https://gitlab.com/samurailink3/podcastmaker/tree/master)