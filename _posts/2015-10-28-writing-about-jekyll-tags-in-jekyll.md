---
layout: post
title: "Writing about Jekyll Tags in Jekyll"
date: 2015-10-28 06:08:45 EDT
comments: true
categories: [Code]
bg: false
---

Just ran into an annoying situation. How do you write about Jekyll tags
inside of Jekyll? How do you get it to avoid parsing?

The best answer I've found is [this Stack Overflow
answer](https://stackoverflow.com/questions/3426182/how-to-escape-liquid-template-tags/5866429#5866429).

**Updated 2019-01-29:** This post keeps causing errors in recent versions of
Jekyll. Just check the Stack Overflow link.
