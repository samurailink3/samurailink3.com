---
layout: post
title: "New PGP Key"
date: 2017-02-16 10:05:15 EST
comments: true
categories: [News]
emoji: false
bg: false
---

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I have signed the text of this blog post with both my new and old keys for
verification purposes.

I've created a new PGP key. You can find it over on the [PGP Page](/pgp/).
Additionally, you will find links to the MIT and SKS keyservers where you can
verify the key. You may also find me (and my identity proofs) [on
Keybase](https://keybase.io/samurailink3). The new key
(37E8CF026EACC295F9EC9CB9CB744273EDA0E0BB) has been signed by my old key
(11C930C4693A6C9B789BB0F76442DF0A14BA4EFD) so you can verify that the new key is
valid. The old key can be found on [the PGP Archive page](/pgp/archive/).
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEN+jPAm6swpX57Jy5y3RCc+2g4LsFAlilwWsACgkQy3RCc+2g
4Lvp5w/+NZO+bhDD9CC0cOS+834vb3vD4F3j8AlE57pQbvn9MKpKoWaVS05Q3Q/w
rIVlAT5HW4UDW6n9vxn/2gNBcymZkGTHP3fNs4rufkYLySgWyZuGJwiZIUTgJ1Nz
gbJoZtQ2YBUZBIecCv9xg+Xcb5vLInTsm1jz39lpLR4FUIhOeojBD/YTq3Z7or+M
Jjwas0rbluhcmrzho3OjBisrxQg3S5GzNhSS4nbRzCKW6Ys9UiylW5P3ERrYHcVZ
BWtpjz+LTmSfAZmQ8i0NKFloDyErHxAa9Sv/Ojp1ooENB/9EmxuYg+wNufw8X0sk
/OiLCZVYvJG5gkCpN0q9HYt5rFMYcYJRxidPv4tfXmViQWALKPnq6s0YO2cAwijF
jk1nTKDGlJG/CUsBP65gQHPksA9mtpfJu0pf+6zTT7LJrTk3gZHp/xBk9fc6Gxgd
QJVOjY8ErQ1IDzJ5WU+Q5se2McBKoZ2LvaQUlHJrEZi8XGwx0g5C4AHioTPMtHPd
BdHK28E7qG3K/0zeI8lKC2EODD0yWm4Rl1BPU6j9VxTPns1jmTkYNZWqoSZwe0Se
eWIvnl5frQr6Yj8Mh4SpOoa3IbAGnWzUhXyPBJXrEb5SpFDuorG3rrikDw/2WV2I
XJlVY5o4diXwKlWgBNk8Umvvbicd0q/C5EYNCLnOF1NQj7anfss=
=clqN
-----END PGP SIGNATURE-----
```

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I have signed the text of this blog post with both my new and old keys for
verification purposes.

I've created a new PGP key. You can find it over on the [PGP Page](/pgp/).
Additionally, you will find links to the MIT and SKS keyservers where you can
verify the key. You may also find me (and my identity proofs) [on
Keybase](https://keybase.io/samurailink3). The new key
(37E8CF026EACC295F9EC9CB9CB744273EDA0E0BB) has been signed by my old key
(11C930C4693A6C9B789BB0F76442DF0A14BA4EFD) so you can verify that the new key is
valid. The old key can be found on [the PGP Archive page](/pgp/archive/).
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEEckwxGk6bJt4m7D3ZELfChS6Tv0FAlilwY0ACgkQZELfChS6
Tv3LDg//bFXLrGtb0ZBYKBl+PJMXQJpkMpSkM/YX4uTDdiUXdxT/ThoHqtMKBKtx
MqOKtjoInLiBlkKsqXjHUJGPPG6RAwfZhwQUWgtXF49j3kzv0isgye/b8an4XCor
d7dbxN/2fuOzCsf7s6FZqx17KfJmDfky84fQmc+yJ+XjJeWkEcf53CY4A4vkaBie
xYoF68ZDF09+zizGc+CqyyoeOt58j5XiiJ3x3K/IEhJIHcc5CTVlsjy1rRWkiHIL
9mcQJpSJmTP75we/N2JBsCQKVXEr1BPneyzBWBP4NOdGew42zGaJwwmGChL+PfMf
QkX9tQQeCOBEoPus1KnCVUTAtHmKZftONZ0w6wWDq8VNU1T8TLRxgahM6TjyQeQf
rzBPkVR50su/FclLj9v11tUWIxYkBdhQuW16X1szkkAKdnIrmfODUiIzl+44MeRx
sHkaDAsE1TQMlht/lxPmniJXOIGsumaIrghsURTyyy2z4IM1npXmm/LLKgKKLYvT
f7IjXWAHAylrYbxw9pp0K725X15eGdTG9rPpT/b7HXodgoWibHtEHBZxq7EoobKN
NXJE9n55wB20QAwXH4/LAnbaxkyiDnCpekJAf7GHviB9TSKK25nTzlYE1mRqH/9P
Kc35tdlb7DyU3fWTqkWFgsZFJbVxO9l65DIYVcLuRUhx3NOZh+s=
=78aO
-----END PGP SIGNATURE-----
```
