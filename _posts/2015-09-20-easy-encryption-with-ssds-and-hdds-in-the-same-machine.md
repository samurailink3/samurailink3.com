---
layout: post
title: "Easy Encryption with SSDs and HDDs in the same machine"
date: 2015-09-20 16:12:35 EDT
comments: true
categories: [Linux, Tutorials]
---

If you have a Linux machine with and SSD for one part of the
filesystem, but need HDDs for the large storage capacity, encryption
can become a pretty huge pain.

If you encrypt multiple filesystems across multiple disks, LVM is the
proper choice, but you have a solid state disk you want to keep for
it's intended purpose: Booting your system quickly and making
applications launch as fast as possible. If you keep all drives in
LVM, some data will end up on physical volumes and slow your rig down.

So how do we boot our system without needing two or more LUKS
passphrases on boot? How do we make it so one password rules them all?

Enter `crypttab`.

`/etc/crypttab` is like `fstab` for your encrypted filesystem
components, and it's really easy to get the hang of. Just a
disclaimer: Using a single LUKS passphrase to unlock all drives is
technically less safe than using a different passphrase for each
drive, but it is way more convenient. That's the ever-long battle:
Convenience vs Security.

If you have [cryptsetup](http://linux.die.net/man/8/cryptsetup)
installed, you should have `/etc/crypttab` in place already, just with
everything commented out. The provided examples make this pretty easy
to figure out:

```
# <name>       <device>                                     <password>              <options>
# home         UUID=b8ad5c18-f445-495d-9095-c9ec4f9d2f37    /etc/mypassword1
# data1        /dev/sda3                                    /etc/mypassword2
# data2        /dev/sda5                                    /etc/cryptfs.key
# swap         /dev/sdx4                                    /dev/urandom            swap,cipher=aes-cbc-essiv:sha256,size=256
# vol          /dev/sdb7                                    none
```

From this file, we can see that the LUKS volume named `home` has a
specific UUID and a keyfile located at `/etc/mypasswd1`. The `swap`
LUKS volume is encrypted randomly on each boot by `/dev/urandom`. The
`vol` LUKS volume has `none` in the password field, meaning it will
ask you for a password at mount time.

With `crypttab`, we can use the combination of a single passphrase for
the root drive (your SSD), then keyfiles for the rest of the encrypted
hard drives.

1. Install your Linux system normally, on an encrypted LVM on the SSD.
2. Create a new key file for your new drive. We're going to use
   `/dev/urandom` and make a 5MB base64-encoded keyfile. While it
   would be more secure to use `/dev/random`, this will take a very
   very long time. Use it if you feel it is neccessary, but keep in
   mind, this is a single-passphrase boot, if your passphrase is poor,
   no amount of `/dev/random` will save you.
    * `dd if=/dev/urandom bs=1M count=5 | base64 > ~/.HDDkey`
3. Now we need to format your hard drive with the key you just
   created:
    * `cryptsetup luksFormat -d ~/.HDDkey /dev/sde`
4. Now map it, format it, then unmap it:
    * `cryptsetup luksOpen -d ~/.HDDkey /dev/sdd BigStorage`
    * `mkfs.ext4 -L BigStorage /dev/mapper/BigStorage`
	* `cryptsetup luksClose BigStorage`
5. Now find the UUID of your drive:
    * `blkid`
    * Find the device identifier of your new encrypted drive, in my
      case, mine is `/dev/sde`.
	* Copy out the UUID, mine is
      `c7792c2a-78fb-425a-8971-6df1c5d5b79c`.
6. Now, let's add this line to `/etc/crypttab` so it will
   automatically unlock `/dev/sde` when the `/etc` filesystem is
   available:
    * `BigStorage UUID=c7792c2a-78fb-425a-8971-6df1c5d5b79c
      /home/samurailink3/.HDDkey`
    * `BigStorage` is going to be the name of the LUKS device exposed
      in `/dev/mapper`.
7. Now the device will be mapped on boot and ready to mount. Here's
   what a line in your `/etc/fstab` should look like if you want to
   mount it somewhere specific, with user and exec access.
    * `/dev/mapper/BigStorage				/run/media/samurailink3/BigStorage	ext4
    user,exec	0 0`
8. Now all that's left is to use the device. If you're treating it like
   an external drive, there's nothing you need to do now, the device will be
   available at `/run/media/samurailink3/BigStorage/`. What I like to do is
   symlink out folders from my existing home directory to the larger drive
   for big files that don't need fast access, like video files or music.
   Here's an `ls -l ~` for an example:

```
lrwxrwxrwx  1 samurailink3 samurailink3      44 May 24 09:33 SteamLibrary -> /run/media/samurailink3/BigData/SteamLibrary
lrwxrwxrwx  1 samurailink3 samurailink3      35 May 24 09:33 tmp -> /run/media/samurailink3/BigData/tmp
lrwxrwxrwx  1 samurailink3 samurailink3      38 May 24 09:33 Videos -> /run/media/samurailink3/BigData/Videos
```

Now you have all of your system drives encrypted, with one passphrase.
Pretty convenient and way more secure than running with just one drive
encrypted.
