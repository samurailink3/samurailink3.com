---
title: "We're Watching You, Facebook."
layout: "post"
date: "2010-06-01 03:23:00"
categories: [Rants]
---

As you all know, a while ago [I deleted my Facebook
account](http://samurailink3.blogspot.com/2010/05/dear-facebook-its-
over.html), and it was due in part to a few things. First off, the [constantly
changing and ever-confusing privacy
settings](http://www.nytimes.com/interactive/2010/05/12/business/facebook-
privacy.html). Second, [my postings being censored on
Facebook](http://samurailink3.blogspot.com/2010/05/dear-facebook-its-
over.html). Third, and frankly, the biggest reason: I didn't want to encourage
my friends to use a system that would put their data and privacy at risk.
Well... Reason number one (and therefore number three) **[have been
fixed](http://www.huffingtonpost.com/larry-magid/new-facebook-privacy-
sett_b_590279.html).** The jury is still out on reason number two, but I feel
that with my continued reliance on [free](http://picasaweb.google.com/)
[and](http://twitter.com/) [open](http://www.google.com/profiles) services,
and the fact that I will be using Facebook for links to the outside, free,
open web, that the censorship will be minimal if at all.Facebook is in a very
precarious position right now, the entire tech industry and all of the major
media outlets are watching them and what they do. [And they know
it](http://www.washingtonpost.com/wp-
dyn/content/article/2010/05/23/AR2010052303828.html). Zuckerberg and the
Facebook executive members have come out of the woodwork to explain that
they've "Missed the mark." Amazingly enough, another company that shares some
of what Facebook is currently going through is Toyota. With the Prius going
through numerous product recalls, the most media-laden being a [braking
problem due to a software
bug](http://www.cnn.com/2010/WORLD/asiapcf/02/04/japan.prius.complaints/index.html),
Toyota is feeling the consumer-distrust-pinch just as much as Facebook is
right now. Although.. this doesn't have to work in the disinterest of
consumers/users.  
A few people have told me something along the lines of, "If I'm going to buy a
car anytime in the next year, it will be a Prius.", and while this may sound
absolutely insane, it makes logical sense. Toyota is being watched. By
everyone. Each and every consumer, safety agencies, government regulators,
factory workers. Everyone that so much as glances at a Prius are doing their
part to inspect, double and triple-check the design and safety of the vehicle
because of the pressure. You, as a consumer, can be guaranteed, that the Prius
is going to be the safest, most inspected, most tested car of this year.
Toyota can't screw it up again, it would be a death-knell for the model as
well as the global image of the entire company. Toyota is walking on eggshells
for a good reason and the biggest winners are the consumers.  
Same story with Facebook. They've fucked up; and this time, they've fucked up
bad enough to anger their entire userbase, [cause a media
outcry](http://www.cnn.com/2010/OPINION/05/26/wolf.polonetsky.obama.oil/?hpt=T2),
and drive users to create [worldwide movement to quit the service
entirely](http://www.news.com.au/technology/users-kill-profile-on-quit-
facebook-day/story-e6frfro0-1225873910561). If any online service is
positioned to lose it all over one more misstep, its Facebook. One more
privacy violation, [information-leaking
bug](http://mashable.com/2010/05/05/facebook-chat-down/), or [advertising
leak](http://www.mercurynews.com/breaking-news/ci_15129607?nclick_check=1),
and people _will_ jump ship, and for good this time. This month, Facebook got
a wake-up call from its more prominent, and more vocal userbase, and if they
want to stand any chance at all at winning these users back (and keeping
them), they need to be very, _very,_ **very,** careful about how they go about
changing policies, or introducing new features.  
This is why I've decided to come back to Facebook. They really messed up, they
got the attention of the entire tech world, and even a good portion of the
'normal' world's media. They can't afford to make another mistake. If Facebook
even so much as breathes the wrong way, everyone and their mother will be
grabbing the torches and pitchforks to put down this monster once and for all.
Facebook is incredibly convenient, but at what cost? For now, I feel a bit
safer knowing that the entire tech industry will be holding this service
accountable for how they treat their users. Again, they haven't fixed
_everything_, at all. There is still a huge issue with exporting data from the
service. Baby steps. They've done a good thing this week, I for one hope this
isn't a one-time scenario, I really want Facebook to start being a more open
platform. There are some things that I will not change about my decision to
untie myself from the platform, however: I will continue to use Buzz, Twitter,
PicasaWeb, and other open sites for my content over Facebook. My Facebook page
will serve a dual purpose: Aggregation and social connectivity. When I publish
content or post images, it won't be a Facebook post or uploaded to a Facebook
photo album; Facebook will be getting a link to these other places on the web.
While this might be slightly annoying for my Facebook followers, I can't take
the chance of any of my content getting locked into Facebook permanently. I
won't do it. Congratulations, Zuckerberg, you've won back this user, but
remember, as soon as you fuck up again, I'm out and I'll take people with me.
We're watching you, Facebook, and you can't afford to let us down again.  
Either way, none of this will matter once [Diaspora comes out in
September](http://joindiaspora.com/), but that's another post for another day.
