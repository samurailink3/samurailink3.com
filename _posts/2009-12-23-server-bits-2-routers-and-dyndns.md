---
title: "Server-Bits #2: Routers and DynDNS"
layout: "post"
date: "2009-12-23 22:39:00"
categories: [Tutorials, Linux]
---

Ok! Now that you have a working Ubuntu box, we need to forge a path to get to
it from the outside. This tutorial will be... well.. completely and utterly
useless unless you follow the next one. **Especially the next one**. The first
thing we need to do is set up your router to forward ports. You should become
_extremely_ familiar with this process, as you'll be doing a great deal of
port forwarding over the next few tutorials.  
The first thing you will want to do is head over to
[PortForward.com](http://portforward.com/), look up your router model, follow
the general instructions.  

The basic ports you should forward are:  

  * 22 - SSH
  * 80 - HTTP

For now... This list will grow as we add more to your server.  

Now... We need to get you off of that bare IP address, and onto a domain name.
Sure, you could spend the $10 bucks a year to buy out a .com domain name, but
this tutorial is based around the idea that you want to spend as little as
possible. This is where [DynDNS](http://dyndns.com/) comes in. Head over to
[DynDNS.com](http://dyndns.com/) and create yourself a free account. Go ahead
and go to the "My Hosts" section and add a new hostname.  

Now... this is the important part: Naming. Go ahead, no rules here, one of the
most pivitol moments in creating your network will be naming your network.
Service type can remain at "Host with IP Address", and go ahead and use your
external IP address. You can leave "Mail Routing" unchecked for now. **It will
take a bit for the DNS records to propagate throughout the internet**. I've
had this process take 10 minutes, I've had this process take 4 hours. Just sit
back, have a mug of coffee, and enjoy yourself, you are well on your way to
having a workable server.  

We will get to testing your domain in the next tutorials.  

**Still coming up:**  

  * **SSH**
  * **Web Server (Apache2)**
  * **Wordpress**
  * **FTP**
  * **Streaming Music Server (Sockso)**
  * **Remote Bittorrent Administration (Transmission)**
  * **and more...**

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
