---
title: "Adventures in Adventure 2: Pizza, Tech, Cabbies, and The Blues"
layout: "post"
date: "2010-01-03 05:24:00"
categories: [News]
---

First off: The trains in Chicago are wonderful. Cheap, easy, a much better
option than finding parking (usually about $16-$20 bucks a day) on the street.
Buses are pretty nice too. Public transit isn't the awful boon that most
people make it out to be if managed correctly. A few hours in and we were
planning routes around city blocks easily enough. The experience in this city
makes me wish that there was more (_and better managed_) public transit in the
US.
We stopped by the Sears Tower (Now, The Willis Tower), and decided to take
their Skybox tour. Here is the video there:

  <object height="344" width="425"><param name="movie" value="http://www.youtube.com/v/0m8unZ1WLmE&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/0m8unZ1WLmE&hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>

We also went to Gino's East of Chicago Pizza. _THE_ Chicago Pizza Place. The
food was less like Pizza and more like Soup. It was absolutely amazing.
[Rob](http://twitter.com/derangedteddy) and I couldn't even finish the small
pizza we ordered. Gino's is officially the best pizza I have ever consumed.

  <object height="344" width="425"><param name="movie" value="http://www.youtube.com/v/_-RKL13pMNs&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/_-RKL13pMNs&hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>

We also traveled to the Museum of Technology and Industry (Video still
pending, TONS of content), it wasn't the most comprehensive museum I've been
to, but it had more than its fair share of _"Good shit that was amazing."_
moments. Like I said, video still pending, but when it is up, I'll make sure
to post it.

Totally forgot to talk about the Blues bar we went to! We hit up Buddy Guys
Legends bar, great atmosphere, food was a good price, drink prices were
through the roof, but it was a good time overall. The music selection was
good, the guys on-stage were plenty talented, but we felt the music lacked
feeling, like playing the Blues was more of their night job, just doing it to
get by, they weren't really feeling the music they were playing. Other than
that, a wonderful experience.

Side note: Cabbies like to take the long way around, unless you wanna get
jacked an extra $.50, make sure you're riding with someone who knows their way
around town (Thanks, [Rob](http://twitter.com/derangedteddy)).
