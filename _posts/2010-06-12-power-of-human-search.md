---
title: "The Power of Human Search"
layout: "post"
date: "2010-06-12 00:23:00"
categories: [Rants]
---

[{% img center ../../../../../images/2010-06-12-power-of-human-search-1.jpg %}](../../../../../images/2010-06-12-power-of-human-search-1.jpg)


After listening to [This Week in Google: Episode 46](http://twit.tv/twig46), I
started to think about human-powered search in a different light. Human-
curated search engines have been around since [the dawn of
time](http://en.wikipedia.org/wiki/2005), but none of them have ever gained
any hugely significant market share when stacked up against the big players
([Google](http://www.google.com/), [Yahoo](http://search.yahoo.com/),
[MSN/Bing](http://bing.com/)). Have you ever heard of
[Mahalo](http://www.mahalo.com/)? They are one of the biggest players in this
space and they are on the cusp of the unknown in the search world. Gaining
most of their popularity through their confident and highly skilled CEO [Jason
Calacanis](http://calacanis.com/), Mahalo is amazingly stable in a business
topography that changes radically. With the [recent debates over
Facebook](http://samurailink3.blogspot.com/2010/05/dear-facebook-its-
over.html), [Google gaining access to Twitter's data
firehose](http://www.pcworld.com/article/174101/google_gets_some_twitter_search_love_too.html),
and now people looking at Facebook for real-time search results, I've been
struck with a question that I just can't seem to shake off.

**Why hasn't human-powered search ever taken off?**

After thinking about this for a while, I've come to the conclusion that the
biggest factor against human-powered search engines is that they are all
missing one key thing: Humans. If you build a search engine that is entirely
dependent on people, you need one key thing to make it work: People! This is
what is so innately brilliant about social networks like Facebook and Twitter
is that they've gained an enormous amount of active users. Plenty of people
posting statuses, recommendations, and location data, all because they want
to. Search is just a natural evolution for these services. With a product like
Twitter, rolling out search is easy, the service is public-by-default, and
people know this. They won't mind being rolled into a search index. Facebook
is a more complicated venture. Only the people who want to share with the
world should be rolled into the search index. But what about link stats,
trends, and general user analytics? Should users be rolled into this non-
descriptive, very general, non-identifying trend-graph? That is the question
that is currently posing Facebook, and with the recent outcry over their
privacy settings (and priorities), they are closely watching what they do with
their user data (as they should).  This isn't to say, however, that completely
public-by-choice users shouldn't be used to build a people-powered search
engine. They should! As a matter of fact, it just might be 'cool' enough to
power a search engine that it would force people to start opening holes in
their Facebook privacy for just this reason. If there is one thing that is for
certain: People want to feel needed, the want to feel like part of a larger
movement or group, and a Facebook-Powered search engine would do just that.

Google has noticed that real-time search is the next big thing with their
release of [Caffeine](http://googleblog.blogspot.com/2010/06/our-new-search-
index-caffeine.html). Because of this new technology, Google has stated that
their speeds have increased by 50% on bringing fresh results to a search
query. Google knows that real-time results matter for content (especially news
content) and is working to bring the freshest parts of the web to you faster.
If that isn't fast enough, Google is also pulling the very latest [data from
Twitter in real time](http://mashable.com/2009/10/21/google-twitter-search-
deal/)! When you search for 'Oil Spill', you see "Latest results for Oil
Spill" and a scrolling Twitter search starts running on your results page
right under that. Google and Microsoft have both admitted that human-powered
search results, while they may never replace algorithm-based results, play an
important role in what people are looking for and how results are curated.

Human-powered results are incredibly important with breaking news items,
restaurant reviews, personal recommendations, and things that aren't
conductive to "Just Googling It". Things that are happening right now, or
situation-dependent searches are much better answered with a human-component
involved. Take the question, "What is a good present to buy a 13-year-old for
his birthday?", the best answers are taken from Wiki-Answers, Yahoo Answers,
and Yelp. All human-powered websites, built around curating and processing
varying opinions, reviews, and suggestions. The big search players have
figured this out: Human augmented search is here to stay. I believe that with
the boom in user numbers from both Facebook and Twitter, there hasn't been a
better time find users that could power an exclusively human-powered search
engine, and only time will tell if the world is ready for one to thrive.
