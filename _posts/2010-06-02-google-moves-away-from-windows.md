---
title: "Google Moves Away from Windows"
layout: "post"
date: "2010-06-02 14:29:00"
categories: [Rants]
---

So, word has been getting around that Google is officially dropping support
for Microsoft Windows internally. Computers running Microsoft Windows are
going to be phased out for Linux and OSX machines. Honestly... _Who didn't see
this coming?_  
Google has stated many times that the **default** operating system for
Googlers [is a heavily modified Long Term Support (LTS) version of Ubuntu
Linux](http://www.informationweek.com/news/software/open_source/showArticle.jhtml?articleID=178600189),
affectionately named "[Goobuntu](http://en.wikipedia.org/wiki/Goobuntu)".
While this modified distribution has never been released outside of Google, it
is in wide use and support there, and hardly a secret. That said, Windows
machines aren't being done away with entirely, Google has stated that
employees that really need to use Windows can acquire special permissions to
use the operating system. Lets take this from a fresh angle: **If all you
really know how to use is Windows, you probably shouldn't be working at
Google.**  
Lets think logically about what Google _really_ needs Windows machines for:
Windows development. Sure, they have Picasa, Desktop Search, Earth, and a few
other cross-platform apps that they need to build and test on versions of
Windows, but these things can easily be accomplished inside a virtual
environment. The vast majority of Google's focus right now is split between
Chrome OS (Linux), Android (Linux), and the web, and if recent trends have
shown us anything, its that Google is [interested in moving away from desktop
applications altogether](http://draft.blogger.com/). Google has proven that
they can take big technologies and move them to the web, and that's exactly
what they are focused on. Microsoft's mission used to be "A computer on every
desk and in every home, running Microsoft software.", and Google has taken a
much more open stance in theirs, what their mission should be is: "A browser
on every device, with every person, using Google products."  
If Google were a software company first-and-foremost, this would be a huge
deal, but they just aren't. Google is focused on providing platforms and
services for other people to utilize and build on. Android development tools
exist for every platform, and, from a personal point of view, development on
Linux platforms tend to be much nicer than Windows or OSX. By keeping Apple
machines around, Google is showing that they will still be developing
applications for the iPad and iPhone. This is extremely important. Google
doesn't want to limit who can use their products, so having a presence on
their biggest competitor's device is a wonderful strategy. Google isn't
interested in limiting themselves, and being browser-based is the cornerstone
of their ideals.  
It does seem like Google is setting a precedent for other companies as well.
Showing other technology businesses that they can be free from licensing and
closed, bug-ridden software. If one of the biggest technology companies in the
world can do without Microsoft Windows, anyone can. This move to make their
unreliance on Windows official and public seems like a power play to the rest
of the industry, setting an example and forging the first path away from
Windows. I'm all for more businesses relying on Linux, it will do huge amounts
of good for the open source ecosystem and mentality.  
Like I said before, I'm really floored that people are surprised over this,
everyone should have seen this coming. Only time will tell if other companies
are willing to follow Google's example and give up their Windows addiction.  

[Original Engadget post](http://www.engadget.com/2010/05/31/google-said-to-be-
moving-away-from-windows-internally-mac-and-l/)
