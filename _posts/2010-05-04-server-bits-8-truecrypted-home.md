---
title: "Server-Bits #8: TrueCrypted Home Directories or SHROUD"
layout: "post"
date: "2010-05-04 16:56:00"
categories: [Linux, Tutorials]
---

So, I've completed work (a while ago... was still gathering the time necessary
to write this blog post) on ~~Project: SHROUD~~ (Link defunct for now...) and
it is time to release the documentation on how I've put it together. The
pieces have been floating around the internet for some time, but I'm here to
put this in one central location so you can have it up and running in no time
flat.  

But what exactly is Project SHROUD?  
_From the PastaNet blog:_ "Completely encrypted storage space via PastaNet
using TrueCrypt and SSH. Hosted on an encrypted RAID-5 server and stored in
your own personal encrypted volume, your data is not only safe, but extremely
secured. Your personal file volume is dismounted 60 seconds after you
disconnect, leaving your data completely encrypted (Twice over!!). Completely
secured network access through SSH encryption. You can access your SHROUD
drive through any FTP program that supports SFTP (The vast majority of these
programs do) or by mounting it as a network drive through ExpanDrive (working
on alternatives for this) [Linux users need not apply, as mounting SSH volumes
is built into the OS]. Completely encrypted, completely secured, cloud-based
storage."  

See the full article after the jump.  


When a user logs in, their TrueCrypt volume is mounted as their home
directory. The system then watches to see when the user logs off and dismounts
their TrueCrypt volume 60 seconds after they disconnect. This SFTP storage
area can then be used through virtually any FTP program or mounted as a
Network Shared Drive for the user. These users are forbidden from shell
access, VPN, and other areas of the filesystem.  
As a server admin, it is bad karma (and very bad practice) to keep your users'
passwords. Create the volume, throw away the key, the rest is up to them. This
keeps them _and_ you safe in a worst-case-scenario.  

The first thing you need to do is create a directory in /home for the location
of the TrueCrypt volumes.  

> mkdir /home/private

In this directory, we will keep all of the SHROUD users' TrueCrypt volumes.
They will have a home directory to be used purely as a mount point for the
volume.  

Now, the difficult part (Which isn't too terribly hard), compiling a more
modern version of PAM, known to work well with automatic TrueCrypt mounting
**(Terminal Commands Ahoy!)**:  

> wget http://dl.dropbox.com/u/860936/Blog/pam_1.1.1.orig.tar.gz  
> tar xfzv pam_1.1.1.orig.tar.gz  
> cd Linux-PAM-1.1.1/  
> ./configure  
> cd modules/pam_exec/  
> make  
> sudo cp .libs/pam_exec.so /lib/security/pam_exec_UNSTABLE.so

We have just compiled the new PAM_EXEC to a new, different file on your
system. We will reference this module in the TrueCrypt-mounting script instead
of your standard PAM module. This will avoid dependency and package
confliction issues later on. We now need to edit the "Common-Auth" and
"Common-Session" text files that controls what happens when a user enters
their password. We'll add the lines via the 'echo' command with sudo
privileges.  

> sudo echo "# SHROUD code below" &gt;&gt; /etc/pam.d/common-auth  
>  
> sudo echo "auth optional pam_exec_UNSTABLE.so debug expose_authtok seteuid
/bin/bash /bin/cryptmount.sh" &gt;&gt; /etc/pam.d/common-auth  
>  
> sudo echo "# SHROUD code below" &gt;&gt; /etc/pam.d/common-session  
>  
> sudo echo "session optional pam_exec_UNSTABLE.so seteuid /bin/bash
/bin/cryptmount.sh" &gt;&gt; /etc/pam.d/common-session

 Whenever a user now logs in, the shell will automatically execute
"cryptmount.sh". One problem: cryptmount.sh doesn't exist yet. You need to
create it. Create a new file inside of '/bin' and name it "cryptmount.sh".
Here are the contents of that file:  
_[TIP: For your convenience, [here is the
file](https://docs.google.com/leaf?id=0B1Rrmx2rA2GKYjZlNTNlZDAtMmE1Ni00NmNjLWEzZDQtNTg0ODEyOTJlZjdk&hl=en)]_  


> #!/bin/bash  
> # /bin/cryptmount.sh  
>  
> CRYPTVOLUME=/home/private/$PAM_USER.tc  
> MOUNTPOINT=/home/$PAM_USER  
>  
> case "$PAM_USER" in  
>   **root | anotheruser) #homedirs of non-shroud_users are not encrypted**  
>     exit 0  
>   ;;  
> esac  
>  
> case "$PAM_TYPE" in  
>       auth )  
>          head -c -1 | truecrypt -t  \--protect-hidden=no -k "" \  
>                       "$CRYPTVOLUME" "$MOUNTPOINT"  
>       ;;  
>       close_session )  
>          MOUNTS=$(mount | grep " $MOUNTPOINT ")  
>          if test -z $MOUNTS ; then  
>            echo MOUNTS  $MOUNTS &gt; /tmp/debug  
>            exit 0  
>          fi  
>          OTHER=$(who | grep "^$PAM_USER " | grep -v " $PAM_TTY ")  
>          if test -z "$OTHER"; then  
>             echo truecrypt -d $MOUNTPOINT | at now + 1 minute  
>          fi  
>       ;;  
> esac  
> exit 0

You will need to make sure to add your username, and any other username that
_**will not**_ be using SHROUD to that list of usernames inside of the script.
Users will see an error if they are not using SHROUD, but are not listed. Here
are the rules: **Using SHROUD - Not named in the file. Not using SHROUD -
Named in the file.**  
Get it? Good.  

The next thing we need to do is limit the amount of access your SHROUD users
have. We want them to be 'jailed' into their home directory, with no hope of
escaping. We need to edit the SSHD configuration file to do this.  

> sudo nano /etc/ssh/sshd_config

Comment out the line "Subsystem sftp /usr/lib/openssh/sftp-server" by placing
a '#' next to it. It'll end up looking like this:  

> #Subsystem sftp /usr/lib/openssh/sftp-server

Now that we have the old system commented out, you will need to add the
following lines of code to your sshd_config:  
_[TIP: Use Control+O to save the file and Control+X to exit nano]_  

> #Start SHROUD code here  
> Subsystem sftp internal-sftp  
>  
>  
> UsePAM yes  
>  
>  
> Match group shroud_users

>

>> ChrootDirectory /home/%u  
> X11Forwarding no  
> AllowTcpForwarding no  
> ForceCommand internal-sftp

>  
>

We will be putting all of the SHROUD users into a group named "shroud_users"
and they will be forced to the following rules:  

  * They will be forced into their home directory, they will not have full filesystem access.
  * They will not be able to forward graphical (X11) applications.
  * They will not be able to forward TCP (They do not have VPN access)
  * When they log on, the command 'internal-sftp' is used instead of a standard shell. This prevents them from having shell access to the server.

You can also give granular permissions to specific users. Lets say that you
want **bob** to have SHROUD, but would also like to give him the ability to
use VPN:  

> Match user **bob**

> > ChrootDirectory /home/%u  
> X11Forwarding no  
> AllowTcpForwarding **yes**  
> ForceCommand internal-sftp

You can set specific user permissions this way. You can even remove the
'chroot' bit if you wanted to give a user permission to roam about your
filesystem, taking whatever they have access to (this is helpful if you are
running a distribution center).  

The next step is to make a script to make building the users and setting the
permissions correctly stupidly easy for you to do. In my experience, creating
a SHROUD user with 1GB of usable space took about 10-12 minutes, depending on
how much I fat-fingered the commands. With this script, you'll be creating
users in 1-2 minutes:  
_[TIP: READ THE COMMENTS!! Commented code (any line starting with a '#') is an
easy way to figure out exactly what is going on. Use this script as a learning
experience!]_  

> #!/bin/bash  
> # This program is free software: you can redistribute it and/or modify  
> # it under the terms of the GNU General Public License as published by  
> # the Free Software Foundation, either version 3 of the License, or  
> # (at your option) any later version.  
> #  
> # This program is distributed in the hope that it will be useful,  
> # but WITHOUT ANY WARRANTY; without even the implied warranty of  
> # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
> # GNU General Public License for more details.  
> #  
> # You should have received a copy of the GNU General Public License  
> # along with this program. If not, see http://www.gnu.org/licenses/gpl.html.  
> # Original code by SamuraiLink3 (2010)  
>  
> MakeUser() {  
>

>

>> #The commands below will set the variable names for both the user and the
user's password. These will then be used to create the new user, volume,
directory structure, and set them all in motion.  
>  echo  
>  echo -n "Enter username for new SHROUD user: "  
>  read user  
>  echo ""  
>  echo -n "Now enter the password for the new user: "  
>  read password  
>  VolumeCreateAndMove

>

> }  
> VolumeCreateAndMove() {  
>

>

>> echo "We need sudo permission to continue..."  
>  sudo echo ""  
>  cat /dev/urandom | head -c 4096 &gt; rand.txt  
>  #This command has created a random 4096-byte text file from /dev/urandom
for random salt used to create the TrueCrypt volume  
>  truecrypt --text -c $user -p $password --encryption=AES --filesystem=none
--hash=ripemd-160 --size=1073741824 --random-source=rand.txt -v --non-
interactive --volume-type=normal --keyfiles=  
>  #This command creates a TrueCrypt volume with the name of the user and the
password you chose at the start of the script. The size is 1GB in bytes. It is
using AES and RIPEMD-160 to create a normal TrueCrypt volume.  
>  sudo truecrypt --text --filesystem=none --password=$password --keyfiles=
--protect-hidden=no $user  
>  sudo truecrypt --text -l  
>  #This will mount the new user volume and list the TrueCrypt volumes for the
next step.  
>  echo  
>  echo -n "Please choose the mapper number you would like to format: "  
>  read number  
>  sleep 5  
>  sudo mkfs.ext4 /dev/mapper/truecrypt$number  
>  #After the volume is mounted, the admin will need to choose which TrueCrypt
mount to format (DO NOT FORMAT THE WRONG ONE!!). After this, an EXT4
filesystem is created within the volume.  
>  sudo mkdir /home/$user  
>  sudo truecrypt -d $user  
>  sudo truecrypt --text --password=$password --keyfiles= --protect-hidden=no
$user /home/$user  
>  sleep 3  
>  sudo cp -r /etc/ServerSoftware/SHROUD/* /home/$user/  
>  #This is the directory you will need to populate with your wanted default
folder structure, change this at your will.  
>  UserAddAndConfigure  
>  #Now, the TrueCrypt volume is mounted as the user's home directory and a
folder structure is copied into the new volume. You will need to create this
folder structure on your own server. Mine is as follows: Code Documents
Downloads Misc Music Pictures Videos. Create any directories/files you would
like to be pushed and defaulted to all of your users.

>

> }  
> UserAddAndConfigure() {  
>

>

>> sudo useradd -N -M -g shroud_users -s /bin/sh -b /home/$user -p $password
$user  
>  #The user is created with the 'shroud_users' group, will not create a home
directory, and will not create a specific user-group (as we are placing them
into the 'shroud_users' group).  
>  sudo chown -R $user /home/$user/  
>  #This makes the user the owner of all the folders within their home
directory  
>  sudo chmod -R 700 /home/$user/  
>  #This sets secure permissions on the folders of the user  
>  sudo chown root /home/$user/  
>  sudo chgrp shroud_users /home/$user/  
>  sudo chmod 750 /home/$user/  
>  #For chroot to work correctly, the chrooted directory needs to be owned by
root and not writable by any other party. To satisfy these requirements, we
make the owner of the main folder, root, but we make the folders inside of the
home directory, owned and writable by the user. This is why the top level
directory is not writable in SHROUD, but everything inside of it is. A slight
workaround, but it works for now and maintains security.  
>  sudo truecrypt --text -d $user  
>  #This dismounts the user's directory  
>  echo $user:$password | sudo chpasswd  
>  #This command 'chpasswd' works differently from 'passwd', it is a scripted
way to change a user's password. While it is normally used to change a very
large list of users, it works nicely for our script as well.  
>  sudo mv $user /home/private/$user.tc  
>  #This will move the user's TrueCrypt volume to the /home/private directory
so cryptmount.sh can get to it.  
>  Cleanup

>

> }  
>

>

>> Cleanup() {  
>  echo "User: "$user" created."  
>  rm rand.txt  
>  #Nicely removes the random-salt text file.  
>  echo "Cleanup complete."  
>  echo  
>  echo "========================"  
>  echo "USER DETAILS:"  
>  echo "USERNAME: "$user  
>  echo "PASSWORD: "$password  
>  echo "========================"  
>  #Echo's the username and password so the user can writedown/memorize/backup
their username and password.  
>  echo  
>  echo "Program complete, now exiting."

>

> }  
> MakeUser

To make it easier on you (Because copy/paste can be fickle with some text
editors/IDEs) [here is the
file](https://docs.google.com/leaf?id=0B1Rrmx2rA2GKZjk1ZDg5ZDctNzQ5Yy00YWNiLWEyODYtMzliZTc5MGY4Y2Rh&hl=en),
I recommend reading the file with [Notepad++](http://notepad-
plus.sourceforge.net/uk/site.htm) on Windows or
[Gedit](http://projects.gnome.org/gedit/) on Linux.  

Now all that is left is to create your first user, distribute a connection
program (such as [FileZilla
Portable](http://portableapps.com/apps/internet/filezilla_portable)), and
start providing encrypted storage. So far, this has been the biggest
undertaking on PastaNet and the biggest post on this blog. Enjoy it.  

To make things easier on my users, I have rolled a modified [FileZilla
Portable](http://portableapps.com/apps/internet/filezilla_portable) zip file
for them to download and easily connect to my server. The modifications were
trivial, I just saved the standard connection profile to the 'saved sites'
button in FileZilla, then re-zipped the folder. I provide this file to each
user that will be using the SHROUD system. I suggest doing the same for your
users. [Here is the documentation I provide for my
users](https://docs.google.com/fileview?id=0B1Rrmx2rA2GKOTQ0ODViYzEtYjI4MS00NGZmLTgxNDItYTg0MDIxYWIyYjk4&hl=en),
I also suggest that you build some form of documentation that your users can
dig through as well.  

There are a few things I would like to be made known, there are a few quirks
with Project SHROUD that you _(and your users) _should be aware of:  

  * The user cannot add/remove files/directories to the top-level (or ‘root’) directory. You should create a folder structure that your users can utilize to organize their files.
  * Due to the very nature of SFTP, it is impossible to determine the amount of free space you have left on your SHROUD drive. The easiest way to tell is to check the amount of space your files are currently taking up, then subtract the amount of known space you have been allotted. This is quite the annoying bug, but is inherent to SFTP itself, not much can be done to fix it without going to the protocol level.
  * If a user's machine is compromised by keyloggers/spyware/viruses, the user's password could be exposed, rendering the encrypted data and user account vulnerable.
  * By the same token, if a user's machine is stolen/remote-controlled while the drive is mounted, the data the attacker could be after is already decrypted.
  * Saving your password is not recommended, especially if your system is unencrypted.
  * If a user loses/forgets/erases their password: **There is no backdoor.** TrueCrypt does not provide any hidden routes or safety nets. The user's data is stored using state-of-the-art encryption, if the password is gone, the data is gone. Its as simple as that.


**This post heavily influenced, paraphrased, and copied (with very slight modification) from the following sources:**  
**  
[Little Impact - Automatic encryption of home directories using TrueCrypt 6.2a
and pam_exec](http://blog.littleimpact.de/index.php/2009/09/14/automatic-
encryption-of-home-directories-using-truecrypt-62-and-pam_exec/)**  
**[Debian-Administration - OpenSSH SFTP chroot() with ChrootDirectory](http://www.debian-administration.org/articles/590)**  
**[OpenBSD journal @ undeadly.org - Chroot in OpenSSH](http://undeadly.org/cgi?action=article&sid=20080220110039) \- Contributed by [merdely (Mike Erdely)](http://erdelynet.com/)**  
**[This UbuntuForums.org post](http://ubuntuforums.org/showthread.php?p=9014786#post9014786) and the UbuntuForums community in general, this community is one of the best I have ever been involved with and they make me a smarter person every time I log on. I want to specifically thank [cdenley](http://ubuntuforums.org/member.php?u=204457) and [sublimination](http://ubuntuforums.org/member.php?u=329678) for all the help they provided me with on this project.**  

**These people have done the amazing legwork. Go to their blog, leave comments, click ads, donate cash, do something if this post helped you out at all. Without them, Project: SHROUD would just be a twinkle in my eye. Props to them!!**  

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
