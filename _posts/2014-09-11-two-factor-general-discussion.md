---
layout: post
title: "2-Factor: General Discussion"
date: 2014-09-11 12:00 EDT
comments: true
categories: [Security, Talks]
---

New security talk! Pretty simple, stuff we've seen before, but packaged
to give in 30-60 minutes to an audience who may or may not know about
2-factor shortcomings. I gave this talk at
[OISF](http://www.ohioinfosec.org/) and it went over pretty well. You can
find it [here](/talks/2-factor/).
