---
title: "Server-Bits: Tip #2"
layout: "post"
date: "2010-04-19 02:57:00"
categories: [Tutorials, Linux]
---

Just a cool tip I found to hide the user list on the logon screen. I have a
great number of users, and the list was getting pretty ridiculous... _This
command will restart your xserver. Save your work beforehand._  

> **sudo gconftool-2 --direct --config-source
xml:readwrite:/etc/gconf/gconf.xml.mandatory --type Boolean --set
/apps/gdm/simple-greeter/disable_user_list True  
>  
> sudo /etc/init.d/gdm restart**



And that will give you a nice clean username/password box.  

[Via: Ubuntu Geek: http://www.ubuntugeek.com/how-to-remove-hide-users-list-at-
login-screen-in-ubuntu-9-10-karmic.html](http://www.ubuntugeek.com/how-to-
remove-hide-users-list-at-login-screen-in-ubuntu-9-10-karmic.html)  


_About Server-Bits:_  


If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster-server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
