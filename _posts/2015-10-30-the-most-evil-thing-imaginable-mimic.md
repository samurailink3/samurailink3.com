---
layout: post
title: "The Most Evil Thing Imaginable: Mimic"
date: 2015-10-30 06:50:03 EDT
comments: true
categories: [Link]
bg: false
---

This is just truly evil, and amazing to witness. [Greg Toombs'
(reinderien) new project Mimic](https://github.com/reinderien/mimic). It
randomly replaces characters in someone's source code with
unicode-lookalikes. Just the other day I had to deal with a dataset that
had some [unicode
spaces](https://en.wikipedia.org/wiki/Whitespace_character) in it and I
thought I might pull my hair out.

Anyway, awesome project and it does include a "Reverse Mimic" function,
so you can fix your files if you suspect someone has mimicked them. [Go
check it out](https://github.com/reinderien/mimic).
