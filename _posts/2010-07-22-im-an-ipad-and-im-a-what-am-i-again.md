---
layout: post
title: "I'm an iPad. And I'm a... What am I again?"
date: 2010-07-22 11:50:00 EDT
comments: true
categories: [News]
---

This article is in response to the new ASUS eee Pads that are coming out.
[Engadget did run a preview article on them](http://www.engadget.com/2010/05/31/asus-eee-pad-ep101tc-and-ep121-hands-on/) and I would like to share my
thoughts on the subject: I really like the idea of a tablet that actually
contains any resemblance of functionality. The only problem I have with
these and most other non-apple tablets is that they are almost all based
off of Windows 7. Now, I don't have a problem with Windows 7, at all. I
think it is the best version of Windows ever created, and it takes a lot
for me to say that, but you cannot make a true, intuitive, responsive
touch-based interface by putting a skin over an existing,
built-for-mouse-and-keyboard operating system. Windows 7 with NEVER be a
\*great\* touch operating system, just because that wasn't the main focus
when creating the operating system. Sure, Windows 7 has some pretty good
touch-centric UI features, but it wasn't built from the ground up to be a
touch-only, or touch-first system. Windows 7 was designed, first and
foremost, to be operated with a keyboard and mouse. iOS and Android were
both built from the ground up to be touch-first (not neccessarily
touch-exclusive, however) devices, which automatically puts them
head-and-shoulders above any Windows 7 skin. This new ASUS hardware looks
absolutely wonderful, but I can't help but imagine how amazing it would
be with a software set that was built primarily for this type of device.
To summarize all of this: I can't help but feel the inclusion of Windows
7 on this tablet is just as good of a fit as putting square wheels on a
car.

**UPDATE:** It appears that ASUS has announced that they are looking to
put [Android on the eeePad](http://gizmodo.com/5591414/asus-wants-android-for-eeepad). A very wise move, indeed.
