---
title: "Server-Bits #7: Transmission and Web-Controlled Torrents"
layout: "post"
date: "2010-05-01 20:58:00"
categories: [Linux, Tutorials]
---

[{% img center ../../../../../images/2010-05-01-server-bits-7-transmission-and-web-1.png %}](../../../../../images/2010-05-01-server-bits-7-transmission-and-web-1.png)

Have you ever been away from your machine when you've thought to yourself,
"Oh! I need to torrent *COMPLETELY LEGAL CONTENT HERE*!! I wish I was at my
machine...". Now you don't have to be at your computer, you can control all of
your torrents, and add new ones, entirely through Transmission's web client.  

Transmission should be installed on Ubuntu by default, but just in case it
isn't, you can install it by running the command:  

> sudo apt-get install transmission

[{% img center ../../../../../images/2010-05-01-server-bits-7-transmission-and-web-2.png %}](../../../../../images/2010-05-01-server-bits-7-transmission-and-web-2.png)

Go ahead and start this in GUI-mode. Just open Transmission while you are
logged into Gnome. From here, go to: **Edit -&gt; Preferences**. Navigate to
the **Privacy** tab. There are a few options that **NEED **to be changed.  

[{% img center ../../../../../images/2010-05-01-server-bits-7-transmission-and-web-3.png %}](../../../../../images/2010-05-01-server-bits-7-transmission-and-web-3.png)

First off: **Blocklist. Blocklist. Blocklist.** Enabling the blocklist will
keep the vast majority of anti-P2P groups off of your back. Make sure that you
enable automatic updates as well. The next thing you need to change is the
encryption. Change this to **Encryption Required**. This will ignore and close
any connection that is not masked by encryption.  

[{% img center ../../../../../images/2010-05-01-server-bits-7-transmission-and-web-4.png %}](../../../../../images/2010-05-01-server-bits-7-transmission-and-web-4.png)

Next, in the **Network** tab, check the "Pick a random port every time
Transmission is started" box.  

[{% img center ../../../../../images/2010-05-01-server-bits-7-transmission-and-web-5.png %}](../../../../../images/2010-05-01-server-bits-7-transmission-and-web-5.png)

In the **Web** tab, check the **"Enable web client"** box, change the
listening port to whatever you prefer (and forward the port on your router if
you so choose [forwarding this port will give you the ability to manage your
torrents outside of your network]). Make sure **"Use authentication"** box is
checked and choose a **username/password**. If you would like, you can also
restrict access to certain IP Addresses. This is helpful if you are only going
to be accessing this page from a known machine with a constant known address.  

Now, in a web browser of your choice, navigate to
**'YourHostname:YourTransmissionPort'** and you should be greeted by a popup
login box. Enter the username/password combination that you set up and start
remote torrenting!!  

[{% img center ../../../../../images/2010-05-01-server-bits-7-transmission-and-web-6.png %}](../../../../../images/2010-05-01-server-bits-7-transmission-and-web-6.png)

_About Server-Bits:_  

If you've ever wanted to get started building a server, right in your own
backyard, kitchen, closet, mother's closet, mother's basement, then this is
the read for you. Aimed at the not-so-technical-but-willing-to-learn, this
will give you everything you need to build... that monster server you've
dreamed of. **My goal: To give you a working, rocking server, for free, that
you can use daily.**
