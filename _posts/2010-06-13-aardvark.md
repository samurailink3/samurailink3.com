---
layout: post
title: "Aardvark"
date: 2010-06-13 12:12:03 EDT
comments: true
categories: [News]
---

{% img /images/2010-06-13-aardvark-1.png %}

I've stumbled upon a cool, new web service called Aardvark. Essentially,
its ChaCha, but with a much higher limit on the amount of questions you
can ask (7 at the moment), and on top of that, _the limit resets daily_.
I've been using the service for a while now and have not been
disappointed. Lets take a look at how the service works:


You submit a question to Aardvark via web, IM, or iPhone app ([SMS and
Android app in development!](http://blog.vark.com/?p=27)). Aardvark then sends the question to other
users who want to answer questions on that topic. For instance, my topics
include: Linux, Technology, Android, The Internet, and Operating Systems,
among many others, so I receive questions on those topics (If you want,
you can also take on any open question that has not received an answer
yet). Aardvark then passes your question around to various individuals
and has them give their best answers. Questions usually receive 2-3
answers, and times range around 5 minutes to a few hours for all answers
to roll in. The time taken to get an answer back is relatively short, and
the answers themselves tend to be more personalized and helpful than
ChaCha's, which I have found to be unreliable at times. Question-askers
are then asked to say "Thanks!" click one of three choices to rate the
answer they received. "Was So-And-So's Answer Helpful?", you can choose:
Yes, No, or Kind of but not for me. This helps Aardvark weed out the
less-than-helpful users and give more questions to the higher-rated ones.


Aardvark is a pretty cool service, and with enough people and time, it
could be as well known as Yahoo Answers or ChaCha, there are only a few
things holding it back:


* The lack of an SMS option:
* The lack of an Android App
* Any major (revealing) news from the site regarding them [being picked up by Google](http://techcrunch.com/2010/02/11/google-acquires-aardvark-for-50-million/).

But the service has more going for it than against it, including: Social
integration with Twitter and Facebook, a very well done (and innovative)
community voting page, and a service that not only works, but provides
great service to all of its users. Whenever I'm logged into my IM client,
I absolutely love seeing Aardvark pop up with a tech question for me to
answer. If I don't feel like it at the moment, I can tell it, "Busy" or
"Pass" and it gets the idea. Aardvark doesn't bug me at all, its all very
wonderfully tuned to be just the perfect amount of user interaction. Do
yourself a favor and try it out today, I'm sure you'll quickly become a
fan.

[Aardvark](http://vark.com/)
