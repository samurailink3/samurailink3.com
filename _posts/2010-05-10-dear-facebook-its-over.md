---
title: "Dear Facebook, Its over."
layout: "post"
date: "2010-05-10 18:26:00"
categories: [Rants]
---

I don't think I can do this anymore, and this time, its not me, its you. When
our relationship started, it was all comments, groups, and wall posts. It was
wonderful, we were madly in love. You didn't need (or support) sparkling gifs,
you had no auto-playing music, users couldn't even change the color scheme on
the page. You knew what our relationship needed better than I did. And you
know what? It worked.  
Sadly... you've changed, and not for the best. Over the past few years, I've
overlooked your very closed and very buggy chat, your outright theft of "The
Like Button" from [FriendFeed](http://www.friendfeed.com/), your [ever-opening
'privacy' settings](http://www.eff.org/deeplinks/2010/04/facebook-timeline/),
your support of apps that [steal all
of](http://www.readwriteweb.com/archives/how_to_delete_facebook_applications_and_why_you_should.php)
your [personal
information](http://www.readwriteweb.com/archives/what_facebook_quizzes_know_about_you.php),
and your
[complete](http://www.readwriteweb.com/archives/facebook_pushes_people_to_go_public.php)
[disregard](http://www.readwriteweb.com/archives/facebooks_zuckerberg_says_the_age_of_privacy_is_ov.php)
for [my private information](http://mattmckeon.com/facebook-privacy/). I
consider myself a [public figure](http://www.google.com/profiles/samurailink3)
on the internet, and you are still creeping me out. I thought we had something
special.  
Your recent push to share me with your new friends
([Pandora](http://www.pandora.com/), [Yelp](http://www.yelp.com/),
[Docs](http://docs.com/)) has me confused, [and it is just about impossible to
get away from them](http://librarianbyday.net/2010/04/protect-your-privacy-
opt-out-of-facebooks-new-instant-personalization-yes-you-have-to-opt-out/). At
first you wanted me all to yourself, it was a fight to get you to play nice
with any other websites, but now... Especially with [giving everyone access to
your 'Like' button](http://techcrunch.com/2010/04/21/facebook-like-button/),
you seem to be interested in more than just me. These things have all led up
to my thoughts right now, but they haven't been enough to push me over the
edge. [Even my friends tell me that I should leave
you,](http://gizmodo.com/5530178/top-ten-reasons-you-should-quit-facebook) but
I felt that we still had something to hold onto... Until this morning.  

Last night, I was incredibly frustrated with the way you've been treating me,
and I needed to vent to my friends. I posted a link to [Jeff Jarvis' article
on your recent behavior](http://www.businessinsider.com/heres-the-privacy-
line-that-facebook-just-crossed-2010-5), and my friend even commented on it,
asking if there were any others I could run to. You must have seen this and
not appreciated the candid honesty _because you deleted it_. Right off of my
wall. Removing the comments, removing the post. I thought it was my Nexus
One's Facebook App being buggy (Because that happens sometimes) and not
displaying the post. But when I checked with you the next morning, it was
true. **_You had removed the link that I had posted on my own wall._** I feel
that this could be the final straw. We really had a good time while it lasted,
but I'm not entirely sure I want to continue this relationship anymore. I
think [I've](http://www.flickr.com/) [found](http://www.google.com/profiles)
[someone](http://www.google.com/buzz) [else](http://www.twitter.com/). Someone
that will respect my privacy and leave my posts intact. I'm going away for a
little while to cool down and think about things, but I don't think this will
work anymore.  

[**The Tipping Point:**](http://www.amazon.com/Tipping-Point-Little-Things-
Difference/dp/0316346624)  


[{% img center ../../../../../images/2010-05-10-dear-facebook-its-over-1.png %}](../../../../../images/2010-05-10-dear-facebook-its-over-1.png)

[{% img center ../../../../../images/2010-05-10-dear-facebook-its-over-2.png %}](../../../../../images/2010-05-10-dear-facebook-its-over-2.png)

Last night, I posted a link to Jeff Jarvis' article entitled: [Here's The
Privacy Line That Facebook Just
Crossed...](http://www.businessinsider.com/heres-the-
privacy-line-that-facebook-just-crossed-2010-5) and got a comment on it pretty
soon after it was up. It was listed in my Facebook notifications, even! I
figured I would respond in the morning and went to bed.  

[{% img center ../../../../../images/2010-05-10-dear-facebook-its-over-3.png %}](../../../../../images/2010-05-10-dear-facebook-its-over-3.png)

To my surprise this morning, I find that the links didn't work. The link in
the 'Notifications' page and in the email I received (I have email
notifications for just about everything enabled) in the Facebook app led to
this error message **[Content Not Found: The page you requested cannot be
displayed right now. It may be temporarily unavailable, the link you clicked
on may be broken or expired, or you may not have permission to view this
page.]**  
Strange... Very strange. I went to my computer to check, hoping that my phone
was being buggy...  

[{% img center ../../../../../images/2010-05-10-dear-facebook-its-over-4.png %}](../../../../../images/2010-05-10-dear-facebook-its-over-4.png)

Yep... there is the email... *clicks link*  

[{% img center ../../../../../images/2010-05-10-dear-facebook-its-over-5.png %}](../../../../../images/2010-05-10-dear-facebook-its-over-5.png)

**[This post is not available anymore]**  
**Facebook deleted my post.** They removed the link I posted (And naturally, all of the links associated with the post) because it put them in a bad light. I did also link to the same article using a [bit.ly](http://bit.ly/) link (Fetched from Twitter), and that was not removed. Its apparent that I'm not an important enough target for profile manipulation, and I seriously doubt that Zuckerberg is sitting in his office, picking off links I post about his baby. The most likely scenario is that Jeff's article was flagged on Facebook for mass deletion sometime yesterday. Since this incident, I have re-posted the link to the article and asked others to do the same (many have now shared the link as well). So far, the link survives. This is the final straw. I am seriously considering deleting my Facebook account. With the recent anti-privacy stunts, bugs, and now censoring of anti-Facebook content, this is far from the "Safe Haven" picture that Zuckerberg paints atop his high tower. Starting today, I will be moving over my data/comments/time to other services ([Twitter](http://www.twitter.com/), [Buzz](http://www.google.com/buzz), [Flickr](http://www.flickr.com/), [Delicious](http://delicious.com/), [Google Profile](http://www.google.com/profiles)) and weaning myself off of Facebook in the likely event that I will be deleting my account.  

If you would like, go ahead and join me on any one of these services, my
username is **samurailink3** on everything. Facebook was a huge convenience,
but this was the final straw, I'm moving on.  
**Looking to delete your account?** [**_Click here._**](https://ssl.facebook.com/help/contact.php?show_form=delete_account&__a=32)
