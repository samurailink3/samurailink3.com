---
layout: post
title: "New Talk: Passwords"
date: 2014-05-08 16:53 EDT
comments: true
categories: [Security, Talks]
---

About to give a talk on modern password requirements and security theory.
It's a bit controversial in some aspects, if you want to check it out,
[head over this way](/talks/modern-times).
