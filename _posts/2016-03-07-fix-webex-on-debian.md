---
layout: post
title: "Fix WebEx Screen Share in Debian"
date: 2016-03-07 06:07:31 EST
comments: true
categories: [Tutorials]
emoji: false
bg: false
---

In Debian, I was able to get the WebEx java applet to launch in Iceweasel with
the help of [OpenJDK](http://openjdk.java.net/), but I couldn't get
screensharing to work properly. It just never came up when someone was sharing
their screen and it wouldn't let me share my own.

Go ahead and install some 32-bit libraries, this should help:  
_Note: You must have multiarch enabled on your machine for this to work: `sudo
dpkg --add-architecture i386; sudo apt-get update`_

`sudo apt-get install libgtk2.0-0:i386 libglib2.0-0:i386 libglib2.0-0:i386 openjdk-7-jre-headless:i386 libpango-1.0-0:i386 libpangoft2-1.0-0:i386 libpangox-1.0-0:i386 libxft2:i386 libxmu6:i386 libxt6:i386 libxv1:i386 openjdk-7-jre`

Then launch your meeting again. Hopefully screensharing should work. I have
heard reports of this not helping with meeting audio issues, but I don't use
WebEx audio, I can't comment on that.

If that doesn't work, figure out what else you're missing with the help of [this
AskUbuntu
post](https://askubuntu.com/questions/368270/how-to-i-make-cisco-webex-work-with-13-10-64bit/).

In case that post ever goes away, I've mirrored the steps here:

From [this
post](http://blogs.kde.org/2013/02/05/ot-how-get-webex-working-suse-linux-122-64bit#comment-9534),
here is a step-by-step method that might work:

1. Install JDK.
1. Configure Java plugin for browser (no need for a 32-bit JDK or Firefox).
1. Start a WebEx to create `.so` files inside `$HOME/.webex/????/`.
1. Check for unresolved `.so` dependencies: `ldd $HOME/.webex/????/*.so > $HOME/check.txt`
1. Search for missing libraries: `grep "not found" $HOME/check.txt | sort | uniq`
1. Review the libraries; for example:
```
libasound.so.2 => not found
libjawt.so => not found
libXmu.so.6 => not found
libXtst.so.6 => not found
libXv.so.1 => not found
```
1. Find the corresponding packages:
```
sudo apt-get install apt-file
sudo apt-file update
```
1. Locate that package that contains the missing libraries:
```
apt-file search libXmu.so.6
apt-file search libjawt.so
```
1. Install the missing libraries, for example:
```
sudo apt-get install -y libxmu6:i386
sudo apt-get install -y libasound2:i386
sudo apt-get install -y libxv1:i386
sudo apt-get install -y libxtst6:i386
sudo apt-get install -y libgcj12-awt:i386
```

## Additional Sources

[http://ubuntuforums.org/showthread.php?t=2184620](http://ubuntuforums.org/showthread.php?t=2184620)
