---
layout: post
title: "Command Line Twitch TV Browser"
date: 2016-02-09 10:59:40 EST
comments: true
categories: [Code]
emoji: true
bg: "2016-02-09-command-line-twitch-tv-browser-bg.jpg"
---

_Updated: 2016-03-02 - Audio-only functions, easier to change preferred game/language/quality, added MIT license._

I've been watching a fair amount of [Dota 2](http://blog.dota2.com/?l=english)
on [Twitch.tv](http://www.twitch.tv/), but I wanted to watch in VLC, rather than
taking up a browser tab. I did a small amount of research and discovered
[Livestreamer](http://docs.livestreamer.io/). It's a really simple command line
utility that launches various live streaming services in VLC, with a super
simple syntax: `livestreamer twitch.tv/day9tv best`.

But then I ran into an annoyance...

I don't watch just one Dota 2 channel. I watch a wide variety of channels
depending on who's streaming. I found myself still opening a browser tab,
heading to the [Dota 2 game page](http://www.twitch.tv/directory/game/Dota%202)
and deciding what channel to watch. Pretty annoying for what should be a
command-line-twitch-tv-vlc-launcher. So I did what any other programmer would
do: I wrote a tiny crappy little thing to fix the problem.

[{% img center ../../../../../images/2016-02-09-command-line-twitch-tv-browser-1.png %}](../../../../../images/2016-02-09-command-line-twitch-tv-browser-1-full.png)

I can run this script (which I've placed at `~/bin/watchtwitch`) with
`watchtwitch` and instantly get a nice menu of who's streaming right now:

```
1) moonducktv (20281)
2) attackerdota (1931)
3) beyondthesummit (657)
4) y0nd (298)
5) dannygamingnc (204)
6) rexitus (176)
7) dotacapitalist (172)
8) theexel80 (33)
9) xxgodzillaxx (28)
10) dotkaember (19)
Choose your channel:
```

From there, I hit a number, press <kbd>Enter</kbd>, and I'm watching Dota.
Pretty rad. You will need `livestreamer`, on Debian, you can accomplish this with a simple `sudo apt-get install livestreamer`. You'll need ruby in some form to use this script. You'll also need the Twitch gem, so run `gem install twitch`.

I've updated this script to use some variables at the top, so it's easier to
modify for your preferred games. You can also pass `--audio-only` while running
the script to get an audio-only stream. It's great if you can't watch, but want
to listen in to the action.

To get an audio-only stream, use it like this: `watchtwitch --audio-only`.

Grab the code below, it's really simple if you want to change your preferred language or game:

```ruby
#!/usr/bin/env ruby

# Requires 'livestreamer': https://github.com/chrippa/livestreamer
# Requires Twitch Gem: https://github.com/dustinlakin/twitch-rb

# Set default options here. You can change these to find different game streams,
# different languages, or set a different stream quality.
GAME = "Dota 2"
LANGUAGE = "en"
QUALITY = "best"

# If "--audio-only" is passed to watchtwitch, we'll set AUDIO_ONLY to true. This
# will force an audio-only stream. This is useful if you are in a situation
# where you can't watch a stream, but would like to listen in and keep up.
if ARGV[0] == "--audio-only"
  AUDIO_ONLY = true
else
  AUDIO_ONLY = false
end

# Import the Twitch gem
require 'twitch'

# Create a new Twitch object
t = Twitch.new

# Initialize our counter and channels array
i = 0
channels = []

# Use the Twitch API to grab all Dota 2 English streams.
# Cut down the list to the top 10 streams, ordered by number of viewers
# Then, for each of those streams...
t.streams(:game => GAME, :language => LANGUAGE)[:body]["streams"][0..9].each do |stream|
  # Increment the counter
  # This will be useful when asking the user which channel to pick
  i += 1
  # Make an empty hash
  # This will hold all of the channel information
  channel = {}
  # Set the channel ID to our counter
  channel[:id] = i
  # Set the channel name
  # This gets used by `livestreamer` later
  channel[:name] = stream["channel"]["name"]
  # Set the number of viewers
  # This is only used to show the user how popular a particular stream is
  channel[:viewers] = stream["viewers"]
  # Push our channel hash into the channels array
  channels.push(channel)
end

# For each channel (all 10 of them)...
channels.each do |channel|
  # Print out the channel ID (from our counter), the name, and the number of viewers
  puts channel[:id].to_s + ") " + channel[:name] + " (" + channel[:viewers].to_s + ")"
end
# Ask the user which channel they'd like to watch
puts "Choose your channel:"
STDOUT.flush
# Grab the user's input
choice = $stdin.gets.chomp
# Start livestreaming whatever choice the user makes We'll execute a native
# binary "livestreamer" (hopefully it's in your path) with the url
# "twitch.tv/channelname", livestreamer has a twitch.tv plugin, so it can figure
# out what to do with that URL. We then set the stream quality, I've chosen
# "best" here, because it's pretty, but you can choose something more sane if
# you have bandwidth constraints or are on a metered connection, check out the
# variables up top. By default, livestreamer launches VLC to play the stream,
# but you can check out it's documentation if you have a better idea. If you
# pass "--audio-only" to watchtwitch, it will use an audio-only stream.
if AUDIO_ONLY
  exec "livestreamer twitch.tv/" + channels[choice.to_i - 1][:name] + " audio"
else
  exec "livestreamer twitch.tv/" + channels[choice.to_i - 1][:name] + " #{QUALITY}"
end
```

This code is [MIT Licensed](https://opensource.org/licenses/MIT). Use it however
you want.
