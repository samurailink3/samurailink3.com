---
layout: post
title: "Distraction Killers"
date: 2015-09-01 18:58:14 EDT
comments: true
categories: [Link]
---

When I need to concentrate and get some hardcore, head-down work done, I employ the use of what I call "Distraction Killers". A collection of sound generators to help get me [in the zone](http://www.joelonsoftware.com/articles/fog0000000068.html) mentally.

## Apps:

[Ambiance](http://ambianceapp.com/): Available on Windows, OSX, iOS, and
Android. This app gives you access to a complete library of free
downloadable sounds that you can listen to individually or mix together
to make your own distraction killer. Be careful with this one, I've
gotten distracted by the sheer amount of sounds and mixes before. Really
awesome app, I wish the Android version would get a visual overhaul (it
_really_ needs it), but the functionality is still there.

## Noise Generators

[MyNoise.net](http://mynoise.net/noiseMachines.php): A huge collection of
free noise generators (and some paid ones if you would like to
contribute). From _[Jungle
Life](http://mynoise.net/NoiseMachines/jungleNoiseGenerator.php)_ to
_[Number
Stations](http://mynoise.net/NoiseMachines/numberStationsRadioNoiseGenerator.php)_
to _[Sounds from the USS
Enterprise](http://mynoise.net/NoiseMachines/spaceshipNoiseGenerator.php)_,
MyNoise.net has just about everything you would want in noise generators
(and they add more all the time). These sounds are very well produced and
the online player comes with an "animate" feature, that subtly changes
the sounds around you, so they don't become too repetitive. I highly
recommend this site if you need to sit down and get some work done.

[RainyCafe.com](http://rainycafe.com/): A very simple site with two
buttons: One turns on Rain sounds, the other turns on Cafe sounds. I go
here quite a bit because it has the best cafe ambiance I've found (the
rain isn't half-bad either).

[SOUNDROWN](http://soundrown.com/Coffee/): A simple site with a great
twist: You get five sounds to turn on or off and mix the volumes as you
see fit. Choose from Coffee Shop, Rain, Waves, Fire, and Birds, mix them
together to make a pretty unique ambiance. I don't use this as much as
the others on this list.

[Rainy Mood](http://www.rainymood.com/): This list simply wouldn't be
complete without Rainy Mood. The very best rain/thunderstorm sounds
_anywhere_. I use this every night when I sleep and at work most days to
drown out the activity around me. It's well-looped, expertly produced,
and is a beautiful (yet simple) website as well. They also put out apps
for all the major platforms that I rely on all the time. As an added
bonus, their fans have suggested music to go along with the great rain
ambiance, and they're listed right at the bottom of the page. I've listed
some of my favorites below.

## Mix with Rainy Mood

[Fireplace](http://endlessvideo.com/watch?v=DIx3aMRDUL4): Very simple,
just a fireplace with sound.

[Piano cover of Where Is My Mind by the
Pixies](http://endlessvideo.com/watch?v=4NZdggNUvq0): Slow enough to not
be distracting, but not so suble that it blends into the background.

[The Fragrance of Dark Coffee
](http://endlessvideo.com/watch?v=HMnrl0tmd3k): Quite possibly the
classiest thing I will ever put on this blog. Slow, jazzy, saxaphone.
Turn on Rainy Mood, set this song to loop, and pour yourself the most
expensive scotch you have on hand. This is by far, my favorite
combination.
