---
layout: default
title: "Technical Information"
---

This site is built using a few different components, and I've had a few
people ask about it, so I made this page.

# This site is open source, grab all of the code here:

[{% img center ../../../../../images/gitlab.png %}](https://gitlab.com/samurailink3/samurailink3.com/)

I also accept pull requests if you find something wrong with the site,
such as typos, mis-information, broken links, etc.

## General Overview

First off, this site is generated using [_Jekyll_](http://jekyllrb.com/),
and hosted on my personal web server (via [Digital
Ocean](https://www.digitalocean.com/)). This site does not use a database
or true CMS, only flat, generated HTML/JS/CSS files, making it very
portable, and very easy to host anywhere.

I chose [_Jekyll_](http://jekyllrb.com/) because I wanted my website to
be portable. With technology changing more and more each day, I needed to
know that my previous writings would remain preserved and under my
control, in a format that I could continue to work with as the years went
on. [_Markdown_](http://daringfireball.net/projects/markdown/) is easily
converted to straight-up HTML, and it is very readable in its plaintext,
unconverted form, making it an easy choice for my past, current, and
future writings. Honestly, reading blog-posts in plain
[_Markdown_](http://daringfireball.net/projects/markdown/) is pretty
readable, much more so than tag-laden HTML.

Using [_Markdown_](http://daringfireball.net/projects/markdown/) is a
thing of beauty. No site to manage from, no continuous internet
connection needed, no rich-text formatting editor required. I can write a
blog post in an email to myself, in a [Google
Keep](https://drive.google.com/keep/) note, in a plain-text file in
Dropbox that I made in Notepad, or in nano while I'm SSH'd into my home
computer. Plain text is the most basic of all file formats, and it is
truly portable, so why not keep things simple?

[_Jekyll_](http://jekyllrb.com/) also brings the promise of portability.
No databases to export, no strange file formats to mess with, no
bullshit. If I wanted to host my blog in my [Dropbox Public
Folder](https://www.dropbox.com/help/16/en) or [Google Drive with Public
Sharing](https://support.google.com/drive/bin/answer.py?hl=en&answer=2494822),
I could. The site boils down to raw HTML, generated using open-source and
widely available programs and toolchains.

And because the site is only flat HTML, CSS, and JavaScript, I dodge 75%
of the hacking attempts, just by getting rid of PHP and the backend
database. Don't get me wrong, I love [WordPress](http://wordpress.org/)
to pieces, but it gets really annoying having to update the damn thing
every 4 days, and its the kind of risk I just don't need with my personal
site. [WordPress](http://wordpress.org/) really is more powerful than I
need.

This style of site isn't for everyone. I live in Git and ZSH. I see the
terminal on a daily basis, and I love working in the server backend. If
you're a tech guy or someone interested in the points I made above, go
for it. If you're a person who just wants a fire-and-forget blog, head
over to [Blogger](http://www.blogger.com/start?hl=en), if you want a
little more power, but aren't ready to commit to anything terminal-based,
try out [WordPress](http://wordpress.org/). But if you're a tech guy who
wants control, power, portability, and ease-of-deployment,
[_Jekyll_](http://jekyllrb.com/) will be your best friend.

## Branch Conventions

* `master` - This branch is the working Jekyll copy. This branch contains the [_Markdown_](http://daringfireball.net/projects/markdown/) files and other files used to generate the site. If you want a good look at the site internals, download this and pull it apart.
* `deployable` - This branch is just in case you want to deploy a site that looks and functions like this one, but you don't want to clean out all of my blog posts and personal data. I've done the work for you, have at it.

## Technology Used (Creating, Generating, Hosting)

* [_Markdown_](http://daringfireball.net/projects/markdown/) - This is an easy-to-read, easy-to-write plain text format.
* [_Jekyll_](http://jekyllrb.com/) - Jekyll transforms your plain text into a fantastic static site. No databases, no active content, fast and easy.
* [_Atom Editor_](https://atom.io/) - I do most of my coding on Linux systems and I need an editor that's easily extendable. I was on Sublime Text 3, but I really preferred something open source and worked in a similar way. Atom fits my needs.
* [_Digital Ocean_](https://www.digitalocean.com/) - Stupid easy VPS provider. Really fast to get up and running and the price is excellent starting at $5 a month.
* [_Vim_](https://www.vim.org/) - When I'm in a terminal (which is most of the time), I use Vim to edit.
* [_Let's Encrypt_](https://letsencrypt.org/) - HTTPS should be everywhere, Let's Encrypt makes this easy **and free**!

## Credits

If you would like to see a "Credits Roll" for the site, hit up the
[humans.txt file](/humans.txt). It has some links and the names of people
who have made various plugins.
