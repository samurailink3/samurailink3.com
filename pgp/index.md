---
layout: default
title: PGP/GPG Key
---

**[Key Archive (For Old Keys)](/pgp/archive/)**

This page contains my personal and professional PGP public keys. You can always
check these against the keys found at [pgp.mit.edu](http://pgp.mit.edu) or
[keybase.io/samurailink3](https://keybase.io/samurailink3). When a key expires,
it will be moved to [the archive page](/pgp/archive/). You may verify these keys
by using the free software GPG. You may download GPG
[here](http://www.gnupg.org/). For Windows clients, I recommend
[GPG4Win](http://www.gpg4win.org/).

Feel free to verify any signed content I post, including code. Sensitive code
will be signed with this key. If you'd like to send me any sensitive
information/documents, feel free to shoot me your public key and an encrypted
email at [tom@samurailink3.com](mailto:tom@samurailink3.com).


<a href="#Key_Domain"><h2 id="Key_Personal">Domain Email Key</h2></a>

You can also find this key on [my Keybase
page](https://keybase.io/samurailink3).

    pub   rsa4096 2019-06-06 [SC]
          4A7458AD56B5819453AC56F98BF4D6D952A0B4DC
    uid           Tom Webster <tom@samurailink3.com>
    sig        8BF4D6D952A0B4DC 2019-06-06   [selfsig]
    sub   rsa4096 2019-06-06 [E]
    sig        8BF4D6D952A0B4DC 2019-06-06   [keybind]


    -----BEGIN PGP PUBLIC KEY BLOCK-----

    mQINBFz5HN4BEADG2IrHUrSscFhAG2EuSuikktLLz/afctAHLxEJiHjQ1eqTLAjM
    meIC+Qzh0nmrcOEpBfSPHyCT8brLlzSCC/UALkpVqVpdL8e7Oo5ZBGiAyTnM5rW6
    LyKVWprhvup+uc817n9m0P8hBdRwvvFlotx0Y8lJBmyyv87ITHozz/9Ze7mgq4+r
    LAY3CINdCPUWhz1521QpAm2xNeHyB7LSo9FUK5EWPSs85g6BMgjEi2zKB6/6WOa8
    FqETYjytuRypsUiyT+8XupZK0QKyrlyaQM6laVta53XZvjKE7vvQWt/fZ8YKqdTV
    pb6izGxEiwuP7ALx1lk1pWZYQc8YsMKTqoHOKoRblUVCU5ayiLQuJs1loifM0M0F
    HBQJ+dBFm6Jvjba9x8yDLtQnkPsZVRK3vz8DZsM+LRvs40wn2MbyrEpKBHQNowtA
    bBN3CsuhvRyLL75zLYe6zy961tIf+BdKlrjCZqpePdMrCIkOcoVdNZHtc3kKFSvG
    MC9Sbme/JypxymAS9kKoL0W2WWNrxcFkIVrfvc3iKdXkuxFtQXPIatbFoLhCtmZF
    lXC2DfIfzfoebwBYekJ+3+Y7k2LZg6QgYyP8wOdWZ3BYEH4trat2lAjU7AtgWdk/
    phpCvU11HT22IEHLwWmrM0ddqZFAPD56BuduqaItkQ5WoW6xWdKwrmTbzwARAQAB
    tCJUb20gV2Vic3RlciA8dG9tQHNhbXVyYWlsaW5rMy5jb20+iQJOBBMBCgA4FiEE
    SnRYrVa1gZRTrFb5i/TW2VKgtNwFAlz5HN4CGwMFCwkIBwIGFQoJCAsCBBYCAwEC
    HgECF4AACgkQi/TW2VKgtNyRqA/8CZQFeccveT7IGnV9nkIquS+IGtkDes9hokFY
    pmVsHhnpUdTNQzfb0mxgp6qcPuT8tYGrwuAioxtnet1U0IRD1CU7fEQmCeiwpdqi
    S7wc4l66MFCgh1rJKboybXxJgBHz4q2FMRqr5OborvfX8dPSKQb6joi86YzlJQos
    jL9Zx+vwX0FFRHtgko+EYfsXxHMgA29FPg3DLs7UDqMA1SKeOjb6sbbMPa45/zyW
    MA1m9RTU5sLbUnLImh61jtaB4mX0h3YyteBC0oBiyiaGBiNWw94s1QaWuehFCcpF
    yG7VUpiye7NXCp2w0jmJxc99w5HTuAFMWe9bHNmQEjeS8xIBl9SD9rBaQrGxiYBR
    5xRJS0hWKeD+z87IIw0LMKJ6JTB8uQzWdY30q3uK7R+pn6aVEyKIK4h4fcHCW6gB
    4xH/Blravpa/yBp1ERzkeL3zgMH58ZC9CBKjlwVMyGb3fXMqtBVFUqU6heo4FkLp
    Dw/4YhJf4fse463fBlanN9ajQtOWCbLzD7ceO4pSch0roTQHFa+V6otdZXiaV+5Z
    G7OtTQHuit6h0VrGHIUiaWPaA/6YsARDMvs2PAr+WtzluYO/9pu9/lEI+uBYTktx
    RTmyyz8mVgebwbBJvogdt89bqu/rzmfQYdhJFgiS9vfsliL+SkBBY5ymE0zJtCw5
    jb4y9jq5Ag0EXPkc3gEQAL1HT2QYj0ckjH6kOjEqXwOiuL3wtheHmv4evdmKKODo
    Szev6Y9w7M1n+Bm04oYMHoC2Cvtx98zqRxdHlXJoZEKQGGaDpxhiYlawlKGv5aaL
    5fp7cGyFKuMGyz6x/uESkor9i9d7amiANyo3S+r2dLyp54rSw70rZPudotV/RPr0
    Cemjs2DkaKbcgyOBoNCAgFtqdhi3REnLdpwi5w6NSPQMTa31PYJbpuU8otlS+LvH
    koDLJqdgl/c3nMv1rWV0nzKeFeAV5XtVjGErwd/RrEX3A3/bbPR2DOSVWG0CvAoG
    Ai82XOc6kHt58hsNFoxSfq7sXWRY07rAd8BchuifbfUT78DE6aLlD2N8md0+NDHw
    PTlPxJyqGwwQ/TcFP7+Sk1k6rI97gzOedX3stdFlcrudUQ14a19Tw75t1X6VOU0v
    iWgV02jVH38tbM61Azp+0lADLOtqd5IeZ+qnnGvPnjKyvSZ/BOm71ggewkpqd5nr
    fl2r2aKkMrUPgs9RkGHXu9nBMANyO3n6FdKeo2fqmBDeAeZxouR+Uwu+SZUGZNq9
    dsJpa/u2+dx/gbCFT5/AKlfvjQK1VjomBiIyCfIU0UQhzmKnKvhA1zXEk8FsJFwY
    rhmTBbe6fPrzIv7+lGfsl+ZuL136XYs7M1h+uO9Qgwjs65j4RiEVVJUHe4JfKnVR
    ABEBAAGJAjYEGAEKACAWIQRKdFitVrWBlFOsVvmL9NbZUqC03AUCXPkc3gIbDAAK
    CRCL9NbZUqC03A7AEACfA85HETyvw1LxbAcd4VGT/gclRJ6VjtyY8th6q/NsdExV
    VXRKb6N8NKinKx9LL2I+Iy854z1SOfm83xRoZcNxIu0L7PnUGLqtGtuiEJUsdiqr
    DDW5CQp/frK4q5h3DcC4jhvw2FhsQxE+m40/DsWzpd7fsamu01RT/m6Qdv55vpvR
    0KvsDLiG+BNmH4CMQDgLrwoUH/jbBMeZz1PfE2kDV6qs96l6/P7PCUjmHNCbiiTy
    zULJPmPLy9C6V56JY86sPFpPPs+keB9Jmomr4AjOUSWydvVEMBL5XtB5XT7eygze
    Q9LxQWNujoASHIj09WhJ1y+AGLJ5wsSofNw1k9qA1gId1raFlSWGUV0sJSkdyT1R
    DaK1IPx8mtAi03t/ngQLlSHDmD8ybN7ngvyRGY/6cL93d2TU4Sil+iE/W8ZHGGFS
    TUF3/h80xC/a34BBtl4GsaWeHG5dJRaKU62lL3QSu8By7FtJszYnfjf7CQzodnwt
    J0u9YQhcBr05b8m3MtmzoXk9i0pzG6xaMo0zgyNna4QjXH6YiWxoF366rCYFTo2x
    qDD5D66Jd/ta4JH4fM3EiZhOMP2vF6vPn5CIfiTVJCFQ5bNI13rgFewnlt8bSsC6
    jVgKQ2rezmMwp83HQEIPCEwUO0Bw66HbLDbbWzgnY3VoOAF4Wn4OFg9602tFcQ==
    =TbMX
    -----END PGP PUBLIC KEY BLOCK-----


<a href="#Key_Personal"><h2 id="Key_Personal">Personal Email Key</h2></a>

**This key and email address is deprecated. Please use the _Domain Email Key_
above. It will be archived after it expires.**

You can also get this key at [MIT's PGP
Server](http://pgp.mit.edu/pks/lookup?op=get&search=0xCB744273EDA0E0BB), [The
SKS PGP
Server](https://sks-keyservers.net/pks/lookup?op=get&search=0xCB744273EDA0E0BB),
or on [Keybase](https://keybase.io/samurailink3).

This key has been signed by my previous personal email key (fingerprint: 11C9
30C4 693A 6C9B 789B  B0F7 6442 DF0A 14BA 4EFD). You can find that key on [the
archive page](/pgp/archive/)

    pub   rsa4096 2017-02-16 [SC] [expires: 2020-02-16]
          37E8 CF02 6EAC C295 F9EC  9CB9 CB74 4273 EDA0 E0BB
    uid           [ultimate] Tom Webster <samurailink3@gmail.com>
    sub   rsa4096 2017-02-16 [E] [expires: 2020-02-16]

    -----BEGIN PGP PUBLIC KEY BLOCK-----

    mQINBFiloGIBEADkEdOEMD0ADiX4F5lz5Pvr/tC/svvGOvjCtQohvGVPcAN6rx3h
    TKEJi1CSpSCaRzAUSfSzcu4XJJu1phZMka44QBZTktIQ9VF3j9nBYQGgVi9JsH6G
    I/Xhn2Hc+wf4cpbqbs9Gfi0GRYVO8pq2puc4/GpeJSd3EKCE0MchEh+0Ki7fuysR
    XUYj88Irv5okVCJi9GPnW0K8PnLxAaQrLB+fwLMDZiOdlQzNIBXVkL8xECZ38bRa
    vg84LY20D6KHyc7HiNeuTgnUIx3NH/rXrmYiYeriMD+1DcmTYPEFJtalqAQ3vLaJ
    eJVZqqRQk/B+d5uQkacS1yfy++Pj2nq68CxiEmMygfxKck6IHq2ptwYDVIN0rAgZ
    O5niOTbVs2XHRxq2YVVXw08l3s8VPS7fdT+IVqFp7uXiWINO2kfiWy+CRjAOOXzl
    1D7FvXd7m76RXqaesJrMV7IX+Cn2Kfbh8eqzgO0QSMaKbhm2hRRnJJOVq/ixRBsj
    8TxXQYrZ0GNAjUqUtixeLFP048bYmjLVeE9UWqV5u0zIjX2Jqw+YIxF1JrVyjnSA
    5IQ5JeA4wQHQdCsFv4+Gn1MJzrldlNyUwCCnEpowwYYPR5aXfYZJohhGoi6PAWoT
    fOmbJXCXy7pfdfrQpSnB5v3l+WcDQlRof0rToPsEX6Z+0JW2huhWdc+qJQARAQAB
    tCRUb20gV2Vic3RlciA8c2FtdXJhaWxpbmszQGdtYWlsLmNvbT6JAlQEEwEIAD4W
    IQQ36M8CbqzClfnsnLnLdEJz7aDguwUCWKWgYgIbAwUJBaOagAULCQgHAgYVCAkK
    CwIEFgIDAQIeAQIXgAAKCRDLdEJz7aDgu0QHEACzUeVOscJEMxBfY0bYBKIjpjkp
    RQuz1yVYKbVWuQCYnqgcWb4sh+2FqqM4rnx/Wf0w8bGkEgotltYhhC1xxRPFhbpF
    9ybKyfVb2CkJp/G5fF3kCt+sPJBy/Z7fmdNqeEGQMvx1L8G90VWEQy67dvtZLczU
    g4zbKnkf63FJ5MfdOLzygDYZDHu7JPAPell1TCSyEjzogc0fqVFTqJ8LHVg98PXj
    vgTYNaYAHUwbcyCDq2XMN23Lkhet6hfMrV/v2E5RcbyQzVClCGyp8dvqymlUfOhI
    ZPumQO3VBtHK8lRi0Rwyge+opYbUv1QyvxocgBB0aoIFxJ1P77YhnLjuEOZlv0Kn
    JxIlrd33FUxzMUlR7jxk+ufnTi06MDxROo9PlYSShw0laRik0dd8lLUpQ7Soil6w
    2xkChhmDncL8lMfUFH39dhbMEanESpmsezl4rHBLqNFsbggFOVscyKCo/fOHhxZm
    7XJcl2SMphUphXHOdCC4rD9q12klildn1njG5d3W7V3s1YK41iuR675wfFbzMnxs
    pX/sP6pXa1Mt03Ugw8s1F8EmwXKrwiKPntC31h8rw0j18CwSzkDA7ni3bCpL2iic
    ZHZYChgi0lToQ56w8y7I+sBiwNjs76q4nb1PfW1fD2xViOIsWX8RDqDvc8voU/Qu
    uLKG8jIAOC8g/FdhoYkCMwQQAQgAHRYhBBHJMMRpOmybeJuw92RC3woUuk79BQJY
    pab4AAoJEGRC3woUuk79zvAP/2nSOZT4RSk9EvU7FByKf7fd74nXqFI4Zmlt+Z3q
    KohcRPeQ+3xvDDRX0z8LgkHAZ9JLdjW0D5Ej24/xPIqvcTeHD9GbL+eNE7T6sQkT
    NE0gN+PEncxOYmClFgP/dJifo0LrrlIaxbHFCYuDwUaSE1MLeVstO3VlDFyJU9xT
    y7CQS+HQ1IFDu1eYZbojKhLAZWFkL6UcuxcO2UXu9DAJzULNBvwSkfF9oSS9N5xU
    eQCleXrIpOiiir6pVzUnSHfMjJVlRCxRw6xbfONwFVtAu36FyvfgMCoPVNXX7T/5
    hRtT/BMPqUevhEviuco5TbAVywLzrORaOvA5mpj+WKBmf9wTe3hEIELhfJMY13B+
    xDnGAJN1wniUWRQlcsCgxguoZX+V/HmiKFfRMqtqed+dkaoKE1vqMkR8wBjtgVR/
    EYP+K1SRVaaJ/nZ/3Ay/GPSK3e28SFu4VSftFxCK2xC0E2yUwcZ1PmzgcMYFBv5o
    ksgiJAnFxXGKr0aBQN6VW5tCXaoajacc3gCyHY22NqWrWho/Dd5AwCgzloI0GrU3
    riPet36W3fTZlP1CoqgHKEpiABdSAKlQ9bv3ikvsSLMhVJrBgHtEc6WyY+Ni5rWx
    +kbU37sBMUU7EEQweMjEZZiPht6/BPUSLUuXiYGSVrUltIFwQHH78ilIIf3M+4dR
    g2ZguQINBFiloGIBEACZUvjDRC7zYezXyHVywACJSTpWbY1rVcz6XzONZremlMQl
    9+5ugicpiADLOdFI/EbTVTRl9+jyYR1I6cVqi+dpvYcs+TYNLAufQ78cGaZmbpW9
    j4fxk+4lLA1hqY8crwre6MF4Mn2nyO6pf/ZoFlI8R53fG797IOQm633HGcGURLa6
    R68ex0R3KPsolpweiedilpPhoFN6Ubvd6Ni/RZR9KVEdpsXNip6PaDGBMtxckjWv
    AjStM70vyRQWHAS8wwOEAuzASo0FSHgBZj8mVO3r9gL3rqncqnQPZpKoRNZhXKc4
    CZqwoDGp5JVsfF5u72EW0/RMesaA6qvSgdXSk2mz4aD2rT5ohJRDogX2DKCxJdJP
    X+L5sa1YJT708PHHt+aNeIiZg57dcj6ZWaeuG2YXGYGeMIGgtqjBh6nrx4G05pkS
    +Z7oauJe/hbh8ugjIbhmZTYqnPzDtHRZ5/9pVVLbEBoPX/15j94/vMOJbRGdUJf0
    iA8nOM3ObKbGtVuz2suUGsVPNoxsM1c9oDQbEok8s/+HRmmkTaYbiLX1D5n2aQbC
    U+iIUFnNPfZDMamKUGcfyH0v7SAa7iwLVE1ksb7/2nJyp1uJXS3CvvIXmnhNZ2uf
    31313AVmTd5ixyf0EyJMpz7k53DGa8ALuxrQTk0CXX9ijvyTEkVgSqERdvBrNQAR
    AQABiQI8BBgBCAAmFiEEN+jPAm6swpX57Jy5y3RCc+2g4LsFAliloGICGwwFCQWj
    moAACgkQy3RCc+2g4LskJA/9F5ruNdOMhBEFriaSgYQlL0JXkGxhiAtJSeFIuc1+
    +mAMLBV12HNECbTaIykcZ1LeFkx0B0Dlz41Sj4+/XU+nFAdRlT0QmncjC03bA/AY
    1kT6e0F8IIWy03EVeS+iNEvtHeRH36W6tH+bR1wpE9vO7rkz94H2I96Cc1hnfQc7
    1WbFNlb3OSuvHr2pFUBxkHhPLXGQwE4RvT2DwPboYdqRQSQ1aj1EIEnRx4x/6i7k
    8oqez0q7/Oys0i32ulNdduz5XrSgZ2n8KMB2SwroIpXpNglWwlaQGDJRHRbCqA44
    cauI2MLP6BISV8vSodY7Lo0UALWhomrUzH+bj3D5snMBtSaXb1BmPRDumscB0vU/
    96Xispf3sm0/uBABcBEpsoA0A0d+BLG+F/dmkmvW2OQtDaS3oMMEVw6DzEAe7h+F
    W7SMyIubjN/xOvG5ZRg10L6o4bT/YU/eFgtB7AhpP5uKxKxfZtxuosrqyWNbQ3vj
    w1MbISE2oZotCQkw2VKlOVe47NH1ly05KdeHwPuDVxBXdVNXh2AyS0frjhjheYg0
    kotvFZ2UPkj2W0QO9rv3ML7CXWeeuTE5O8229YBJkv1jglCfOhdThy751bNe91o0
    Gw+SoKRyx+c/OCw8219Re9JbaeU0RG2CUnUhiTFi9X3eVLwkJf0Xcqm9yF+HjQvN
    4ec=
    =L1ID
    -----END PGP PUBLIC KEY BLOCK-----
