---
layout: default
title: NSA Document Archive
comments: true
---

Information should be free, so I'm going to do my part to make sure you can access every leaked NSA document the [EFF](https://www.eff.org/nsa-spying/nsadocs) has kindly provided access to. Unfortunately, they didn't make it easy to mirror, so I'll show you that too. Also, this release is GPG signed with my [public key](/pgp/#Key_Personal), so verify this archive is good before you extract it.

**[NSA Document Archive (2015-02-17)](https://429f159747010d3e425a-7558f5f48a8025938a6d2873e8a987c0.ssl.cf2.rackcdn.com/nsa-docs_2015-02-17.zip)** - **[Signature](https://429f159747010d3e425a-7558f5f48a8025938a6d2873e8a987c0.ssl.cf2.rackcdn.com/nsa-docs_2015-02-17.zip.asc)**

And here's the sig as well.

```
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2

iQIcBAABCAAGBQJU48UZAAoJEGRC3woUuk79gysP/Rn/81oi7QdfNsRmFAZ/E4xD
5YqnhgEROoRkoDiYyYBDWUCYidyQfRSaqKAQ21qy6q3XU6bxn/J5wDgvSSDjR1Dr
rgBUb6NBTXMWjm0+sjsx9xf5wyZ5lBwQ2YO4GzRdXkIoKLdplt0hs4jBoKZm6gg5
6pS92Jly3FADDFnXs7NeYtD6FHhti/ejkVD+ct7M74G6iRkTvq7LZ/bDTUxaVJnQ
RpiJBuv0EDV3NWl8SJ5C1N6feaPVzwakwbIS0n5YndZVm+stGb0WrVCxA5dTjvoT
QX1M89XSdhtUBcQ4ASi0ydpfcpWkzDHNpgE5PjwB9axfLW3NeYLesHy1EljKr73E
6g3kwdcwg33UJ28+aMcNuxgkeXN5GC1iQAqglI7BCby/mwFv7sHBfV67I9aZ2T2n
kJKLyPZAWeCrSxRaPjdcH4NFyzCrEVPoRX3LAPh9W5gyyIKYmi62uC2ysAoKzXl0
UEVuz6ninJxrrVgCefWlLvQx6hgOLuX+GJSNAqxEcpx0LE0O70RBjPsj0wRgz889
XdaXscadBufb8USFj4pO1b0+UvsY7RcyGdt6z9cihnJ8IVyS1Irpd7ITBjfSzgVp
CHE12bm+BPqR/L/77hVDDvxXeTH86JhMBxjWRsJQWsiLm4KyP+hVpJr/HFeJvupA
+VRVj+BiD7u4PIAbTzhO
=JW6c
-----END PGP SIGNATURE-----

```

## How to Mirror the EFFs NSA Document Archive

Unfortunately, the EFF doesn't do a great job of giving us an easy way to archive their entire document store, so I had to hack around a bit to make it work. Here's the steps:

1. Make a new directory, cd into it: `mkdir nsadocs ; cd nsadocs`
2. Mirror the site: `wget -mkxKE -e robots=off https://www.eff.org/nsa-spying/nsadocs`
3. Get the PDF links: `cd www.eff.org/document/ ; cat * | grep -ohe ">https://www.eff.org/files.*<" | sed -e 's/<//' -e 's/>//' > list.txt`
4. Download the whole list: `wget -i list.txt`
5. Copy out the PDFs and you're golden.

## Other Mirrors

Another place you can find these documents (and many more) is [Cryptome](http://cryptome.org/2013/11/snowden-tally.htm). They run a great service over there. If you donate, you can pull their entire archive (not just Snowden files).

You can also check out the [Snowden Surveillance Archive](https://snowdenarchive.cjfe.org/greenstone/cgi-bin/library.cgi) thanks to the [Canadian Journalists for Free Expression (CJFE)](http://cjfe.org/).
