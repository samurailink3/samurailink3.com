---
layout: default
title: "Donations!"
comments: true
---

If you feel like I've helped in some way, or you just want to buy me a beer, you can donate using the buttons below. You can donate any amount you want, there are no set amounts here. I currently support [PayPal](https://www.paypal.com), [Dwolla](https://www.dwolla.com), and [Bitcoin](http://bitcoin.org/en/). If you'd like additional ways to donate, let me know and I'll put a button up.

## Donate with Bitcoin

You can donate to this address directly: <a href="bitcoin:{{ site.btc }}"><span class="fa fa-bitcoin"></span> `{{ site.btc }}`</a>

## Donate with PayPal

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="X89X7AVZHPZTS">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
