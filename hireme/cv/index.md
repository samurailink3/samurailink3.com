---
layout: default
title: "CV"
---

# Tom Webster

**Phone:** +1-(937)-601-8484  
**Email:** [tom@samurailink3.com](mailto:tom@samurailink3.com)

## Tech Career

### 2017 - Present

#### Senior Systems Development Engineer III - EC2 - Amazon Web Services (2021)

* Promoted to Senior Systems Development Engineer III in 2.75 years
* Build API-first company-wide platform for managing internal and external root cause analysis (RCA) documents
  * System is used by several customer-facing and internal teams at AWS to manage communications, risks, timelines, and data integrity for RCA documents
  * System built using cloud-native technologies on AWS:
    * EC2
    * Lambda
    * DynamoDB
    * API Gateway
    * CloudFront
    * CloudFormation
    * S3
    * SQS
    * SNS
    * Various internal Amazon services
  * Backend written in GoLang
  * Frontend written with ReactJS
* Assist in troubleshooting key customer issues for a wide variety of EC2 services
* Reproduce customer issues and track down root cause for edge case scenarios
* Suggest and provide workarounds to alleviate customer pain
* Suggest and provide core fixes for internal services
* Work with AWS' biggest customers on best practices, key issues, and long-term solutions
* Mentor several engineers on software engineering, career growth, and service architecture on an ongoing basis
* Edit and consult on various team documents including:
  * Yearly team planning documents
  * Technical specifications and design documents
  * PR FAQ project documents
  * Promotion documents
* Wrote several small pieces of automation for reporting, analysis, data integrity validation, and metric gathering
* Comleted a total of 164 technical interviews and phone screens

#### Systems Development Engineer II - EC2 - Amazon Web Services (2018)

* Promoted to Systems Development Engineer II in 3 months
* Build core team service supporting over 75 engineers using:
  * GoLang
  * EC2
  * Lambda
  * DynamoDB
  * API Gateway
  * S3
  * CloudFront
  * CloudFormation
  * SQS
  * Various internal Amazon services
* Led team of 6 engineers in developing core team service
  * Trained team in Git and GoLang best practices
  * Led development efforts through an Agile methodology
  * Coordinated efforts between various stakeholders and sponsors
  * Tracked and modified delivery dates based on sprint progress and project management analysis
* Train team members in:
  * Git usage and best practices
  * Developing and deploying scalable systems
  * GoLang usage, best practices, and building scalable services
  * Core AWS technologies (internal and external)
  * Writing and maintaining secure code
  * Cryptography
  * Infosec best practices
* Interview candidates on scripting, developing services, and architecting scalable systems
* Write core cryptography code related to internal projects
* Review all security related code for team
* Joined the core GoLang maintainers team at Amazon
  * Contribute core packages used across the entire Amazon organization
  * Maintain core packages in an ever-changing ecosystem
  * Contribute to code reviews for all GoLang core packages
* Assist in troubleshooting customer issues for a wide variety of EC2 services
* Reproduce customer issues and track down root cause for edge case scenarios
* Suggest and provide workarounds to alleviate customer pain
* Suggest and provide core fixes for internal services
* Work with AWS' biggest customers on best practices, key issues, and long-term solutions
* Mentor several engineers on software engineering, career growth, and service architecture on an ongoing basis

#### Systems Development Engineer I - EC2 - Amazon Web Services (2018)

* Promoted to next level in 3 months, see above section for details

#### Systems Engineer - EC2 - Amazon Web Services (2017)

* Promoted to Systems Development Engineer I in under one year
* Assist service teams across AWS deploy new features and service to various regions
* Troubleshoot various systems issues and service issues
* Provide guidance and fixes to alleviate service issues
* Train team members on Linux systems and troubleshooting
* Interview candidates specializing in Linux systems and troubleshooting
* Assist service teams in diagnosing service edge cases
* Created several operations scripts to assist other team members in troubleshooting
* Create ChatOps bot, now used by many internal AWS teams

### 2016 - 2017:

#### President of OISF ([Ohio Information Security Forum](https://www.ohioinfosec.org/))

* Acquire speakers/presenters
* Help build new website
* Help with Google Apps deployment and configuration
* Acquire sponsors
* Present at meetings and conferences
* Lead security discussions with members
* Research, document, and present on new attacks and defenses
* Help organize yearly security conference

### 2014 - 2017

#### Linux Systems Administrator for Cincinnati Bell

* Build, maintain, and document various Linux systems for various projects
* Help team track and stay on top of deadlines
* Train team members on git and how to use version control systems
* Train team members on Ruby basics and programming in our environment
* Administer git-based collaboration for scripts and configuration data
  * Manage Stash and GitLab instances for team members
  * Review and merge code changes from team members through a code review process
* Guide team members through acceptance testing in multiple stage-gate environments
* Help deploy and implement FreeIPA identity management services
* Maintain Ansible deployment through entire environment
  * Create roles, playbooks, and Ansible configurations for all environments
  * Train team members in Ansible use and extension
* Develop various Ruby applications for internal business use
  * Simple Ruby scripts for automation purposes
  * Sinatra applications for data comparison and small data crunching
  * Rails application for asset data management
* Develop various GoLang applications for internal business use
  * Automation executables for servers and appliances
  * Small executables responsible for using many different company data sources and APIs
  * Libraries to make some vendor APIs easier to use safely and program against
* Responsible for proposing and implementing security-related changes on all environments
  * Configuration changes
  * Patching structure
  * Password management
  * Encryption policies
  * Data protection policies
* Help manage IPTV set top box firmware migration project
  * Develop tools and reporting software for migrations
  * Train team member on migration processes
  * Document migration tools and processes
* Work with various vendors to implement solutions in a secure, documented, and automated way
* Work with vendors to correct programming mistakes in applications
* Work with vendors to correct API usage best-practices
* Work with vendors to fix security holes
* Work with other areas of the business to develop solutions and explain technical capabilities

### 2013 - 2016:

#### Board Member of OISF ([Ohio Information Security Forum](https://www.ohioinfosec.org/))

* Acquire speakers/presenters
* Help build new website
* Help with Google Apps deployment and configuration
* Acquire sponsors
* Present at meetings and conferences
* Lead security discussions with members
* Research, document, and present on new attacks and defenses

### 2010 - 2014:

#### IT Engineer for BWIgroup

* Network and security engineer. Helped craft BWI security policy and implementation
* Lead web app developer (PHP, Ruby on Rails)
  * Design, code, implement business web apps, both internal and external.
  * Consult with business stakeholders, gather requirements, develop user testing scenarios.
* Conceptualize, create, and maintain a ticketing system for an enterprise-level environment
  * Link ticketing database to LDAP / Active Directory authentication
  * Link IMAP/SMTP receiver / sender to ticketing system for user ticket request and response
  * Database upgrades / MySQL troubleshooting and design
* Configure and deploy Git-based web frontend (GitLab) for IT source code repository
* Web development and deployment for internal company projects
* Deploying and configuring a variety of Linux servers
* Conceptualize, build, and maintain a Microsoft Window Deployment Server
* Utilize Windows Deployment Services to convert old client computer to new deployments
* Create, maintain, and troubleshoot Active Directory units and domain controllers
* Migrate users from Delphi computers to BWI computers while minimizing downtime and maintaining client data integrity
* Support and manage IT systems across multiple worldwide locations
* Remote support and troubleshooting of traveling employees
* Train multiple IT technicians on company processes and IT skills across a wide variety of subjects, including:
  * Break-Fix Support
  * Deployment
  * Imaging
  * SCCM Management
  * Remote Desktop Setup and Support
* Manage and utilize enterprise DHCP servers to create reservations and maintain subnets
* Modification of enterprise DNS servers
* Windows Server hardware setup and configuration
* Network and security configuration experience with systems such as Astaro and Fortigate.
* IPsec tunnel configuration and deployment
* Virtual host configuration (Microsoft Hyper-V) and deployment
* Set up of virtual servers on a wide variety of platforms (Many Windows versions, many Linux distributions)
* Configure and maintain Linux servers in a Microsoft Windows Active Directory Environment
* Familiar with configuring Microsoft Domain Controller systems
* Wireless network deployment, configuration, and troubleshooting
* Secure data removal for highly sensitive corporate data
* Data recovery services for bad drives and accidentally-deleted files
* Configure and Maintain Fortigate UTM Solutions across multiple worldwide locations
* Design, code, and launch custom corporate S/MIME Email Encryption solution for users
* Design, code, and launch custom corporate PGP Email Encryption solution for users
* Design, code, and launch live-updating AJAX bulletin site for customer
* Design, code, and launch public-facing corporate aftermarket website
* Coordinate team of developers
* Configure and maintain SNMP metric systems (MRTG/Cacti)
* Configure and maintain latency metric systems (Smokeping)
* Create standard Debian Linux server deployment
  * Move old servers to new Debian standard
* Train employees on the use of Linux Desktops and Servers
  * Work with students to troubleshoot issues and answer questions
  * Work with coworkers to develop training regime and additional curriculum

### 2008 - 2010:

#### Computing and Telecommunications Services, Wright State University Senior Assistant

* Supervise and train other student employees
* Maintain printers and computer laboratories
* Assist with secure data removal and data recovery
* Repair networking and wireless connectivity issues
* Perform malware removal and security penetration testing
* Provide hardware and software technical support to clients
* Small amounts of programming for help desk computers

### 2007 - 2008:

#### Operating Systems Class Teacher's Assistant

* Maintain a punctual, current, and accurate grade sheet
* Provide assistance and troubleshoot issues in lab setting
* Tutor students in core and advanced Unix and Windows concepts
* Maintain independent office hours, lab troubleshooting, network maintenance

### 2006 - 2017:

#### Freelance Technology Contractor

* Act as tech and server-based consultant
* Replace defective or install new hardware
* Perform malware cleanups and troubleshooting services
* Assist with installation of software and operating systems
* Break-fix solutions management
* Help-desk deployment strategy consultant
* Internet presence consultant
* Website set up and configuration
* Web site design consultant
* Web app development and consultation
* Email and domain configuration
* Amazon EC2 Deployment and Configuration
* RackSpace Deployment and Configuration
* Digital Ocean Deployment and Configuration
* Linux Scripting and server deployment
* Cloud computing specialist
* Security specialist
* Astaro Security Gateway Deployment and Management
* pfSense Deployment and Configuration
* Network engineering
* Web technology engineering (web presence, DNS management, hosting server setup)
* Programming in Ruby and GoLang

## Interesting Projects

* Built corporate-style home network with Astaro Managed Router and Cisco enterprise access point
  * This was later changed to a custom built pfSense machine
  * Built and configured Linux Virtual Machine Hosting platform
  * Built and configured SSL-enabled web hosting subdomains on home Linux server
    * Basic web hosting
    * Mail server hosting with AJAX frontend and mobile site
    * Dropbox-style cloud storage with HTML5 web frontend
  * Later migrated home servers to AWS using IPsec tunnel to VPC
* Built 10-foot interface for live-updating site conditions for corporate customer
* Built various Ruby applications over the years
* Build various GoLang applications over the years
* Built a build system to compile tools used for creating a classic gaming purpose-built computer, with synchronized save states and games
* Many others, just ask!

## Other Experience

### 2013 - Present: Co-Host of the weekly thirty-minute security podcast [Security:inThirty](http://inthirty.net/author/security/)

Responsibilities: Research on security topics, talk creation, testing and fielding audience questions.

### 2007 - Present: Co-Founder of Seventy Two Pin Connector: Blog focused on classic gaming

Responsibilities: Site design, FTP Management, integrating Google Apps into domains, domain and hosting management, WordPress administration and deployment, graphic art, enlisting writers, producing a podcast.

### 2005 - 2007: Co-Editor of Fragility Productions: Media-centric weblog.

### 2000 - 2003: Ambassador for Children's Medical Center of Dayton.

Responsibilities: PR appearances, speeches, radio interviews, and television spots.

## Skills

* Extremely experienced in the medical industry, dealing with the care, as well as, administrative/business side of the medical industry.
* Hardware and software troubleshooting across Linux, Unix, Windows, and OSX systems. pPssesses extensive knowledge of Windows and Unix (Linux) systems, extremely experienced with computer security / data encryption, and adept at acquiring new technological knowledge.
* Skilled at building and deploying web applications using Ruby (Sinatra, Rails, stand-alone Ruby) and GoLang.
* Friendly and professional, proven history of reliability, superb people-skills, and excellent at explaining complex technology to non-technical users and clients.
* Built and configured Linux servers capable of secured network storage (accessible through any internet connection), multi-user web hosting packages (Including WordPress, phpBB, flat hosting, Ruby on Rails, and many others), SSH configuration and administration, media streaming, and automated network analysis.
* Creating and maintaining enterprise-level Linux SAMBA Primary Domain Controllers.
* Bash scripting and implementation via User-Facing PHP frontend.
* Creating and maintaining enterprise-level, complex networks.
* Creating and maintaining enterprise-level Active Directory networks.
* Creating and operating large-scale Windows Deployment Servers.
* Maintain and upgrade enterprise systems utilizing Microsoft System Center Configuration Manager (SCCM).
* Build Windows 7 deployment images on various platforms, hardware configurations, and architectures.
* Maintain Microsoft Windows Server Systems.
* Creating and maintaining Microsoft Windows DHCP and DNS systems and integrating these with Active Directory networks on a professional level.
* Experience with Microsoft ImageX and Windows AIK imaging tools.
* Experience with Group Policy tools and procedures.
* Experience with Microsoft Hyper-V, VMware, VirtualBox, and other virtualization tools.
* Experience in deploying virtual (and physical) Unix/Linux servers into an Active Directory environment.
* Automation experience with Ansible.
* Experience in training team members in automation, introductory programming, security, and version control.
* Excellent team player and works well with others.
* Experience in conducting technical interviews with a wide variety of candidates.
* Experience with building, securing, managing, and scaling projects using AWS technologies.
* Experience in mentoring junior coworkers, helping them skill up and gain promotions in the organization.
* Experience in leading and managing a team of developers to build, scale, and maintain large-scale systems in the organization.
* Experience in building critical company-wide systems used by many organizations.
* Experience in building API-first systems designed for large-scale use in a complex environment.
* Experience in working with key business customers and partners through complex and challenging technical issues to come to a resolution while maintaining the customer relationship.
